# Changelog
All notable changes to this project will be documented in this file

## Unreleased
### Added
* Include testing dependencies ([#1](https://gitlab.com/yale-sdmp/stc-scheduler/issues/1)).
* Include authentication dependencies ([#2](https://gitlab.com/yale-sdmp/stc-scheduler/issues/2)).
* Include front end gems ([#3](https://gitlab.com/yale-sdmp/stc-scheduler/issues/3)).
* Include environment variables file and gem ([#4](https://gitlab.com/yale-sdmp/stc-scheduler/issues/4)).
* Include rubocop ([#5](https://gitlab.com/yale-sdmp/stc-scheduler/issues/5)).
* Include set up bundler audit ([#6](https://gitlab.com/yale-sdmp/stc-scheduler/issues/6)).
* Set up CI/CD ([#7](https://gitlab.com/yale-sdmp/stc-scheduler/issues/7)).
* Set up heartbeat ([#8](https://gitlab.com/yale-sdmp/stc-scheduler/issues/8)).
* Set up initial models ([#9](https://gitlab.com/yale-sdmp/stc-scheduler/issues/9)).
* Set up Docker ([#16](https://gitlab.com/yale-sdmp/stc-scheduler/issues/16)).
* Added CI for node modules  ([#51](https://gitlab.com/yale-sdmp/stc-scheduler/issues/51)).
* Added yale fonts  ([#52](https://gitlab.com/yale-sdmp/stc-scheduler/issues/52)).
* Added a blank navbar ([#56](https://gitlab.com/yale-sdmp/stc-scheduler/issues/56)).
* Added model validations ([#23](https://gitlab.com/yale-sdmp/stc-scheduler/issues/23)).
* Added branch validations ([#21]https://gitlab.com/yale-sdmp/stc-scheduler/issues/21).
* Added model validations ([#23](https://gitlab.com/yale-sdmp/stc-scheduler/issues/23)).
* Added branch validations ([#21]https://gitlab.com/yale-sdmp/stc-scheduler/issues/21).
* Added availability validations ([#20](https://gitlab.com/yale-sdmp/stc-scheduler/issues/20)).
* Added location validations ([#19](https://gitlab.com/yale-sdmp/stc-scheduler/issues/19)).
* Added chair validations ([#22](https://gitlab.com/yale-sdmp/stc-scheduler/issues/22)).
* Added user model validations ([#17](https://gitlab.com/yale-sdmp/stc-scheduler/issues/17)).
* Added user preferred location validations ([#18](https://gitlab.com/yale-sdmp/stc-scheduler/issues/18)).
* Added user profile page ([#18](https://gitlab.com/yale-sdmp/stc-scheduler/issues/18)).
* Set up admin interface ([#12](https://gitlab.com/yale-sdmp/stc-scheduler/issues/12))
* Add 'role' to User ([#24](https://gitlab.com/yale-sdmp/stc-scheduler/issues/24))
* Set up administrate ([#25](https://gitlab.com/yale-sdmp/stc-scheduler/issues/25))
* Seed branches ([#28](https://gitlab.com/yale-sdmp/stc-scheduler/issues/28))
* Restrict access to admin pages ([#26](https://gitlab.com/yale-sdmp/stc-scheduler/issues/26))
* Set up the User role option to be a dropdown of admin or student in admin page ([#31](https://gitlab.com/yale-sdmp/stc-scheduler/issues/31))
* Add link to administrate in nav bar for logged-in admins ([#27](https://gitlab.com/yale-sdmp/stc-scheduler/issues/27))
* Added user profile edit page ([#35](https://gitlab.com/yale-sdmp/stc-scheduler/issues/35)).
* Added submitted boolean to users table ([#41](https://gitlab.com/yale-sdmp/stc-scheduler/issues/41))
* Added user profile edit page ([#35](https://gitlab.com/yale-sdmp/stc-scheduler/issues/35)).
* Added submitted boolean to users table ([#41](https://gitlab.com/yale-sdmp/stc-scheduler/issues/41))
* Added partial form ([#37](https://gitlab.com/yale-sdmp/stc-scheduler/issues/37))
* Added availability display ([#38](https://gitlab.com/yale-sdmp/stc-scheduler/issues/38))
* Added validations to the availability creation ([#48](https://gitlab.com/yale-sdmp/stc-scheduler/issues/48))
* Added home page for logged in users ([#39](https://gitlab.com/yale-sdmp/stc-scheduler/issues/39))
* Updated day field in availability table to be an integer and set up an enum ([#47](https://gitlab.com/yale-sdmp/stc-scheduler/issues/47))
* Added unsubmitted report to the home page for admins ([#43](https://gitlab.com/yale-sdmp/stc-scheduler/issues/43))
* Added calendar display for availabilities ([#46](https://gitlab.com/yale-sdmp/stc-scheduler/issues/46))
* Added live readout of number of hours selected in availabilities calendar ([#74](https://gitlab.com/yale-sdmp/stc-scheduler/issues/74))
* Added submission display to availabilities calendar ([#76](https://gitlab.com/yale-sdmp/stc-scheduler/issues/76))
* Added javascript to allow availabilities to be painted into calendar ([#75](https://gitlab.com/yale-sdmp/stc-scheduler/issues/75))
* Added submission button to calendar ([#83](https://gitlab.com/yale-sdmp/stc-scheduler/issues/83))
* Set up selenium ([#85](https://gitlab.com/yale-sdmp/stc-scheduler/issues/85))
* Set up calendar to prevent painting over submitted availabilities ([#84](https://gitlab.com/yale-sdmp/stc-scheduler/issues/84))
* Added Matt's changes to calendar ([#91](https://gitlab.com/yale-sdmp/stc-scheduler/issues/91))
* Added improved-yarn-audit ([#92](https://gitlab.com/yale-sdmp/stc-scheduler/issues/92))
* Added welcome page for unverified users ([#67](https://gitlab.com/yale-sdmp/stc-scheduler/issues/67))
* Added pundit ([#61](https://gitlab.com/yale-sdmp/stc-scheduler/issues/61))
* Added availability due date message ([#95](https://gitlab.com/yale-sdmp/stc-scheduler/issues/95))
* Added policy for availability calendar route ([#114](https://gitlab.com/yale-sdmp/stc-scheduler/issues/95))
* Added user preferred location form ([#62](https://gitlab.com/yale-sdmp/stc-scheduler/issues/62))
* Added step bar for student input flow ([#102](https://gitlab.com/yale-sdmp/stc-scheduler/issues/102))
* Added Review page ([#98](https://gitlab.com/yale-sdmp/stc-scheduler/issues/98))
* Updated upl validations ([#120](https://gitlab.com/yale-sdmp/stc-scheduler/issues/120))
* Updated user edit form so that user cannot select nil branch ([#122](https://gitlab.com/yale-sdmp/stc-scheduler/issues/122))
* Changed student step bar to link_to (and other minor fixes) [#130](https://gitlab.com/yale-sdmp/stc-scheduler/issues/130))
* Changed branch.title to enumerable ([#129](https://gitlab.com/yale-sdmp/stc-scheduler/issues/129))
* Added delete buttons to calendar ([#90](https://gitlab.com/yale-sdmp/stc-scheduler/issues/90))
* Removed profile link from dropdown for students ([#134](https://gitlab.com/yale-sdmp/stc-scheduler/issues/134))
* Added policies for student input flow ([#101](https://gitlab.com/yale-sdmp/stc-scheduler/issues/101))
* Added validation for min hours ([#127](https://gitlab.com/yale-sdmp/stc-scheduler/issues/127))
* Added slot view for heatmap ([#109](https://gitlab.com/yale-sdmp/stc-scheduler/issues/109))
* Added calendar to heatmap ([#110](https://gitlab.com/yale-sdmp/stc-scheduler/issues/110))
* Added user view to Admin Dashboard ([#111](https://gitlab.com/yale-sdmp/stc-scheduler/-/issues/111))
* Added pundit policy for admin dashboard ([#108](https://gitlab.com/yale-sdmp/stc-scheduler/issues/108))
* Added shift model ([#115](https://gitlab.com/yale-sdmp/stc-scheduler/issues/115))
* Added percentage of target hours reached to user view of admin dashboard ([#133](https://gitlab.com/yale-sdmp/stc-scheduler/issues/133))
* Added calendar for chairs ([#118](https://gitlab.com/yale-sdmp/stc-scheduler/issues/118))
* Added user list to calendar view ([#151](https://gitlab.com/yale-sdmp/stc-scheduler/issues/151))
* Added shift creation calendar to scheduling page ([#159](https://gitlab.com/yale-sdmp/stc-scheduler/issues/159))
* Add user list to calendar view ([#151](https://gitlab.com/yale-sdmp/stc-scheduler/issues/151))
* Sort the list of users by who is available during the selected chair ([#160](https://gitlab.com/yale-sdmp/stc-scheduler/issues/160))
* Added email incomplete button to admin home page ([#44](https://gitlab.com/yale-sdmp/stc-scheduler/issues/44))
* Added button to display/hide users' special requests ([#170](https://gitlab.com/yale-sdmp/stc-scheduler/issues/170))
* Added chair#filled ([#168](https://gitlab.com/yale-sdmp/stc-scheduler/issues/168))
* Added tooltip for shift locations not in current location in shifts calendar ([#171](https://gitlab.com/yale-sdmp/stc-scheduler/issues/171))
* Added font awesome ([#149](https://gitlab.com/yale-sdmp/stc-scheduler/issues/149))
* Added mini calendar to scheduling page user list ([#167](https://gitlab.com/yale-sdmp/stc-scheduler/issues/167))
* Added shifts to Administrate dashboard and tweaked other Administrate formatting ([#136](https://gitlab.com/yale-sdmp/stc-scheduler/issues/136)
* Added Deputy export ([#179](https://gitlab.com/yale-sdmp/stc-scheduler/issues/179))
* Added progress bar to show chair filled status ([#180](https://gitlab.com/yale-sdmp/stc-scheduler/issues/180))
* Fix search and sort for Administrate page for shifts ([#184](https://gitlab.com/yale-sdmp/stc-scheduler/issues/184))
* Added ability to sort users list ([#169](https://gitlab.com/yale-sdmp/stc-scheduler/issues/169))
* Added alert for gaps in availabilities to calendar ([#183](https://gitlab.com/yale-sdmp/stc-scheduler/issues/183))
### Changed
* Switched yale-ui implementation to work with webpacker ([#50](https://gitlab.com/yale-sdmp/stc-scheduler/issues/50))
* Removed yale-ui custom js from the webpack pipeline and fixed CI ([#55](https://gitlab.com/yale-sdmp/stc-scheduler/issues/55))
* Limited availability times on form to 8am-midnight ([#49](https://gitlab.com/yale-sdmp/stc-scheduler/issues/49))
* Changed calendar to look like wireframe ([#78](https://gitlab.com/yale-sdmp/stc-scheduler/issues/78))
* Moved js to webpacker ([#89](https://gitlab.com/yale-sdmp/stc-scheduler/issues/89))
* Updated user form to have asterisk next to required fields ([#65](https://gitlab.com/yale-sdmp/stc-scheduler/issues/55))
* Updated rails from 6.0.2 to 6.0.3 ([#92](https://gitlab.com/yale-sdmp/stc-scheduler/issues/92))
* Updated puma from 4.3.1 to 4.3.5 ([#92](https://gitlab.com/yale-sdmp/stc-scheduler/issues/92))
* Updated administrate from 0.13.0 to 0.14.0 ([#92](https://gitlab.com/yale-sdmp/stc-scheduler/issues/92))
* Updated javascript dependencies ([#92](https://gitlab.com/yale-sdmp/stc-scheduler/issues/92))
* Ordered availabilities in table by day then time ([#86](https://gitlab.com/yale-sdmp/stc-scheduler/issues/86))
* Removed net id from User table ([#71](https://gitlab.com/yale-sdmp/stc-scheduler/-/issues/71))
* Added defaults to booleans in db([#72](https://gitlab.com/yale-sdmp/stc-scheduler/issues/72))
* Updated application view ([#70](https://gitlab.com/yale-sdmp/stc-scheduler/issues/70))
* Added fields to user model ([#112](https://gitlab.com/yale-sdmp/stc-scheduler/issues/112))
* Moved calendar to its own route ([#96](https://gitlab.com/yale-sdmp/stc-scheduler/issues/96))
* Set up user info flow ([#100](https://gitlab.com/yale-sdmp/stc-scheduler/issues/100))
* Changed availability#destroy to redirect to page from which it was called ([#125](https://gitlab.com/yale-sdmp/stc-scheduler/issues/125))
* Set up calendar flow ([#97]https://gitlab.com/yale-sdmp/stc-scheduler/-/issues/97))
* Improved upl creation ([#121](https://gitlab.com/yale-sdmp/stc-scheduler/issues/121))
* Improved accesibility for availability creation ([#82](https://gitlab.com/yale-sdmp/stc-scheduler/issues/82))
* Improved accessibility for application ([#123](https://gitlab.com/yale-sdmp/stc-scheduler/issues/123))
* Allowed adjacent availabilities ([#119](https://gitlab.com/yale-sdmp/stc-scheduler/issues/119))
* Set up user preferred location flow ([#103](https://gitlab.com/yale-sdmp/stc-scheduler/issues/103))
* Updated user form to look like wireframe ([#104](https://gitlab.com/yale-sdmp/stc-scheduler/-/issues/104))
* Handled student redirection upon login ([#99](https://gitlab.com/yale-sdmp/stc-scheduler/issues/99))
* Allowed date and time range for bare calendar partial ([#153](https://gitlab.com/yale-sdmp/stc-scheduler/issues/153))
* Allowed user to click "Next" on calendar even if they haven't entered any input ([#154](https://gitlab.com/yale-sdmp/stc-scheduler/issues/154))
* Added fields to location and chair models ([#145](https://gitlab.com/yale-sdmp/stc-scheduler/issues/145))
* Updated admin profile editing ([#155](https://gitlab.com/yale-sdmp/stc-scheduler/issues/155))
* Allowed more than one user to have no email ([163](https://gitlab.com/yale-sdmp/stc-scheduler/-/issues/163))
* Added test to check if new user is made when user first logs in ([#132](https://gitlab.com/yale-sdmp/stc-scheduler/issues/132))
* Added currently scheduled shifts to scheduling panel ([#158](https://gitlab.com/yale-sdmp/stc-scheduler/issues/158))
* Moved constants to initializer ([#166](https://gitlab.com/yale-sdmp/stc-scheduler/issues/166))
* Turned off yarn integrity check for webpacker
* Changed shifts scheduling view to show available users by slot and make display of scheduled users more compact ([#181](https://gitlab.com/yale-sdmp/stc-scheduler/issues/181))
* Switched yale-ui implementation to work with webpacker ([#50](https://gitlab.com/yale-sdmp/stc-scheduler/issues/50))
* Changed SLOT_LENGTH to MINUTE_STEP ([#80](https://gitlab.com/yale-sdmp/stc-scheduler/issues/80))
* Updated packages to pass security audits ([#126](https://gitlab.com/yale-sdmp/stc-scheduler/issues/126))
* Updated rails to 6.1.4.1 and ruby to 3.0.3 ([#187](https://gitlab.com/yale-sdmp/stc-scheduler/issues/187))
* Changed deployment from Heroku to Gitlab registry for migration to AWS Fargate. ([#188](https://gitlab.com/yale-sdmp/stc-scheduler/issues/188))

### Fixed
* Fixed yarn compilation issues with docker ([#57](https://gitlab.com/yale-sdmp/stc-scheduler/issues/57))
* Fixed shoulda-matcher deprecation warning ([#69](https://gitlab.com/yale-sdmp/stc-scheduler/issues/57))
* Fixed availability validation and spec ([#81](https://gitlab.com/yale-sdmp/stc-scheduler/issues/81))
* Fixed user#update to correctly update branch ([#66](https://gitlab.com/yale-sdmp/stc-scheduler/issues/66))
* Fixed calendar JS to work after navigating from other pages ([#93](https://gitlab.com/yale-sdmp/stc-scheduler/issues/93))
* Fixed autoloading during iniatlization issue ([#135](https://gitlab.com/yale-sdmp/stc-scheduler/issues/135))
* Fixed users list to show all users when no chair is selected ([#173](https://gitlab.com/yale-sdmp/stc-scheduler/issues/173)))
* Fix deprecation warning and yarn audit ([#175](https://gitlab.com/yale-sdmp/stc-scheduler/issues/175)))
* Fix #slots in models for times ending at midnight ([#186](https://gitlab.com/yale-sdmp/stc-scheduler/issues/175)))
