# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_07_17_191304) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "availabilities", force: :cascade do |t|
    t.integer "day"
    t.integer "start_time"
    t.integer "end_time"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_availabilities_on_user_id"
  end

  create_table "branches", force: :cascade do |t|
    t.integer "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "due_date"
  end

  create_table "chairs", force: :cascade do |t|
    t.integer "start_time"
    t.integer "end_time"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "location_id"
    t.integer "day"
    t.integer "user_min"
    t.integer "user_max"
    t.index ["location_id"], name: "index_chairs_on_location_id"
  end

  create_table "deputy_roles", force: :cascade do |t|
    t.string "title"
    t.bigint "branch_id"
    t.index ["branch_id"], name: "index_deputy_roles_on_branch_id"
  end

  create_table "locations", force: :cascade do |t|
    t.string "name"
    t.integer "required_hours"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "branch_id"
    t.boolean "lead_needed", default: false
    t.boolean "lead_only", default: false
    t.boolean "continuous", default: false
    t.integer "hours_staffed"
    t.integer "times_staffed"
    t.integer "user_min", default: 1
    t.integer "user_max"
    t.index ["branch_id"], name: "index_locations_on_branch_id"
  end

  create_table "shifts", force: :cascade do |t|
    t.integer "start_time"
    t.integer "end_time"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id"
    t.bigint "chair_id"
    t.integer "day"
    t.index ["chair_id"], name: "index_shifts_on_chair_id"
    t.index ["user_id"], name: "index_shifts_on_user_id"
  end

  create_table "user_preferred_locations", force: :cascade do |t|
    t.integer "preferred_hours"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id"
    t.bigint "location_id"
    t.index ["location_id"], name: "index_user_preferred_locations_on_location_id"
    t.index ["user_id"], name: "index_user_preferred_locations_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "username"
    t.string "email"
    t.string "first_name"
    t.string "last_name"
    t.boolean "lead", default: false
    t.text "special_requests"
    t.integer "min_hours", default: 2
    t.integer "max_hours", default: 19
    t.boolean "meal_plan", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "branch_id"
    t.boolean "submitted", default: false
    t.integer "role", default: 0, null: false
    t.integer "target_hours"
    t.integer "min_shift"
    t.integer "max_shift"
    t.bigint "deputy_role_id"
    t.index ["branch_id"], name: "index_users_on_branch_id"
    t.index ["deputy_role_id"], name: "index_users_on_deputy_role_id"
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  add_foreign_key "availabilities", "users"
  add_foreign_key "chairs", "locations"
  add_foreign_key "deputy_roles", "branches"
  add_foreign_key "locations", "branches"
  add_foreign_key "shifts", "chairs"
  add_foreign_key "shifts", "users"
  add_foreign_key "user_preferred_locations", "locations"
  add_foreign_key "user_preferred_locations", "users"
  add_foreign_key "users", "branches"
  add_foreign_key "users", "deputy_roles"
end
