# frozen_string_literal: true

# This file should contain all the record creation needed to
# seed the database with its default values.
# The data can then be loaded with the rails db:seed command
# (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Fifth Element' }])
#   Character.create(name: 'Luke', movie: movies.first)

mt = Branch.create(title: 'mt')
ct = Branch.create(title: 'ct')
st = Branch.create(title: 'st')
Branch.create(title: 'sdmp')

Location.create(name: 'IO', branch: st)
Location.create(name: 'TTO', branch: st)
Location.create(name: 'Sage Hall', branch: st)
Location.create(name: 'CCAM', branch: mt)
Location.create(name: 'ELO', branch: mt)
Location.create(name: 'Zone 1', branch: ct)
Location.create(name: 'Zone 2', branch: ct)
Location.create(name: 'Zone 3', branch: ct)
Location.create(name: 'Zone 4', branch: ct)
