class AddDueDateToBranch < ActiveRecord::Migration[6.0]
  def change
    add_column :branches, :due_date, :datetime
  end
end
