# frozen_string_literal: true

class AddRolesTable < ActiveRecord::Migration[6.0]
  def change
    create_table :deputy_roles do |t|
      t.string :title
    end

    add_reference :users, :deputy_role, foreign_key: true
    add_reference :deputy_roles, :branch, foreign_key: true
  end
end
