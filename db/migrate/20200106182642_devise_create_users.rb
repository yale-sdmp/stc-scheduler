# frozen_string_literal: true

class DeviseCreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :username
      t.string :netid
      t.string :email
      t.string :first_name
      t.string :last_name
      t.boolean :lead
      t.text :special_requests
      t.integer :min_hours
      t.integer :max_hours
      t.boolean :meal_plan
      t.timestamps null: false
    end

    add_index :users, :username, unique: true
  end
end
