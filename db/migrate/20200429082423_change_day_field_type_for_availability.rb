class ChangeDayFieldTypeForAvailability < ActiveRecord::Migration[6.0]
  def change
    change_column :availabilities, :day, :integer, using: 'day::integer'
  end
end
