class AddDayToChair < ActiveRecord::Migration[6.0]
  def change
    add_column :chairs, :day, :integer 
  end
end
