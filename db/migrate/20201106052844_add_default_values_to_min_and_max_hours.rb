class AddDefaultValuesToMinAndMaxHours < ActiveRecord::Migration[6.0]
  def change
    change_column_default :users, :min_hours, 2
    change_column_default :users, :max_hours, 19
  end
end
