class AddDayToShifts < ActiveRecord::Migration[6.0]
  def change
    add_column :shifts, :day, :integer
  end
end
