class AddSumbmittedColumnToUserTable < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :submitted, :boolean, :default => false
  end
end
