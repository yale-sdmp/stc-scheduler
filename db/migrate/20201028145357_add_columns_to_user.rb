class AddColumnsToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :target_hours, :integer
    add_column :users, :min_shift, :integer
    add_column :users, :max_shift, :integer
  end
end
