class AddDefaultsToBooleans < ActiveRecord::Migration[6.0]
  def change
    change_column :users, :lead, :boolean, default: false
    change_column :users, :meal_plan, :boolean, default: false
  end
end
