class RemoveNetid < ActiveRecord::Migration[6.0]
  def change
    remove_column :users, :netid
  end
end
