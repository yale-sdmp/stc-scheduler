class AddFieldsToLocationAndChair < ActiveRecord::Migration[6.0]
  def change
    add_column :locations, :lead_needed, :boolean, default: false
    add_column :locations, :lead_only, :boolean, default: false
    add_column :locations, :continuous, :boolean, default: false
    add_column :locations, :hours_staffed, :integer
    add_column :locations, :times_staffed, :integer
    add_column :locations, :user_min, :integer, default: 1
    add_column :locations, :user_max, :integer

    add_column :chairs, :user_min, :integer
    add_column :chairs, :user_max, :integer

    remove_column :locations, :public_facing
    remove_column :chairs, :lead
    remove_column :chairs, :required
  end
end
