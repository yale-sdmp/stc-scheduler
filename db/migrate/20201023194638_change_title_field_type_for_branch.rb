class ChangeTitleFieldTypeForBranch < ActiveRecord::Migration[6.0]
  def change
    change_column :branches, :title, :integer, using: 'title::integer'
  end
end
