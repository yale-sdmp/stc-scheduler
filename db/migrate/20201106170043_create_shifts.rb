class CreateShifts < ActiveRecord::Migration[6.0]
  def change
    create_table :shifts do |t|
      t.integer :start_time
      t.integer :end_time
      t.timestamps null: false
    end

    add_reference :shifts, :user, foreign_key: true
    add_reference :shifts, :chair, foreign_key: true
  end
end
