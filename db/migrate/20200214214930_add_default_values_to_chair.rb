class AddDefaultValuesToChair < ActiveRecord::Migration[6.0]
  def change
    change_column :chairs, :lead, :boolean, default: false
    change_column :chairs, :required, :boolean, default: true
  end
end
