class AddAllModels < ActiveRecord::Migration[6.0]
  def change
    create_table :availabilities do |t|
      t.string :day
      t.integer :start_time
      t.integer :end_time
      t.timestamps null: false
    end

    create_table :branches do |t|
      t.string :title
      t.timestamps null: false
    end

    create_table :locations do |t|
      t.string :name
      t.integer :required_hours
      t.boolean :public_facing
      t.timestamps null: false
    end

    create_table :user_preferred_locations do |t|
      t.integer :preferred_hours
      t.timestamps null: false
    end

    create_table :chairs do |t|
      t.integer :start_time
      t.integer :end_time
      t.boolean :lead
      t.boolean :required
      t.timestamps null: false
    end

    add_reference :users, :branch, foreign_key: true
    add_reference :availabilities, :user, foreign_key: true
    add_reference :locations, :branch, foreign_key: true
    add_reference :user_preferred_locations, :user, foreign_key: true
    add_reference :user_preferred_locations, :location, foreign_key: true
    add_reference :chairs, :location, foreign_key: true
  end
end
