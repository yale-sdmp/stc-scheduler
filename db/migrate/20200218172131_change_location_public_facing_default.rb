class ChangeLocationPublicFacingDefault < ActiveRecord::Migration[6.0]
  def change
    change_column :locations, :public_facing, :boolean, :default => true
  end
end
