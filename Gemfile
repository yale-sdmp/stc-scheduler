# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.0.3'

gem 'administrate', '~> 0.16.0'
gem 'bootsnap', '>= 1.9.3', require: false
gem 'devise', '~> 4.8'
gem 'devise_cas_authenticatable', '~> 1.10'
gem 'dotenv-rails', '~> 2.7'
gem 'jbuilder', '~> 2.9.1'
gem 'pg', '1.2.2'
gem 'puma', '~> 4.3.5'
gem 'pundit', '~> 2.1.0'
gem 'rails', '~> 6.1.4.1'
gem 'turbolinks', '~> 5.2.1'
gem 'webmock', '~> 3.8'
gem 'webpacker', '~> 5.4.3'

group :development, :test do
  gem 'bullet', '~> 6.1.0'
  gem 'bundler-audit', '~> 0.9.0', require: false
  gem 'byebug', '~> 11.0.1', platforms: %i(mri mingw x64_mingw)
  gem 'factory_bot_rails', '~> 6.2.0'
  gem 'ffaker', '~> 2.13.0'
  # allows for the usage of 'save_and_open_page'
  gem 'launchy', '~> 2.5'
  gem 'pry-byebug', '~> 3.9.0'
  gem 'pry-rails', '~> 0.3.9'
  gem 'rubocop', '~> 1.23.0'
  gem 'rubocop-rails', '~> 2.12.4', require: false
  gem 'rubocop-rspec', '~> 2.6.0', require: false
  gem 'shoulda-matchers', '~> 4.2.0'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring', '~> 2.1.0'
end

group :test do
  gem 'capybara', '~> 3.36.0'
  gem 'rspec-rails', '~> 5.0.2'
  gem 'selenium-webdriver', '~> 4.1.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', '~> 1.2019.3', platforms: %i(mingw mswin x64_mingw jruby)
