# frozen_string_literal: true

require 'administrate/base_dashboard'

# Administrate dashboard for Location model
class LocationDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    name: Field::String,
    id: Field::Number,
    branch: Field::BelongsTo,
    chairs: Field::HasMany,
    user_preferred_locations: Field::HasMany,
    required_hours: Field::Number,
    lead_needed: Field::Boolean,
    lead_only: Field::Boolean,
    continuous: Field::Boolean,
    hours_staffed: Field::Number,
    times_staffed: Field::Number,
    user_min: Field::Number,
    user_max: Field::Number
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = %i(
    name
    branch
    required_hours
  ).freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = %i(
    name
    id
    branch
    chairs
    user_preferred_locations
    required_hours
    hours_staffed
    times_staffed
    user_min
    user_max
    lead_needed
    lead_only
    continuous
  ).freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = %i(
    name
    branch
    chairs
    user_preferred_locations
    required_hours
    hours_staffed
    times_staffed
    user_min
    user_max
    lead_needed
    lead_only
    continuous
  ).freeze

  # COLLECTION_FILTERS
  # a hash that defines filters that can be used while searching via the search
  # field of the dashboard.
  #
  # For example to add an option to search for open resources by typing "open:"
  # in the search field:
  #
  #   COLLECTION_FILTERS = {
  #     open: ->(resources) { where(open: true) }
  #   }.freeze
  COLLECTION_FILTERS = {}.freeze

  # Overwrite this method to customize how locations are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(location)
    location.name.to_s
  end
end
