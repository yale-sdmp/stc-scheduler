# frozen_string_literal: true

require 'administrate/base_dashboard'

# Administrate dashboard for User model
class UserDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    first_name: Field::String,
    last_name: Field::String,
    id: Field::Number,
    username: Field::String,
    email: Field::String,
    branch: Field::BelongsTo,
    user_preferred_locations: Field::HasMany,
    availabilities: Field::HasMany,
    lead: Field::Boolean,
    special_requests: Field::Text,
    min_hours: Field::Number,
    max_hours: Field::Number,
    min_shift: Field::Number,
    max_shift: Field::Number,
    target_hours: Field::Number,
    meal_plan: Field::Boolean,
    role: Field::Select.with_options(collection: User.roles.keys)
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = %i(
    first_name
    last_name
    branch
  ).freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = %i(
    first_name
    last_name
    id
    username
    email
    branch
    user_preferred_locations
    availabilities
    lead
    special_requests
    min_hours
    max_hours
    min_shift
    max_shift
    target_hours
    meal_plan
    role
  ).freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = %i(
    first_name
    last_name
    username
    email
    branch
    user_preferred_locations
    availabilities
    lead
    special_requests
    min_hours
    max_hours
    min_shift
    max_shift
    target_hours
    meal_plan
    role
  ).freeze

  # COLLECTION_FILTERS
  # a hash that defines filters that can be used while searching via the search
  # field of the dashboard.
  #
  # For example to add an option to search for open resources by typing "open:"
  # in the search field:
  #
  #   COLLECTION_FILTERS = {
  #     open: ->(resources) { where(open: true) }
  #   }.freeze
  COLLECTION_FILTERS = {}.freeze

  # Overwrite this method to customize how users are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(user)
    "#{user.first_name} #{user.last_name} (#{user.username})"
  end
end
