$( document ).ready(function() {
  $('#user_preferred_location_no_preference').click(function() {
      let checked = $('#user_preferred_location_no_preference').prop('checked');
      if(checked) {
          $('.location_pref_obj').prop('disabled', true);
          $('.location_pref_obj').attr('aria-disabled', true);
          $('.location_pref_obj').css('opacity', 0.5);
      } else {
          $('.location_pref_obj').removeAttr('disabled');
          $('.location_pref_obj').removeAttr('aria-disabled');
          $('.location_pref_obj').removeAttr('style');
      }
  });
});
