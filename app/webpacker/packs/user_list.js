import { sort_users_list } from '../components/user_list.js.erb'
import { sort_by_url } from '../components/user_list.js.erb'
import { sort_cards } from '../components/user_list.js.erb'
import { update_card_urls } from '../components/user_list.js.erb'
import { update_chair_urls } from '../components/user_list.js.erb'
import { visible } from '../components/user_list.js.erb'

$( document ).ready( function () {
  // special request icon expand/collapse
  let icons = document.getElementsByClassName("special-request-icon");
  icons = Array.from(icons).filter(x => visible(x));
  for (let i = 0; i < icons.length; i++) {
    icons[i].style.zIndex = "1";
    let user_index = icons[i].id.split('-')[1];
    let request_div = document.getElementById(`special-request-${user_index}`);
    icons[i].addEventListener('click', function(e){
      document.getElementsByClassName('users-list').innerHTML += "clicked"
      if (request_div.classList.contains('collapse')){
        request_div.classList.remove('collapse');
      } else {
        request_div.classList.add('collapse');
      }
      e.preventDefault();
    });
  };

  //sorting 
  // If sorting parameter present in url
  sort_by_url()

  $('#filter-dropdown').on("change", function(){
    // When new option in dropdown selected
    var sortBy = this.options[this.selectedIndex].value //Get parameter to sort by from dropdown
    let url = new URL(window.location);
    url.searchParams.set('sort-by', sortBy);
    sort_cards(sortBy, url.searchParams.has('chair')) //Sort user cards by parameter
    update_chair_urls(sortBy);  
  })

});
