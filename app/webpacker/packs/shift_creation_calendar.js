import { mousedown_cal, mouseover_cal, mouseup_cal, mouseleave_cal } from '../components/paint_calendar.js.erb'
import { collectHighlighted } from '../components/submit_calendar.js.erb'

// add event listeners for painting

if (!$( document ).attr('calendar_listeners')) {
    $( document ).on("mousedown", "#sched-panel td[id]", function () {
        mousedown_cal(this);
    });

    $( document ).on("mouseover", "#sched-panel td[id]", function () {
        mouseover_cal(this);
    });

    $( document ).on("mouseup", "#sched-panel #cal_div", mouseup_cal)
    $( document ).on("mouseleave", "#sched-panel #cal_div", mouseleave_cal)

    // add form submission function
    $( document ).on("submit","#shift-creation-form", collectHighlighted)

    $( document ).attr('calendar_listeners', true)
}



