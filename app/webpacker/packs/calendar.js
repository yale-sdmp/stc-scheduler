import { updateHours } from '../components/live_hours_readout.js.erb'
import { mousedown_cal, mouseover_cal, mouseup_cal, mouseleave_cal } from '../components/paint_calendar.js.erb'
import { collectHighlighted } from '../components/submit_calendar.js.erb'


// add event listeners for painting

if (!$( document ).attr('calendar_listeners')) {
    $( document ).on("mousedown", "td[id]", function () {
        mousedown_cal(this);
        updateHours()
    });

    $( document ).on("mouseover", "td[id]", function () {
        mouseover_cal(this);
        updateHours()
    });

    $( document ).on("mouseup", "#cal_div", mouseup_cal)
    $( document ).on("mouseleave", "#cal_div", mouseleave_cal)

    // initialize the live hours readout 
    $( document ).on('turbolinks:load', function () {
	if ($('#cal_div').length > 0) { updateHours() }
    });

    // add form submission function
    $( document ).on("submit","#availabilities_form", collectHighlighted)

    $( document ).attr('calendar_listeners', true) 
}


