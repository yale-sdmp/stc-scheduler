# frozen_string_literal: true

# Pundit policy for shifts
class ShiftPolicy < ApplicationPolicy
  def calendar_create?
    user.admin? && record.chair.location.branch == user.branch
  end

  def deputy_export?
    user.admin?
  end
end
