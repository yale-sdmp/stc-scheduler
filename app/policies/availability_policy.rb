# frozen_string_literal: true

# Pundit policy for availabilities
class AvailabilityPolicy < ApplicationPolicy
  def self_or_admin?
    (user == record.user) || user.admin?
  end

  def index?
    true
  end

  def new?
    user.student? && !user.submitted
  end

  def create?
    new?
  end

  def calendar?
    user.student? && !user.submitted
  end

  def calendar_create?
    calendar?
  end

  def destroy?
    self_or_admin?
  end

  def availabilities_csv?
    user.admin?
  end

  def delete_all?
    user.admin?
  end
end
