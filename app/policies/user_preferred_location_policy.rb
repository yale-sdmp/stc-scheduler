# frozen_string_literal: true

# Pundit policy for user preferred locations
class UserPreferredLocationPolicy < ApplicationPolicy
  def new?
    user.student? && !user.submitted
  end

  def create?
    new?
  end
end
