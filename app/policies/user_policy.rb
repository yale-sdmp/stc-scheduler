# frozen_string_literal: true

# Pundit policy for users
class UserPolicy < ApplicationPolicy
  def self_or_admin?
    (user == record) || user.admin?
  end

  delegate :admin?, to: :user

  def index?
    self_or_admin?
  end

  def show?
    admin?
  end

  def destroy?
    admin?
  end

  def edit?
    user == record && (!user.submitted || user.admin?)
  end

  def update?
    edit?
  end

  def submit?
    self_or_admin?
  end
end
