# frozen_string_literal: true

# Controller to manage user preferred locations
class UserPreferredLocationsController < ApplicationController
  prepend_before_action :setup_user

  def new
    set_upls if @user.branch.present?
  end

  def create
    obj = UserPreferredLocationCreator.new(params: upl_params, user: @user)
    if obj.call
      redirect_to calendar_availabilities_path,
                  notice: 'Successfully saved location preferences!'
    else
      flash_alerts(obj)
      @upls = obj.upls
      render 'new'
    end
  end

  private

  def setup_user
    @user = current_user
  end

  def set_upls
    if @user.branch&.ct?
      set_ct_upl
    else
      set_other_upls
    end
  end

  def set_ct_upl
    @upls = helpers.get_preferred_location(@user)
  end

  # for STs and MTs
  def set_other_upls
    # upls already exist
    @upls = UserPreferredLocation.where(user: @user).order(:location_id)
    return unless @upls.length != @user.branch.locations.length

    # need to initialize new upls
    @upls = @user.branch.locations.map do |l|
      UserPreferredLocation.new(location: l, preferred_hours: 0)
    end
  end

  def upl_params
    params.require(:user_preferred_location).permit(
      :no_preference, branch_specific_params
    )
  end

  def branch_specific_params
    # CTs do not provide preferred hours
    return :location_id if @user.branch.ct?

    # for each location, permit location_id => preferred_hours
    @user.branch.locations.map { |loc| [loc.id.to_s, :preferred_hours] }.to_h
  end

  def authorize!
    authorize UserPreferredLocation.new
  end
end
