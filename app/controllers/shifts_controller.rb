# frozen_string_literal: true

# Controller to manage scheduled shifts
class ShiftsController < ApplicationController
  def calendar_create
    scc = ShiftCalendarCreator.new(params)

    redirect_url = scheduling_admin_dashboard_index_url(location: scc.location,
                                                        chair: scc.chair,
                                                        user: scc.user)
    if scc.call
      redirect_to redirect_url, notice: 'Successfully created shift(s)!'
    else
      redirect_to redirect_url, alert: scc.errors.full_messages.join(', ')
    end
  end

  def deputy_export
    dep_exp = DeputyCsvGenerator.new(current_user.branch)

    if dep_exp.call
      send_data(dep_exp.csv_data, filename: 'deputy_export.csv')
    else
      flash[:notice] = dep_exp.error
    end
  end

  def authorize!
    authorize Shift.new(chair_id: params[:chair], user: current_user)
  end
end
