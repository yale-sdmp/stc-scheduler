# frozen_string_literal: true

# Controller for admin dashboard
class AdminDashboardsController < ApplicationController
  # rubocop:disable Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
  def scheduling
    locs = Location.where(branch: current_user.branch)
    @location = locs.find_by(id: params[:location])
    @selected_chair = @location.chairs.find_by(id: params[:chair]) if @location
    @location ||= locs.first

    @users = helpers.users_sorted_by_availability(@location, @selected_chair)

    @selected_user = @location.branch.users.find_by(id: params[:user])
    @availabilities = @selected_user.availabilities if @selected_user
    @scheduled_users = (@selected_chair&.shifts || []).map(&:user).uniq
    return if @scheduled_users.include?(@selected_user)

    @scheduled_users << @selected_user if @selected_user
  end

  # rubocop:enable Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity

  def heatmap
    @all_users = User.where(branch_id: current_user.branch_id)
    return unless params.key?(:slot_id)

    @slot = Slot.from_id(params[:slot_id])
    @available_students = []
    @all_users.each do |s|
      @available_students << s if s.covers_timeslot?(@slot)
    end
  end

  def users
    @users = User.where(branch_id: current_user.branch_id)
    return unless params.key?(:user_id)

    @selected_user = User.find(params[:user_id])
    @availabilities = Availability.where(user_id: params[:user_id])
  end

  def authorize!
    authorize (current_user || User.new), :admin?
  end
end
