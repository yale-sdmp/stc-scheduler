# frozen_string_literal: true

# Controller to manage all user information
class DashboardsController < ApplicationController
  def index
    @user = current_user
  end

  def unauthenticated; end

  def unsubmitted_report
    authorize current_user, :admin?
    @unsubmitted_users = User.where(branch: current_user.branch,
                                    submitted: false)
  end

  def authorize!; end

  # rubocop:disable Metrics/MethodLength
  def email_incomplete
    find_incomplete_users
    unsent_emails = []
    @incomplete_users.each do |user|
      UserMailer.email_incomplete_user(user: user).deliver
    rescue StandardError
      unsent_emails.append(user.email)
    end
    if unsent_emails.empty?
      flash.notice = 'Successfully emailed students!'
    else
      flash.alert = "Failed to deliver emails to #{unsent_emails.join(', ')}"
    end
    redirect_to root_path
  end
  # rubocop:enable Metrics/MethodLength

  private

  def find_incomplete_users
    @incomplete_users = User.all.where(role: 'student',
                                       branch: current_user.branch,
                                       submitted: false)
  end
end
