# frozen_string_literal: true

# controller for checking if API and application are live
class HeartbeatController < ApplicationController
  # Route for checking if the application is live
  def show; end
end
