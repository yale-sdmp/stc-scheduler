# frozen_string_literal: true

# Base class for all controllers
class ApplicationController < ActionController::Base
  # to be used when rendering, not redirecting
  include Pundit

  before_action :authorize!, unless: :public_action?
  rescue_from Pundit::NotAuthorizedError do
    flash[:alert] = 'Sorry, you don\'t have permission to do that.'
    redirect_to(root_path)
  end

  def flash_alerts(instance)
    instance.errors.full_messages.each do |message|
      flash.now[:alert] = message
    end
  end

  def after_sign_in_path_for(user)
    return root_path unless user.student?

    user.submitted ? root_path : edit_user_path(user)
  end

  private

  def public_action?
    devise_controller? || instance_of?(HeartbeatController)
  end
end
