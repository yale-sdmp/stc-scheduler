# frozen_string_literal: true

module Admin
  # Administrate controller for shift customized to:
  #  - allow sorting by user's name
  class ShiftsController < Admin::ApplicationController
    # rubocop:disable Metrics/MethodLength, Metrics/AbcSize
    def index
      authorize_resource(resource_class)
      search_term = params[:search].to_s.strip
      resources = Administrate::Search.new(scoped_resource,
                                           dashboard_class,
                                           search_term).run
      resources = apply_collection_includes(resources)

      resources = user_ordered_resources(resources, order)

      resources = resources.page(params[:_page]).per(records_per_page)
      page = Administrate::Page::Collection.new(dashboard, order: order)

      render locals: {
        resources: resources,
        search_term: search_term,
        page: page,
        show_search_bar: show_search_bar?
      }
    end
    # rubocop:enable Metrics/MethodLength, Metrics/AbcSize

    # orders shifts by user's name (last, first) if list is sorted by user
    def user_ordered_resources(resources, order)
      if order.ordered_by?(:user)
        user_order = User.order(last_name: order.direction,
                                first_name: order.direction)
        resources.joins(:user).merge(user_order)
      else
        order.apply(resources)
      end
    end
  end
end
