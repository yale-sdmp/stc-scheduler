# frozen_string_literal: true

# controller for Availability
class AvailabilitiesController < ApplicationController
  prepend_before_action :set_availability
  before_action :set_availabilities, only: %i(index new)

  def index; end

  def new
    @availability = Availability.new
  end

  def create
    obj = AvailabilityCreator.new(params: availability_params)
    if obj.call
      redirect_to new_availability_path,
                  notice: 'Successfully added availability!'
    else
      @availability = Availability.new
      set_availabilities
      flash_alerts(obj)
      render 'new'
    end
  end

  def calendar_create
    # availableIds is an array of cell_ids as strings
    ac = AvailabilityCalendarCreator.new(params['highlightedIds'], current_user)
    if ac.call
      redirect_to dashboards_path,
                  notice: 'Succesfully submitted hours!'
    else
      redirect_to calendar_availabilities_path,
                  alert: ac.errors.full_messages.join(', ')
    end
  end

  def calendar
    @avails_hash = helpers.generate_avails_hash(current_user&.availabilities)
  end

  def destroy
    redirect_path = request.referer || availabilities_path
    if @availability.destroy
      redirect_to redirect_path,
                  notice: 'Successfully deleted availability!'
    else
      redirect_to redirect_path,
                  alert: 'Failed to delete availability'
    end
  end

  def availability_params
    params.require(:availability).permit(:day, :start_time, :end_time)
          .merge(user: current_user)
  end

  def availabilities_csv
    export_users = User.where(branch: current_user.branch)
    acg = AvailabilityCsvGenerator.new(export_users)

    if acg.call
      send_data(acg.csv_data,
                filename: 'availabilities.csv')
    else
      flash[:notice] = acg.error
    end
  end

  def delete_all
    if BulkAvailabilitiesDestroyer.new(current_user).call
      redirect_to root_url, notice: 'Successfully deleted all availabilities!'
    else
      redirect_to root_url, alert: 'Failed to delete all availabilities.'
    end
  end

  def set_availability
    @availability = if params[:id]
                      Availability.find(params[:id])
                    else
                      Availability.new
                    end
  end

  def set_availabilities
    @availabilities = current_user&.availabilities&.order('day ASC,
                                                           start_time ASC')
  end

  def authorize!
    authorize @availability
  end
end
