# frozen_string_literal: true

# Controller to manage all user information
class UserController < ApplicationController
  prepend_before_action :set_user, only: %i(show edit update submit authorize!)

  def show; end

  def edit; end

  def update
    if @user.update(user_params)
      redirect =
        @user.student? ? new_user_preferred_location_path : user_path(@user)
      redirect_to redirect, notice: 'Successfully added information!'
    else
      flash_alerts(@user)
      render 'edit'
    end
  end

  def destroy; end

  def submit
    if @user.update(submitted: true)
      redirect_to dashboards_path, notice: 'Successfully submitted information!'
    else
      redirect_to dashboards_path, alert: 'Could not submit information.'
    end
  end

  def authorize!
    authorize(@user || User.new)
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user)
          .permit(%i(first_name last_name branch_id min_hours max_hours
                     target_hours min_shift max_shift lead meal_plan
                     special_requests email))
  end
end
