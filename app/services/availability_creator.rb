# frozen_string_literal: true

# Service object to create an Availability
class AvailabilityCreator
  include ActiveModel::Model
  attr_reader :availability

  # Initialize an AvailabilityCreator
  #
  # @param params [ActionController::Parameters] parameters for availability
  def initialize(params:)
    @user = params[:user]
    @start_time = params[:start_time].to_i
    @end_time = params[:end_time].to_i
    @day = params[:day]
  end

  def call
    create
  rescue ActiveRecord::RecordInvalid, StandardError => e
    errors.add(:base, e.message)
    false
  end

  def create
    validate_times
    handle_adjacent

    ActiveRecord::Base.transaction do
      @availability = Availability.new(user: @user, start_time: @start_time,
                                       end_time: @end_time, day: @day)
      @availability.save!
    end
  end

  def validate_times
    # check to make sure params are present before validating
    return unless @start_time && @end_time && @user

    raise StandardError, 'Start time must be before end time' if invalid_start?

    overlap_error = 'Availability overlaps with existing availability'
    raise StandardError, overlap_error if overlap?
  end

  # returns true if availability params overlap with existing availability
  def overlap?
    @user.availabilities.where(day: @day).each do |a|
      time_overlap = @start_time.between?(a.start_time, a.end_time - 1) || \
                     @end_time.between?(a.start_time + 1, a.end_time)
      return true if time_overlap
    end
    false
  end

  def handle_adjacent
    @user.availabilities.where(day: @day).each do |a|
      merge(a, a.start_time, @end_time) if @start_time == a.end_time
      merge(a, @start_time, a.end_time) if @end_time == a.start_time
    end
  end

  # updates start and end time and deletes old availability to merge
  def merge(old_availability, new_start, new_end)
    @start_time = new_start
    @end_time = new_end
    old_availability.destroy
  end

  # returns true if availability start_time is after or equal to end_time
  def invalid_start?
    @start_time >= @end_time
  end
end
