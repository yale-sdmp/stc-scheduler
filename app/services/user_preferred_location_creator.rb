# frozen_string_literal: true

# Service object to create User Preferred Location records
class UserPreferredLocationCreator
  include ActiveModel::Model
  # the upls generated from the given params
  # for CTs, only the upl for the preferred location
  attr_accessor :upls

  # Params
  # any branch: "no_preference" => "1" or "0"
  # CT: "location_id"=> location_id
  # ST/MT: {location_id =>
  #   {"preferred_hours"=> preferred_hours}}, for all branch locations
  def initialize(params:, user:)
    @params = params
    @user = user
  end

  def call
    create
  rescue ActiveRecord::RecordInvalid, StandardError => e
    errors.add(:base, e.message)
    false
  end

  private

  def create
    return true if check_no_preference

    if @user.branch.ct?
      ActiveRecord::Base.transaction { ct_create }
    else
      ActiveRecord::Base.transaction { stmt_create }
    end
  end

  # Method for Cluster Techs
  # for the given preferred location, create a upl with hours as user.max_hours
  # for the remaining locations, create a upl with hours set to 0
  def ct_create
    @upls = @user.user_preferred_locations.find_or_initialize_by(@params)
    @upls.update!(preferred_hours: @user.max_hours)
    @user.branch.locations.each do |loc|
      next unless loc != @upls.location

      upl = @user.user_preferred_locations
                 .find_or_initialize_by(location_id: loc.id)
      upl.update!(preferred_hours: 0)
    end
    true
  end

  # Method for Student/Media Techs
  def stmt_create
    @upls = []
    @params.each do |loc_id, pref_hours|
      x = @user.user_preferred_locations
               .find_or_initialize_by(location_id: loc_id)
      x.assign_attributes(pref_hours)
      @upls << x
    end
    validate_total_hours
    @upls.each(&:save!)
  end

  def check_no_preference
    if @params[:no_preference] == '1'
      @user.user_preferred_locations.destroy_all
      true
    else
      @params = @params.except(:no_preference)
      false
    end
  end

  def sum_hours
    @params.values.sum { |h| h[:preferred_hours].to_i }
  end

  def validate_total_hours
    check_min_hours
    check_max_hours
  end

  def check_min_hours
    return unless @user.min_hours.present? && sum_hours < @user.min_hours

    error = 'Total number of requested hours must not be less than minimum requested hours.'
    raise StandardError, error
  end

  def check_max_hours
    return unless @user.max_hours.present? && sum_hours > @user.max_hours

    error = 'Total number of requested hours must not be greater than maximum requested hours.'
    raise StandardError, error
  end
end
