# frozen_string_literal: true

# Service object for deleting all availabilities
class BulkAvailabilitiesDestroyer
  include ActiveModel::Model

  def initialize(user)
    @user = user
  end

  def call
    branch_users.destroy_all
    true
  end

  def branch_users
    Availability.where(user: @user.branch&.users)
  end
end
