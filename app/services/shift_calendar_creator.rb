# frozen_string_literal: true

# Service object to create Availabilities from calendar submission
class ShiftCalendarCreator
  include ActiveModel::Model
  attr_reader :user, :chair, :location

  def initialize(params)
    @slots = params['highlightedIds']&.map { |x| Slot.from_id(x) }&.sort || []
    @user = User.find(params[:user])
    @chair = Chair.find(params[:chair])
    @location = Location.find(params[:location])
  end

  def call
    if @slots.empty?
      errors.add(:base, 'Please select times on the shift calendar')
      return false
    end
    create
  end

  private

  def create
    merged_slots.reduce(true) do |tf, slot|
      sc = ShiftCreator.new(user: @user,
                            chair: @chair,
                            location: @location,
                            slot: slot)
      ret_tf = tf && sc.call
      sc.errors.full_messages.each { |err| errors.add(:base, err) }
      ret_tf
    end
  end

  def merged_slots
    merged = [@slots.first]
    (@slots[1..] || []).each do |slot|
      if slot.adjacent?(merged[-1])
        merged[-1] = merged[-1].merge(slot)
      else
        merged << slot
      end
    end

    merged
  end
end
