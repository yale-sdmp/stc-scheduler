# frozen_string_literal: true

require 'csv'
require 'date'

# service object for generating availability CSVs
class AvailabilityCsvGenerator
  include ActiveModel::Model

  attr_accessor :export_users, :csv_data

  validates :export_users,
            presence:
              { message:
                  'must exist their availabilities can be exported' }

  def initialize(export_users)
    @export_users = export_users
  end

  def call
    return false unless valid?

    generate_csv
    true
  end

  def slots
    slot_length = 15
    days = Date::DAYNAMES
    slot_arr = days.map do |day|
      start_slot = Slot.new(day: day, start: 800, length: slot_length)
      end_start = 2360 - slot_length
      end_slot = Slot.new(day: day, start: end_start, length: slot_length)

      (start_slot..end_slot).to_a
    end
    slot_arr.flatten
  end

  def generate_csv
    names = @export_users.map { |u| "#{u.first_name} #{u.last_name}" }
    @csv_data = CSV.generate do |csv|
      csv << (['Slot'] + names)
      slots.each do |slot|
        yes_no = @export_users.map do |u|
          u.covers_timeslot?(slot) ? 'YES' : 'NO'
        end
        csv << ([slot.to_s] + yes_no)
      end
    end
  end
end
