# frozen_string_literal: true

require 'csv'
require 'date'

# service object for generating CSVs of shifts to import into Deputy
class DeputyCsvGenerator
  include ActiveModel::Model

  attr_accessor :branch, :csv_data

  validates :branch, presence: { message: 'Branch must exist' }

  def initialize(branch)
    @branch = branch
  end

  def call
    return false unless valid?

    generate_csv
    true
  end

  private

  def generate_csv
    shifts = []
    @branch.users.each { |user| shifts += user.shifts }

    @csv_data = CSV.generate do |csv|
      csv << ['Start Time', 'End Time', 'Area', 'Location', 'Meal break',
              'Employee']
      shifts.each do |shift|
        csv << shift_data(shift)
      end
    end
  end

  # @param time_int [Integer] time in 24 hr format (i.e. 1315 = 1:15 PM)
  # @param day [String] day name
  # Returns the given time and date in the format required for Deputy
  # (yyyy-mm-dd hh:mm:ss) for that day the coming week
  def format_time(time_int, day)
    day_num = Shift.days[day]
    coming_sun = DateTime.now + (7 - DateTime.now.wday)
    datetime = (coming_sun + day_num).change(hour: time_int / 100,
                                             min: time_int % 100)
    datetime.strftime('%Y-%m-%d %H:%M:%S')
  end

  # returns shift data formatted as a Deputy csv row (array)
  def shift_data(shift)
    start_time = format_time(shift.start_time, shift.day)
    end_time = format_time(shift.end_time, shift.day)
    area = shift.user.deputy_role&.title
    location = shift.chair.location.name
    name = shift.user.full_name
    [start_time, end_time, area, location, 0, name]
  end
end
