# frozen_string_literal: true

# Service object to create Availabilities from calendar submission
class ShiftCreator
  include ActiveModel::Model

  def initialize(user:, chair:, location:, slot:)
    @user = user
    @chair = chair
    @location = location
    @slot = slot
  end

  def call
    create
    true
  rescue ActiveRecord::RecordInvalid, StandardError => e
    errors.add(:base, e.message)
    false
  end

  def create
    check_time
    check_available
    check_adjacent
    merge_adjacent
  end

  def check_time
    return if @chair.covers_timeslot?(@slot)

    raise StandardError, 'Shift is not within chair'
  end

  def check_available
    if @user.scheduled?(@slot.to_id)
      raise StandardError,
            "#{@user.full_name} is already scheduled during this time"
    end

    return if @user.covers_timeslot?(@slot)

    raise StandardError,
          "#{@user.full_name} is not available during this time"
  end

  def check_adjacent
    adjacents = @user.shifts.select { |x| Slot.from_shift(x).adjacent?(@slot) }

    return unless adjacents.any? { |x| x.chair.location != @location }

    raise StandardError,
          "#{@user.full_name} has an adjacent shift at another location"
  end

  def merge_adjacent
    adjacents = @user.shifts.select { |x| Slot.from_shift(x).adjacent?(@slot) }
    shift = Shift.new(start_time: @slot.int,
                      end_time: @slot.time_to_int(@slot.end_time),
                      day: @slot.day,
                      chair: @chair,
                      user: @user)
    adjacents.each do |adj|
      shift = merge(adj, shift)
    end
    shift.save!
  end

  def merge(shift1, shift2)
    start_time = [shift1, shift2].map(&:start_time).min
    end_time = [shift1, shift2].map(&:end_time).max
    shift1.update(start_time: start_time, end_time: end_time)
    shift2.destroy
    shift1
  end
end
