# frozen_string_literal: true

# Service object to create Availabilities from calendar submission
class AvailabilityCalendarCreator
  include ActiveModel::Model
  # @param params [Array<String>] a list of cell_ids as strings
  def initialize(params, user)
    @slots = params&.map { |x| Slot.from_id(x) }&.sort || []
    @user = user
  end

  def call
    return true if @slots.empty?

    merged_slots.reduce(true) do |tf, slot|
      ac = AvailabilityCreator.new(params: slot_to_params(slot))
      ret_tf = tf && ac.call
      ac.errors.full_messages.each { |err| errors.add(:base, err) }
      ret_tf
    end
  end

  private

  def slot_to_params(slot)
    params = { day: slot.day }
    params[:user] = @user
    params[:start_time] = slot.time_to_int(slot.start_time)

    params[:end_time] = slot.time_to_int(slot.end_time)
    params
  end

  def merged_slots
    merged = [@slots.first]
    (@slots[1..] || []).each do |slot|
      if slot.adjacent?(merged[-1])
        merged[-1] = merged[-1].merge(slot)
      else
        merged << slot
      end
    end

    merged
  end
end
