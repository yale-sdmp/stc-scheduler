# frozen_string_literal: true

# The model for a block of available time for a student
class Availability < ApplicationRecord
  belongs_to :user

  # validates day, start time, and end time
  validates :day, presence: true
  validates :start_time, presence: true
  validates :end_time, presence: true

  # @param slot [Slot]
  # returns true iff the availability completely covers the given slot, meaning
  # start_time is less than or equal to the start of the slot and
  # end_time is greater than or equal to the end of the slot
  def covers_timeslot?(slot)
    (start_time..end_time).cover?(slot.range) && (slot.day == day)
  end

  # returns an array of Slots that make up the availability
  def slots
    slot_start = Slot.new(day: day,
                          start: start_time,
                          length: MINUTE_STEP)
    slot_end = Slot.new(day: day,
                        start: [end_time, 2360 - MINUTE_STEP].min,
                        length: MINUTE_STEP)
    (slot_start..slot_end).to_a.select { |slot| covers_timeslot?(slot) }
  end

  def length
    hr_diff = (end_time / 100) - (start_time / 100)
    min_diff = (end_time % 100) - (start_time % 100)
    (60 * hr_diff) + min_diff
  end
end
