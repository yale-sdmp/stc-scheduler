# frozen_string_literal: true

# The model for student workers and administrators
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable, :omniauthable,
  # :recoverable, :rememberable, :validatable
  devise :cas_authenticatable

  belongs_to :branch, optional: true
  belongs_to :deputy_role, optional: true

  validates :username, presence: true
  validates :first_name, :last_name, :email, :branch, presence: { on: :update }

  validates :email, uniqueness: { case_sensitive: false }, allow_blank: true # rubocop: disable Rails/UniqueValidationWithoutIndex
  validates :branch, presence: true, on: :update
  validates :special_requests, length: { maximum: 150 }

  validate :deputy_role_must_belong_to_same_branch
  enum role: { student: 0, admin: 1 }

  def full_name
    "#{first_name} #{last_name}"
  end

  # student-specific logic
  has_many :user_preferred_locations, dependent: :destroy
  has_many :availabilities, dependent: :destroy
  has_many :shifts, dependent: :destroy
  has_many :chairs, through: :shifts

  with_options if: :student? do
    validates :min_hours, :max_hours, :target_hours,
              :min_shift, :max_shift, presence: { on: :update }

    validate :min_hours_less_than_max
    validate :min_shift_less_than_max
    validate :target_hours_between_min_max

    after_update :destroy_upls_if_necessary
  end

  # since a user's upls depend on branch, min_hours, and max_hours,
  # if any of those fields change, destroy user's upls
  def destroy_upls_if_necessary
    if saved_change_to_branch_id? || saved_change_to_min_hours? ||
       saved_change_to_max_hours?
      user_preferred_locations.destroy_all
    end
  end

  # takes a Slot object and returns true iff the User has an availability
  # that covers the entirety of the slot
  def covers_timeslot?(slot)
    availabilities.any? { |x| x.covers_timeslot?(slot) }
  end

  def min_hours_less_than_max
    return if min_hours.blank?
    return unless max_hours.present? && min_hours > max_hours

    errors.add(:min_hours, 'must not be greater than max hours')
  end

  def min_shift_less_than_max
    return if min_shift.blank?
    return unless max_shift.present? && min_shift > max_shift

    errors.add(:min_shift, 'must not be greater than max shift')
  end

  def target_hours_between_min_max
    return if target_hours.blank?

    return unless (min_hours.present? && target_hours < min_hours) ||
                  (max_hours.present? && target_hours > max_hours)

    errors.add(:target_hours, 'must be between min hours and max hours')
  end

  def mins_covered
    total = 0
    shifts.all.find_each do |shift|
      total += shift.length
    end
    total
  end

  # @param chair [Chair] returns true iff the user is available at any
  # point during the time range specified by chair.
  def available_during?(chair)
    (chair.slots.any? { |slot| covers_timeslot?(slot) }) &&
      (chair.location.branch == branch)
  end

  # @param id [String] time slot id in day-24hrtime format
  # (e.g. '0-800' for Sunday at 8 a.m.)
  # returns true iff the user has a shift that covers the given timeslot
  def scheduled?(id)
    s = Slot.from_id(id)
    shifts.filter { |shift| shift.covers_timeslot?(s) }[0]
  end

  def deputy_role_must_belong_to_same_branch
    return unless deputy_role

    return if deputy_role.branch == branch

    errors.add(:deputy_role, 'must have the same branch as the user')
  end
end
