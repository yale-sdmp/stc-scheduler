# frozen_string_literal: true

# The model for a staffing need over a period of time
class Chair < ApplicationRecord
  belongs_to :location

  has_many :shifts, dependent: :destroy
  has_many :users, through: :shifts

  validates :start_time, :end_time, presence: true

  def user_min
    self[:user_min] || location&.user_min
  end

  def user_max
    self[:user_max] || location&.user_max
  end

  def covers_timeslot?(slot)
    (start_time..end_time).cover?(slot.range) && (slot.day == day)
  end

  def slots
    slot_start = Slot.new(day: day,
                          start: start_time,
                          length: MINUTE_STEP)
    slot_end = Slot.new(day: day,
                        start: [end_time, 2360 - MINUTE_STEP].min,
                        length: MINUTE_STEP)
    (slot_start..slot_end).to_a.select { |slot| covers_timeslot?(slot) }
  end

  # returns EMPTY     if no shifts are scheduled
  #         PARTIAL   if some are scheduled
  #         SATISFIED if all scheduling needs are met
  def filled_status
    return :empty if shifts.empty?

    return :partial if location.lead_only && users.none?(&:lead)

    return filled_continuous if location.continuous

    filled_non_continuous
  end

  # takes an array of users and returns the number of slots within the chair
  # that have the minimum number of users scheduled from the given set of users
  def filled_slots(users)
    filled = 0
    slots.each do |slot|
      filled += 1 if users.filter { |u| u.scheduled?(slot.to_id) }.size >= user_min
    end
    filled
  end

  # takes an array of users and returns an array of slots containing the slots
  # within the chair that are not fully staffed by the given users
  def unfilled_slots(users)
    slots.filter do |slot|
      users.filter { |u| u.scheduled?(slot.to_id) }.size < user_min
    end
  end

  # takes an array of users and the returns of the number of users within the
  # array that are availble and unscheduled during any unfilled slot within
  # the chair--i.e. the number of users which could help fill in the gaps
  def num_available(users)
    avail_users = unfilled_slots(users).map do |slot|
      users.filter do |u|
        u.covers_timeslot?(slot) & !u.scheduled?(slot.to_id)
      end
    end
    avail_users.flatten.uniq.size
  end

  # returns the total number of scheduled hours for this chair
  def tot_hrs
    shifts.map(&:length).sum / 60.0
  end

  # returns the total number of the scheduled lead hours for this chair
  def lead_hrs
    shifts.filter { |s| s.user.lead? }.map(&:length).sum / 60.0
  end

  private

  # checks filled status for continuous chairs
  def filled_continuous
    return :partial if location.lead_needed && !covered_by?(shifts.select(&:lead?))

    return :partial unless covered_by?(shifts, user_min)

    :satisfied
  end

  # checks filled status for non continuous chairs
  def filled_non_continuous
    if location.lead_needed
      lead_slots = shifts_by_time(shifts.select(&:lead?))
      all_slots = shifts_by_time

      (0...lead_slots.length).each do |i|
        return :partial if lead_slots[i].empty? && all_slots[i].present?
      end

    end
    :satisfied
  end

  # takes an array of shifts (all of the chair's shifts by default) and
  # returns an array in which the ith entry contains an array of the shifts
  # that the ith slot in the chair
  def shifts_by_time(shifts_ = shifts)
    slots.map { |slot| shifts_.select { |shift| shift.covers_timeslot?(slot) } }
  end

  # takes an array of shifts (all of the chair's shifts by default) and
  # the minimum number of users required at all times
  # returns true iff each slot is covered by at least user_min shifts
  def covered_by?(shifts_, user_min = 1)
    shifts_by_time(shifts_).all? { |shfts| shfts.size >= user_min }
  end
end
