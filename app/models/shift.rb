# frozen_string_literal: true

# The model for a shift being worked by a user
class Shift < ApplicationRecord
  belongs_to :user
  belongs_to :chair

  validates :start_time, :end_time, presence: true
  validates :user_id, :chair_id, presence: true

  def length
    hr_diff = (end_time / 100) - (start_time / 100)
    min_diff = (end_time % 100) - (start_time % 100)
    (60 * hr_diff) + min_diff
  end

  # @param slot [Slot]
  # returns true iff the availability completely covers the given slot, meaning
  # start_time is less than or equal to the start of the slot and
  # end_time is greater than or equal to the end of the slot
  def covers_timeslot?(slot)
    (start_time..end_time).cover?(slot.range) && (slot.day == day)
  end

  delegate :lead?, to: :user
end
