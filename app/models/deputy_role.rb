# frozen_string_literal: true

# A model for storing roles for Deputy export
class DeputyRole < ApplicationRecord
  has_many :users, dependent: :nullify
  belongs_to :branch

  validates :title, uniqueness: { scope: [:branch_id] } # rubocop: disable Rails/UniqueValidationWithoutIndex
end
