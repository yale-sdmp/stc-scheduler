# frozen_string_literal: true

# The class from which all models will inherit
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  enum day: { Sunday: 0, Monday: 1, Tuesday: 2,
              Wednesday: 3, Thursday: 4, Friday: 5, Saturday: 6 }
end
