# frozen_string_literal: true

# The model for a place that has staffing needs
class Location < ApplicationRecord
  # Set public_facing to true by default
  attribute :public_facing, :boolean, default: true
  belongs_to :branch
  has_many :chairs, dependent: :destroy
  has_many :user_preferred_locations, dependent: :destroy
  # Set public_facing to true by default
  validates :name, presence: true, uniqueness: { case_sensitive: false } # rubocop: disable Rails/UniqueValidationWithoutIndex
  validates :lead_needed, :lead_only, :continuous,
            inclusion: { in: [true, false] }
  validates :user_min, presence: true
end
