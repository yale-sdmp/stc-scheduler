# frozen_string_literal: true

# The model to hold student preferences for working location hours
class UserPreferredLocation < ApplicationRecord
  belongs_to :user
  belongs_to :location

  # rubocop: disable Rails/UniqueValidationWithoutIndex
  validates :location,
            uniqueness:
              { scope: :user_id,
                message:
                'For each user, only one record may exist per location.' }
  # rubocop: enable Rails/UniqueValidationWithoutIndex
  validates :preferred_hours, presence: true

  attr_accessor :no_preference
end
