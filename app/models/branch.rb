# frozen_string_literal: true

# The model for the branches of STC
class Branch < ApplicationRecord
  has_many :users, dependent: :nullify
  has_many :locations, dependent: :nullify
  has_many :deputy_roles, dependent: :nullify
  enum title: { mt: 0, ct: 1, st: 2, sdmp: 3 }

  validates :title, uniqueness: true # rubocop:disable Rails/UniqueValidationWithoutIndex

  def full_title
    title_names = { 'mt' => 'Media Techs', 'ct' => 'Cluster Techs',
                    'st' => 'Student Techs', 'sdmp' => 'SDMP' }
    title_names[title]
  end
end
