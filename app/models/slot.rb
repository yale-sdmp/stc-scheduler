# frozen_string_literal: true

# a class for handling time slots for availabilities
class Slot
  include Comparable
  attr_reader :day, :int, :length

  # takes the day on which the slot occurs, its starting time as an integer
  # (in the format 1300 for 1:00 PM for example), and the length (in minutes
  # of the slot
  def initialize(day:, start:, length: MINUTE_STEP)
    raise 'Slot received invalid day' unless Date::DAYNAMES.include?(day)
    raise 'Slot received invalid time' if start > 2359

    @day = day
    @int = start
    @length = length
  end

  # @param id [String] an id corresponding to a cell on the calendar
  # @return a new slot for that cell
  def self.from_id(id)
    start = id.split('-')[1].to_i
    day = Availability.days.invert[id.split('-')[0].to_i]
    new(day: day, start: start, length: MINUTE_STEP)
  end

  # @param chair [Chair]
  # converts chair into a slot with matching start, end, etc
  def self.from_chair(chair)
    strt = chair.start_time
    endt = chair.end_time
    diff_hr = (endt / 100) - (strt / 100)
    diff_min = (endt % 100) - (strt % 100)
    new(day: chair.day,
        start: strt,
        length: (60 * diff_hr) + diff_min)
  end

  def self.from_shift(shift)
    strt = shift.start_time
    endt = shift.end_time
    diff_hr = (endt / 100) - (strt / 100)
    diff_min = (endt % 100) - (strt % 100)
    new(day: shift.day,
        start: strt,
        length: (60 * diff_hr) + diff_min)
  end

  def day_index
    Date::DAYNAMES.index(@day)
  end

  def start_time
    { hr: @int / 100, min: @int % 100 }
  end

  def end_time
    end_hr = start_time[:hr] + ((start_time[:min] + @length) / 60)
    end_min = (start_time[:min] + @length) % 60
    { hr: end_hr, min: end_min }
  end

  def to_id
    "#{Availability.days[day]}-#{time_to_int(start_time)}"
  end

  def time_to_int(time)
    (100 * time[:hr]) + time[:min]
  end

  def time_to_str(time)
    format('%<hr>02d:%<min>02d', time)
  end

  def adjacent?(slot2)
    same_day = (day == slot2.day)
    adj_times = (end_time == slot2.start_time) || \
                (start_time == slot2.end_time)
    same_day && adj_times
  end

  def merge(slot2)
    raise 'Can only merge adjacent slots' unless adjacent?(slot2)

    new_start = [int, slot2.int].min
    new_length = length + slot2.length
    Slot.new(day: day, start: new_start, length: new_length)
  end

  def range
    (@int..time_to_int(end_time))
  end

  def succ
    Slot.new(day: @day, start: time_to_int(end_time), length: @length)
  end

  def to_s
    "#{@day[0..2]} #{time_to_str(start_time)}"
  end

  def <=>(other)
    return day_index <=> other.day_index if other.day_index != day_index

    @int <=> other.int
  end
end
