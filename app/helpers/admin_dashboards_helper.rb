# frozen_string_literal: true

# Helper methods for Admin Dashboard views
# rubocop:disable Metrics/ModuleLength
module AdminDashboardsHelper
  SELECTED_USER_CLASS = 'selected'

  def scheduling_calendar_styling(location, users)
    lambda do |id|
      chair = location.chairs.find { |c| c.covers_timeslot?(Slot.from_id(id)) }
      return {} unless chair

      ids = chair.slots.map(&:to_id)
      style = progress_style(chair, users) if id == ids.first

      { class: scheduling_calendar_classes(id, ids, users), style: style }
    end
  end

  def scheduling_calendar_classes(id, ids, users)
    classes = case id
              when ids.first then %w(chair top-of-chair)
              when ids.last then %w(chair bottom-of-chair)
              else %w(chair)
              end

    classes << 'no-avail-alert' if available_users_slot(users, id).empty?
    classes
  end

  # rubocop:disable Metrics/MethodLength
  def scheduling_calendar_inner_html(location, selected_user, users)
    lambda do |id|
      chair = location.chairs.find { |c| c.covers_timeslot?(Slot.from_id(id)) }
      opts = { location: location, user: selected_user }
      opts[:chair] = chair
      url = scheduling_admin_dashboard_index_url(opts)
      text = if chair && id == chair.slots.first.to_id
               progress_text(chair, users)
             else
               '&nbsp;'
             end

      return "<a href='#{url}' style='text-decoration:none;'>
                <div style='height:100%;width:100%'>#{text}</div>
              </a>".html_safe
    end
  end
  # rubocop:enable Metrics/MethodLength

  # text summarizing scheduling progress to display on chair calendar
  def progress_text(chair, users)
    if chair.location.continuous
      format('%<filled_slots>d/%<tot_slots>d filled (%<avail>d available)',
             filled_slots: chair.filled_slots(users),
             tot_slots: chair.slots.size,
             avail: chair.num_available(users))
    else
      format('%<tot_hrs>0.1f scheduled hours(s) (%<lead_hrs>0.1f lead hours)',
             lead_hrs: chair.lead_hrs, tot_hrs: chair.tot_hrs)
    end
  end

  # generate scheduling progress bar style for chair calendar
  def progress_style(chair, users)
    return unless chair.location.continuous

    filled_pct = 100 * chair.filled_slots(users) / chair.slots.size
    "background:
       linear-gradient(to right,
                       lightgreen #{filled_pct}%,
                       lightgrey #{filled_pct}%)"
  end

  def heatmap_calendar_inner_html(id)
    "<a href='/admin_dashboard/heatmap?slot_id=#{id}'
          style='display:block;text-decoration:none;'
          id=#{id}_link>
        <div style='height:100%;width:100%'>
          &nbsp;
        </div>
    </a>".html_safe
  end

  def heatmap_calendar_styling(users)
    lambda do |id|
      slot = Slot.from_id(id)
      count = 0
      (users || []).each do |s|
        count += 1 if s.covers_timeslot?(slot)
      end

      count.positive? ? { class: "heatmap-color-#{count}" } : {}
    end
  end

  # rubocop:disable Metrics/MethodLength
  # rubocop:disable Style/GuardClause
  def shift_scheduling_styling(loc)
    lambda do |id, user, selected|
      shift = user.scheduled?(id)
      classes = selected ? [SELECTED_USER_CLASS] : []
      return { class: classes << UNAVAIL_STR } unless user.covers_timeslot?(Slot.from_id(id))

      if shift
        if shift.chair.location == loc
          return { class: classes << SCHEDULED_HERE_CLASS }
        else
          return { class: classes << SCHEDULED_CLASS,
                   'data-toggle' => 'tooltip',
                   'title' => shift.chair.location.name }
        end
      end
      classes << PAINTABLE_STR if selected
      { class: classes }
    end
  end
  # rubocop:enable Metrics/MethodLength
  # rubocop:enable Style/GuardClause

  def mini_calendar_styling(user)
    lambda do |id|
      av = user.availabilities.find { |a| a.covers_timeslot?(Slot.from_id(id)) }
      return {} unless av

      ids = av.slots.map(&:to_id)

      classes = user.scheduled?(id) ? %w(mini-scheduled) : %w(mini-available)
      classes << 'mini-top' if id == ids.first
      classes << 'mini-bottom' if id == ids.last
      { class: classes }
    end
  end

  def location_options(branch)
    branch.locations.map do |loc|
      [loc.name, scheduling_admin_dashboard_index_url(location: loc)]
    end
  end

  def available_users(users, selected_chair)
    users.filter { |u| u.available_during?(selected_chair) }
  end

  def available_users_slot(users, slot_id)
    users.filter do |u|
      u.covers_timeslot?(Slot.from_id(slot_id)) && !u.scheduled?(slot_id)
    end
  end

  def formatted_user_table(users)
    users.map do |u|
      if u.target_hours.nil? || u.target_hours.zero?
        { user: u, msg: 'Form incomplete' }
      else
        { user: u, msg: "#{u.mins_covered * 100 /
                                           (u.target_hours * 60)}%" }
      end
    end
  end

  def user_link(user:, chair:, location:)
    # if the user is currently selected, clicking on it should deselect it
    user = nil if selected?(user&.id, :user)
    scheduling_admin_dashboard_index_url(user: user,
                                         chair: chair, location: location)
  end

  def selected?(id, param)
    params[param].to_i == id
  end

  # @param show [Boolean] whether the button should be visible and active
  # @param value [String] the icon that the button should display
  def icon_button(show, id = nil, classes = [])
    classes.append(show ? '' : 'invisible')
    "<i id='#{id}' class='#{classes.join(' ')}'></i>".html_safe
  end

  def users_sorted_by_availability(location, chair)
    users = location.branch.users.order(last_name: :asc)
    return users unless chair

    available_users = users.select { |u| u.available_during?(chair) }
    available_users + (users - available_users)
  end

  def user_list_greyed_out(user, chair)
    !chair.nil? && !user.available_during?(chair)
  end
end
# rubocop:enable Metrics/ModuleLength
