# frozen_string_literal: true

# Helper methods for the Availabilities views
module AvailabilitiesHelper
  # @param time [Integer] time in 24h int format
  # returns time in form hour:min AM/PM
  def display_time(time, opts = {})
    hour = time / 100
    minute = format('%<min>02d', min: time % 100) # minute should be two digits
    ampm = am_or_pm?(hour)
    hour -= 12 if hour > 12
    hour = 12 if hour.zero?

    if opts[:abbreviated]
      "#{hour}:#{minute}"
    else
      "#{hour}:#{minute} #{ampm}"
    end
  end

  # array of times from range start to (range end - MINUTE_STEP)
  def times(range = (800...2400))
    times = []
    time = range.begin
    until time >= range.end || time.zero?
      times.append(time)
      time = next_time(time)
    end
    times
  end

  # @param day [Integer] 0-6, day index
  # @param time [Integer] time in 24h int format
  def time_to_id(day, time)
    "#{day}-#{time}"
  end

  # for availability form
  def options_for_start
    times.index_by { |time| display_time(time) }
  end

  def options_for_end
    options_for_start.merge(display_time(2400) => 2400)
  end

  # @param time [Integer] time in 24h int format
  # returns hour, minute
  def parse_time(time)
    hour = time / 100
    minute = time % 100
    [hour, minute]
  end

  # @param time [Integer]
  # returns true if time should show up on the calendar
  # returns true for times on hour or half hour
  def time_displayed?(time)
    min = time % 100
    [0, 30].include?(min)
  end

  # returns classes for td and th
  def get_class(time, day = nil)
    submitted = day.nil? ? '' : submitted_class(day, time)
    dashes = time_displayed?(time) ? 'bottom' : 'top'
    paintable = submitted.empty? ? PAINTABLE_STR : ''
    "#{submitted} border__dashed--#{dashes} #{paintable}"
  end

  def sum_hours(availabilities)
    return 0 unless availabilities

    availabilities.reduce(0) { |sum, a| sum + a.length } / 60.0
  end

  # if the given day and time start an availability,
  # return a button to delete that availability
  def delete_availability_button(day, time, avails)
    availability_id = starts_availability(day, time, avails)
    return unless availability_id

    link_to 'X', availability_path(availability_id),
            method: :delete, data: { confirm: 'Are you sure?' },
            disabled: current_user&.submitted,
            type: 'button', class: 'btn btn-danger btn-sm delete-button'
  end

  # create a hash such that avails[day][time] returns the id of the
  # availability that starts at that day and time, if there is one
  def generate_avails_hash(availabilities)
    avails = Availability.days.map { {} }
    availabilities.each do |a|
      avails[Availability.days[a.day]][a.start_time] = a.id
    end
    avails
  end

  # @param time [Integer] time in 24h int format
  # returns the next time, in MINUTE_STEP minute intervals
  def next_time(time)
    hour, minute = parse_time(time)
    new_min = (minute + MINUTE_STEP) % 60
    new_hour = hour + ((minute + MINUTE_STEP) / 60)
    return (new_hour * 100) + new_min unless new_hour == 24

    0
  end

  private

  # @param hour [Integer] 0..24
  def am_or_pm?(hour)
    hour < 12 || hour == 24 ? 'AM' : 'PM'
  end

  def submitted_class(day, time)
    slot = Slot.from_id(time_to_id(day, time))
    return '' unless current_user.covers_timeslot?(slot)

    SUBMIT_STR
  end

  # returns avail.id if that cell represents the start of an availability
  def starts_availability(day, time, avails)
    return unless avails

    avails[day][time]
  end
end
