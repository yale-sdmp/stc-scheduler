# frozen_string_literal: true

# ApplicationHelper top level comment
module ApplicationHelper
  # @param hours [Integer]
  # generates string for displaying number of hours
  def hours_string(hours)
    is_plural = hours == 1 ? '' : 's'
    # if hours ends with .0, drop the .0
    hours = hours.to_i == hours ? hours.to_i : hours
    "#{hours} hour#{is_plural}"
  end
end
