# frozen_string_literal: true

# Helper for student step bar
module StudentStepBarHelper
  def get_bar_class(button, active_button)
    color = active?(button, active_button) ? 'dark' : 'light'
    disabled = active?(button, active_button) ? 'disabled' : ''
    curve = ''
    curve = 'bar--left' if button == 1
    curve = 'bar--right' if button == 4
    "btn btn-#{color} step-bar #{curve} #{disabled}"
  end

  def active?(button, active_button)
    button == active_button
  end
end
