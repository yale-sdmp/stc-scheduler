# frozen_string_literal: true

# Helper for Users
module UserHelper
  def display_due_date(user)
    user && user.role == 'student' && !user.submitted\
      && user.branch && user.branch.due_date
  end
end
