# frozen_string_literal: true

# Helper methods for the upl views
module UserPreferredLocationsHelper
  # for CTs
  # their preferred location is the one with the highest preferred hours
  def get_preferred_location(user)
    user.user_preferred_locations.order(preferred_hours: :desc)
        .first_or_initialize
  end
end
