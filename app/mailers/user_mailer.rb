# frozen_string_literal: true

# Mailer class for user emails
class UserMailer < ApplicationMailer
  def email_incomplete_user(user:)
    @user = user
    mail(to: @user.email,
         subject: '[ACTION REQUIRED] You have not submitted your availability')
  end
end
