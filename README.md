# README
## Local Development
### Cloning and initial setup
* Clone down the project using `git clone https://gitlab.com/yale-sdmp/stc-scheduler.git`
* From the project root, use a ruby version manager to make sure the correct version is active
    * If you're using rbenv `rbenv local x.x.x` where x.x.x is the ruby version specified in the Gemfile
    * If that version isn't installed on your machine you may need to run `rbenv install x.x.x`
* Install the ruby dependencies using `bundle install`
* Install the node dependencies using `yarn install`
* Set up the environment variables
    * Use the template for a starting point by running `cp .env.template .env`
    * Make sure to edit the .env template for your dev environment
* Initialize the database using `rails db:create`
* Load the schema to the database using `rails db:schema:load`
* If there is anything in /db/seed.rb run `rails db:seed`

### Starting the server
Run `rails s`

### Starting the server in production mode
`RAILS_ENV=production rails webpacker:compile && rails s`

## Docker development
### Cloning and initial setup
* Clone down the project using `git clone https://gitlab.com/yale-sdmp/stc-scheduler.git`
* Set up the environment variables
    * Use the template for a starting point by running `cp .env.template .env`
    * Make sure to edit the .env template for your dev environment
    * Make sure to uncomment the variables mentioned for docker development
* Use docker-compose to set up the containers and container networking by running the command `docker-compose build .`
* Start the containers and networks in detached mode by running `docker-compose up -d`
* Initialize the database using `docker-compose exec rails bundle exec rails db:create`
* Load the database schema using `docker-compose exec rails bundle exec rails db:schema:load`
* Stop the containers and networks using `docker-compose down`

### Starting the server
You can run `docker-compose up` to run it in attached mode to see the stream
You can run `docker-compose up -d` to run it in detached mode which will keep it running in the background

EITHER WAY run `docker-compose down` to stop the server.

### Other notes
If you make changes to the dependencies you will need to run `docker-compose build` to reset the image.
To restart the server you can either run `docker compose down` then `docker-compose up`, or you can just run `docker-compose restart`
