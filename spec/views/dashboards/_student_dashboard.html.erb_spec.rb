# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'dashboards/_student_dashboard', type: :view do
  context 'when user.submitted is true' do
    let(:user) { build(:user, submitted: true) }

    it 'displays lock message' do
      render_with_user(user)
      expect(response).to have_text('Your information is now locked.')
    end

    it 'does not display submit button' do
      render_with_user(user)
      expect(response).not_to have_button('Submit')
    end

    it 'does not display step bar' do
      render_with_user(user)
      expect(response).not_to have_text('Add User Information')
    end
  end

  context 'when branch has no locations' do
    let(:user) { build(:user, branch: build(:branch, locations: []), id: 1) }

    it 'does not display Location Preferences' do
      stub_template 'application/_student_step_bar' => 'step bar'
      render_with_user(user)
      expect(response).not_to have_text('Location Preferences')
    end
  end

  def render_with_user(user)
    render partial: 'dashboards/student_dashboard', locals: { user: user }
  end
end
