# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'user_preferred_locations/_index', type: :view do
  it "shows 'No Preference' when there are no upls" do
    user = build(:user)
    render_index(user)
    expect(response).to have_text('No preference')
  end

  # rubocop:disable RSpec/ExampleLength
  it 'shows the correct hours for each upl' do
    user = build(:user, user_preferred_locations: set_up_upls)
    render_index(user)
    user.user_preferred_locations.each do |upl|
      expect(response.squish).to have_text\
        "#{upl.location.name}: #{upl.preferred_hours} hours"
    end
  end
  # rubocop:enable RSpec/ExampleLength

  it "shows singular 'hour' when preferred_hours is 1" do
    upls = [build(:user_preferred_location, preferred_hours: 1)]
    user = build(:user, user_preferred_locations: upls)
    render_index(user)
    expect(response).not_to have_text('1 hours')
  end

  it "shows 'Preferred Location' when user is CT" do
    upls = [build(:user_preferred_location)]
    user = build(:user, user_preferred_locations: upls, branch: build(:branch))
    allow(user.branch).to receive(:ct?).and_return(true)
    render_index(user)
    expect(response).to have_text('Preferred Location')
  end

  private

  def set_up_upls
    loc1 = build(:location, name: 'loc1')
    loc2 = build(:location, name: 'loc2')
    [
      build(:user_preferred_location, location: loc1, preferred_hours: 0),
      build(:user_preferred_location, location: loc2, preferred_hours: 10)
    ]
  end

  def render_index(user)
    render 'user_preferred_locations/index', user: user
  end
end
