# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'user/edit', type: :view do
  it 'shows additional form fields for students' do
    @user = build(:user, role: 'student', id: 0)
    render
    expect(response).to have_text 'Target hours'
  end

  it 'does not show those additional form fields for admins' do
    @user = build(:user, role: 'admin', id: 1)
    render
    expect(response).not_to have_text 'Target hours'
  end
end
