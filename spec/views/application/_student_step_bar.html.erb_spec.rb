# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '_student_step_bar', type: :view do
  let(:user) { build(:user, id: 1, submitted: false) }

  before do
    assign(:user, user)
    allow(view).to receive(:current_user).and_return(user)
  end

  context 'when user is a student' do
    before { user.role = 'student' }

    context 'when user.submitted is not true' do
      it 'student dashboard displays exactly one active button' do
        render partial: 'dashboards/student_dashboard', locals: { user: user }
        expect(response).to have_css('.btn-dark', count: 1)
      end

      it 'displays step bar on user info page' do
        render partial: 'dashboards/student_dashboard', locals: { user: user }
        expect(response).to have_css('.step-bar', count: 4)
      end

      it 'displays step bar on calendar page' do
        render template: 'availabilities/calendar'
        expect(response).to have_css('.step-bar', count: 4)
      end

      it 'displays step bar on preferred locations page' do
        render template: 'user_preferred_locations/new'
        expect(response).to have_css('.step-bar', count: 4)
      end

      it 'displays step bar on availability new page' do
        @availability = build(:availability)
        render template: 'availabilities/new'
        expect(response).to have_css('.step-bar', count: 4)
      end
    end

    context 'when user.submitted is true' do
      before { user.submitted = true }

      it 'does not display step bar' do
        render template: 'dashboards/index'
        expect(response).not_to have_css('.step-bar')
      end
    end
  end

  context 'when user is an admin' do
    before { user.role = 'admin' }

    it 'does not display step bar' do
      render template: 'user/edit'
      expect(response).not_to have_css('.step-bar')
    end
  end
end
