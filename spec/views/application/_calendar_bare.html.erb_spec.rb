# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'application/_calendar_bare', type: :view do
  it 'adds inner html given in inner_html proc' do
    render partial: 'calendar_bare', locals: { styling: proc { {} },
                                               inner_html: proc { |id| id } }
    Nokogiri::HTML(rendered).css('td[id]').each do |td|
      expect(td[:id].strip).to eq td.text.strip
    end
  end

  # rubocop:disable RSpec/ExampleLength
  it 'gives tds styling specified in styling proc' do
    styling = proc { { class: 'styled' } }
    render partial: 'calendar_bare', locals: { styling: styling,
                                               inner_html: proc {} }
    Nokogiri::HTML(rendered).css('td[id]').each do |td|
      expect(td[:class]).to include 'styled'
    end
  end
  # rubocop:enable RSpec/ExampleLength

  it 'displays days of the week including Sunday' do
    render partial: 'calendar_bare'
    expect(response).to have_text('Sunday')
  end

  it 'displays days of the week including Saturday' do
    render partial: 'application/calendar_bare'
    expect(response).to have_text('Saturday')
  end

  it 'displays times hourly' do
    render partial: 'calendar_bare'
    expect(response).to have_text('10:00 AM')
  end

  it 'displays half-hour times' do
    render partial: 'calendar_bare'
    expect(response).to have_text('10:30 AM')
  end

  it 'does not display 15-min times' do
    render partial: 'calendar_bare'
    expect(response).not_to have_text('10:15 AM')
  end

  it 'gives cells id of <day index>-<time>' do
    render partial: 'calendar_bare'
    expect(response).to have_css('td#0-1030')
  end

  it 'renders the correct days' do
    render partial: 'calendar_bare', locals: { days: (1..1) }
    expect(response).to have_text('Monday')
  end

  it 'does not render the incorrect days' do
    render partial: 'calendar_bare', locals: { days: (1..1) }
    expect(response).not_to have_text('Sunday')
  end

  it 'renders the correct times' do
    render partial: 'calendar_bare', locals: { time_range: (1100...1200) }
    expect(response).to have_text('11:00 AM')
  end

  it 'does not render incorrect times' do
    render partial: 'application/calendar_bare',
           locals: { time_range: (1100...1200) }
    expect(response).not_to have_text('10:00 AM')
  end
end
