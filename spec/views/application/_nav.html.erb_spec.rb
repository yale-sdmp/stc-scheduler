# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '_nav', type: :view do
  it 'displays text, "Scheduler"' do
    render partial: 'application/nav'
    expect(response).to have_text('Scheduler')
  end

  it 'shows link to admin page for admins', type: :view do
    user = create(:user, role: 'admin')
    allow(view).to receive(:current_user) { user }
    allow(view).to receive(:user_signed_in?).and_return(true)
    render partial: 'application/nav'
    expect(response).to have_link('Admin page')
  end

  it 'does not show admin page link for non-admins', type: :view do
    user = create(:user, role: 'student')
    allow(view).to receive(:current_user) { user }
    allow(view).to receive(:user_signed_in?).and_return(true)
    render partial: 'application/nav'
    expect(response).not_to have_link('Admin page')
  end
end
