# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'admin_dashboards/_users_list', type: :view do
  it 'does not hide the special request icon when user has a special request' do
    special_user = create(:user, role: 'student', special_requests: 'bananas')
    render_view(special_user)
    expect(rendered).not_to have_css('heading0//invisible', text: '*')
  end

  it 'hides special request icon when user has no special requests' do
    user = create(:user, role: 'student', special_requests: nil)
    render_view(user)
    expect(rendered).to have_css('#heading0//.special-request-icon.invisible')
  end

  it 'hides lead icon when user is not a lead' do
    user = create(:user, role: 'student', lead: false)
    render_view(user)
    expect(rendered).to have_css('#heading0//.lead-icon.invisible')
  end

  # render the view with the given user rendered first in the list (index 0)
  def render_view(user)
    render 'admin_dashboards/users_list', users: [user],
                                          selected_chair: nil,
                                          location: nil,
                                          selected_user: nil
  end
end
