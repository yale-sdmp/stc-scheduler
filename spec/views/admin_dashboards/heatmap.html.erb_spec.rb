# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'admin_dashboards/heatmap', type: :view do
  before { setup }

  it 'display Heatmap when no slot id passed' do
    assign(:slot, nil)
    assign(:available_students, nil)
    render
    expect(response).to have_text('Heatmap')
  end

  it 'display slot content when slot id passed' do
    the_slot = Slot.from_id('1-1230')
    assign(:slot, the_slot)
    assign(:available_students, [])
    render
    expect(response).to have_text(the_slot.to_s)
  end

  it 'display available students when students found' do # rubocop:disable RSpec/MultipleExpectations
    assign(:slot, Slot.from_id('1-1230'))
    assign(:available_students, sample_students)
    render
    expect(response).to have_text('John Doe')
    expect(response).to have_text('Max Smith')
  end

  def sample_students
    student1 = build(:user, first_name: 'John', last_name: 'Doe')
    student2 = build(:user, first_name: 'Max', last_name: 'Smith')
    Array[student1, student2]
  end

  def avail_shading(_id)
    0.3
  end

  # rubocop:disable Metrics/MethodLength
  def setup
    assign(:styling_proc, proc { |id|
      { style: "background-color: rgba(128, 128, 128, #{avail_shading(id)});" }
    })
    assign(:inner_html_proc, proc { |id|
      "<a href='/admin_dashboard/heatmap?slot_id=#{id}'
          style='display:block;text-decoration:none;'
          id=#{id}_link>
        <div style='height:100%;width:100%'>
          &nbsp;
        </div>
      </a>"
    })
  end
  # rubocop:enable Metrics/MethodLength
end
