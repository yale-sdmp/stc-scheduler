# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'availabilities/_due_date', type: :view do
  let(:due_date_message) { 'Availability information is due' }

  context 'when user is a student' do
    context 'when user.submitted is not true' do
      let(:user) { build(:user, role: 'student', submitted: false) }

      it 'displays due date' do
        render 'availabilities/due_date', user: user
        expect(response).to have_text(due_date_message)
      end
    end

    context 'when user.submitted is true' do
      let(:user) { build(:user, role: 'student', submitted: true) }

      it 'does not display due date' do
        render 'availabilities/due_date', user: user
        expect(response).not_to have_text(due_date_message)
      end
    end

    context 'when branch is nil' do
      let(:user) do
        build(:user, role: 'student', submitted: false,
                     branch: nil)
      end

      it 'does not display due date' do
        render 'availabilities/due_date', user: user
        expect(response).not_to have_text(due_date_message)
      end
    end

    context 'when due date is nil' do
      let(:user) do
        build(:user, role: 'student', submitted: false,
                     branch: build(:branch, due_date: nil))
      end

      it 'does not display due date' do
        render 'availabilities/due_date', user: user
        expect(response).not_to have_text(due_date_message)
      end
    end
  end

  context 'when user is an admin' do
    let(:user) { build(:user, role: 'admin') }

    it 'does not display due date' do
      render 'availabilities/due_date', user: user
      expect(response).not_to have_text(due_date_message)
    end
  end
end
