# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'availabilities/calendar', type: :view do
  let(:user) { build(:user, id: 1) }
  let(:avail) do
    build(:availability, user_id: user.id, id: 2,
                         start_time: 830,
                         end_time: 1300,
                         day: 'Sunday')
  end

  before { setup }

  it 'displays days of the week' do
    render
    expect(response).to have_text('Sunday')
  end

  it 'displays times hourly' do
    render
    expect(response).to have_text('10:00 AM')
  end

  it 'displays half-hour times' do
    render
    expect(response).to have_text('10:30 AM')
  end

  it 'does not display 15-min times' do
    render
    expect(response).not_to have_text('10:15 AM')
  end

  it 'gives cells id of <day index>-<time>' do
    render
    expect(response).to have_css('td#0-1030')
  end

  it 'gives current availabilities with right class' do
    render
    avail.slots.each do |slot|
      expect(response).to have_css("##{slot.to_id}.submitted")
    end
  end

  describe 'delete button' do
    let(:day) { Availability.days[avail.day] }

    before do
      @avails_hash = generate_avails_hash(user.availabilities)
    end

    it 'is present in the top cell for each availability' do
      render
      id = time_to_id(day, avail.start_time)
      expect(response).to have_css("td##{id}/a.delete-button")
    end

    it 'is not present in cells that do not start availabilities' do
      render
      id = time_to_id(day, next_time(avail.start_time))
      expect(response).not_to have_css("td##{id}/a.delete-button")
    end
  end

  # rubocop:disable RSpec/MultipleExpectations
  it 'has a dashed line for every other row' do
    render
    expect(response).to have_css('tr[1]/td.border__dashed--bottom')
    expect(response).to have_css('tr[2]/td.border__dashed--top')
    expect(response).to have_css('tr[3]/td.border__dashed--bottom')
    expect(response).to have_css('tr[4]/td.border__dashed--top')
  end
  # rubocop:enable RSpec/MultipleExpectations

  def setup
    allow(view).to receive(:current_user) { user }
    allow(user).to receive(:availabilities) { [avail] }
    stub_template 'application/_student_step_bar' => 'step bar'
  end
end
