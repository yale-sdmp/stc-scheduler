# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'availabilities/_form', type: :view do
  let(:availability) { build(:availability) }

  before do
    render 'availabilities/form', availability: availability
  end

  describe 'start_time select' do
    it 'starts at 8:00 AM' do
      expect(rendered).to\
        have_select('availability_start_time', with_options: ['8:00 AM'])
    end

    it 'does not have times before 8:00 AM' do
      expect(rendered).not_to\
        have_select('availability_start_time', with_options: ['7:00 AM'])
    end

    it 'does not include midnight' do
      expect(rendered).not_to\
        have_select('availability_start_time', with_options: ['12:00 AM'])
    end
  end

  describe 'end_time select' do
    it 'ends at midnight' do
      expect(rendered).to\
        have_select('availability_end_time', with_options: ['12:00 AM'])
    end
  end
end
