# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Admin page requests', type: :request do
  it 'redirect non-admins to home' do
    user = create(:user, role: 'student')
    login_as(user)
    get admin_root_path
    expect(response).to redirect_to root_url
  end

  it 'redirect users who are not logged in' do
    get admin_root_path
    expect(response).to redirect_to root_url
  end

  it 'allow admins to proceed to admin page' do
    user = create(:user, role: 'admin')
    login_as(user)
    get admin_root_path
    expect(response).not_to redirect_to root_url
  end
end
