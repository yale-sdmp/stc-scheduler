# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Admin heatmap page requests', type: :request do
  let(:user) { create(:user, role: 'student') }

  it 'wont allow students to visit heatmap page' do
    login_as(user)
    get '/admin_dashboard/heatmap'
    expect(response).to redirect_to root_url
  end

  it 'wont allow students to visit slot view' do
    login_as(user)
    get '/admin_dashboard/heatmap?slot_id=\'1-1230\''
    expect(response).to redirect_to root_url
  end
end
