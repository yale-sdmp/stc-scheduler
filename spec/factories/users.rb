# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    branch { Branch.find_or_create_by(FactoryBot.attributes_for(:branch)) }
    username { FFaker::Internet.user_name }
    email { FFaker::Internet.email }
    first_name { FFaker::Name.first_name }
    last_name { FFaker::Name.last_name }
    target_hours { rand(min_hours..max_hours) }
    min_shift { 1 }
    max_shift { 3 }
  end
end
