# frozen_string_literal: true

FactoryBot.define do
  factory :chair do
    location
    start_time { 1130 }
    end_time { 1315 }
    day { 'Monday' }
  end
end
