# frozen_string_literal: true

FactoryBot.define do
  factory :deputy_role do
    title { FFaker::Job.title }
    branch
  end
end
