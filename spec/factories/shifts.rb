# frozen_string_literal: true

FactoryBot.define do
  factory :shift do
    chair
    day { chair.day }
    start_time { chair.start_time }
    end_time { chair.end_time }
    user
  end
end
