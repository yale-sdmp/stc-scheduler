# frozen_string_literal: true

FactoryBot.define do
  factory :branch do
    title { 'st' }
    due_date { FFaker::Time.date }
  end
end
