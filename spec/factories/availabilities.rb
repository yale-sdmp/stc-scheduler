# frozen_string_literal: true

FactoryBot.define do
  factory :availability do
    day { 'Sunday' }
    start_time { 1200 }
    end_time { 1300 }
    user
  end
end
