# frozen_string_literal: true

FactoryBot.define do
  factory :user_preferred_location do
    user
    location
    preferred_hours { 1 }
  end
end
