# frozen_string_literal: true

FactoryBot.define do
  factory :location do
    branch { Branch.find_or_create_by(FactoryBot.attributes_for(:branch)) }
    name { FFaker::Name.name }
  end
end
