# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of(:username) }
    it { is_expected.to validate_uniqueness_of(:email).case_insensitive }
    it { is_expected.to belong_to(:branch).optional }
    it { is_expected.to validate_length_of(:special_requests).is_at_most(150) }

    describe 'as an admin' do
      subject(:user) { described_class.new(role: 'admin') }

      it { is_expected.not_to validate_presence_of(:min_hours) }
      it { is_expected.not_to validate_presence_of(:max_hours) }
      it { is_expected.not_to validate_presence_of(:target_hours) }
      it { is_expected.not_to validate_presence_of(:min_shift) }
      it { is_expected.not_to validate_presence_of(:max_shift) }
    end

    describe 'as a student' do
      subject(:user) { described_class.new(role: 'student') }

      it { is_expected.to validate_presence_of(:min_hours).on(:update) }
      it { is_expected.to validate_presence_of(:max_hours).on(:update) }
      it { is_expected.to validate_presence_of(:target_hours).on(:update) }
      it { is_expected.to validate_presence_of(:min_shift).on(:update) }
      it { is_expected.to validate_presence_of(:max_shift).on(:update) }
    end
  end

  describe 'associations' do
    subject(:user) { described_class.new }

    it {
      expect(user).to have_many(:user_preferred_locations)
        .dependent(:destroy)
    }

    it { is_expected.to have_many(:availabilities).dependent(:destroy) }
    it { is_expected.to have_many(:shifts).dependent(:destroy) }
    it { is_expected.to have_many(:chairs).through(:shifts) }
    it { is_expected.to define_enum_for(:role).with_values(%i(student admin)) }
    it { is_expected.to belong_to(:deputy_role).optional }
  end

  describe 'custom validations' do
    describe 'as an admin' do
      it 'student validations are not applied' do
        user = build(:user, min_hours: 10, max_hours: 5, role: 'admin')
        expect(user).to be_valid
      end
    end

    # rubocop:disable RSpec/NestedGroups
    describe 'as a student' do
      describe 'min_hours' do
        it 'is invalid when min hours is greater than max hours' do
          user = build(:user, min_hours: 10, max_hours: 5, role: 'student')
          expect(user).not_to be_valid
        end

        it 'is valid when min hours is not greater than max hours' do
          user = build(:user, min_hours: 5, max_hours: 10, role: 'student')
          expect(user).to be_valid
        end
      end

      describe 'min_shift' do
        it 'is invalid when min shift is greater than max shift' do
          user = build(:user, min_shift: 3, max_shift: 1, role: 'student')
          expect(user).not_to be_valid
        end

        it 'is valid when min shift is not greater than max shift' do
          user = build(:user, min_shift: 1, max_shift: 3, role: 'student')
          expect(user).to be_valid
        end

        it 'is valid when min shift is not present' do
          user = build(:user, max_shift: 10, min_shift: nil, role: 'student')
          expect(user).to be_valid
        end

        it 'is valid when max shift is not present' do
          user = build(:user, min_shift: 5, max_shift: nil, role: 'student')
          expect(user).to be_valid
        end
      end

      describe 'target_hours' do
        it 'is invalid when target hours is less than min hours' do
          user = build(:user, target_hours: 1, min_hours: 3, role: 'student')
          expect(user).not_to be_valid
        end

        it 'is invalid when target hours is greater than max hours' do
          user = build(:user, target_hours: 20, max_hours: 10, role: 'student')
          expect(user).not_to be_valid
        end

        it 'is valid when target hours is between min hours and max hours' do
          user = build(:user, target_hours: 5, min_hours: 1, max_hours: 10,
                              role: 'student')
          expect(user).to be_valid
        end

        it 'is valid when target hours is not present' do
          user = build(:user, min_hours: 1, max_hours: 10, target_hours: nil,
                              role: 'student')
          expect(user).to be_valid
        end
      end

      describe 'deputy_role' do
        let(:user) { build(:user) }

        it 'is invalid when it has a different branch than user' do
          user.update(deputy_role: build(:deputy_role))
          expect(user).not_to be_valid
        end

        it 'is valid when branch is the same as users' do
          user.update(deputy_role: build(:deputy_role, branch: user.branch))
          expect(user).to be_valid
        end
      end
    end
    # rubocop:enable RSpec/NestedGroups
  end

  describe 'role field' do
    it 'when role changes to admin' do
      user = build(:user)
      user.role = :admin
      expect(user).to be_valid
    end

    it 'when role changes to student' do
      user = build(:user)
      user.role = :student
      expect(user).to be_valid
    end

    it 'when role changes to not student or admin' do
      user = build(:user)
      expect { user.role = :other }.to raise_error(ArgumentError)
    end
  end

  context 'when updated as a student' do
    it 'clears upls when branch changes' do
      user = create(:user, role: 'student')
      create(:user_preferred_location, user: user)
      user.update(branch: create(:branch, title: 'ct'))
      expect(user.user_preferred_locations).to be_empty
    end

    it 'clears upls when min_hours changes' do
      user = create(:user, target_hours: 10, role: 'student')
      create(:user_preferred_location, user: user)
      user.update(min_hours: user.min_hours + 1)
      expect(user.user_preferred_locations).to be_empty
    end

    it 'clears upls when max_hours changes' do
      user = create(:user, target_hours: 10, role: 'student')
      create(:user_preferred_location, user: user)
      user.update(max_hours: user.max_hours - 1)
      expect(user.user_preferred_locations).to be_empty
    end

    it 'does not clear upls when other fields change' do
      user = create(:user, role: 'student')
      create(:user_preferred_location, user: user)
      user.update(first_name: 'Luke')
      expect(user.user_preferred_locations).not_to be_empty
    end
  end

  context 'when #covers_timeslot? is called, returns' do
    it 'true when user covers time slot' do
      user = user_avail_setup
      slot = make_slot(day: 'Monday', start: 845, length: 15)
      expect(user.covers_timeslot?(slot)).to be true
    end

    it 'false when the time slot is on a different day' do
      user = user_avail_setup
      slot = make_slot(day: 'Tuesday', start: 845, length: 15)
      expect(user.covers_timeslot?(slot)).to be false
    end

    it 'false when the user covers part of the slot' do
      user = user_avail_setup
      slot = make_slot(day: 'Monday', start: 845, length: 150)
      expect(user.covers_timeslot?(slot)).to be false
    end

    it 'false when the slot is at a different time' do
      user = user_avail_setup
      slot = make_slot(day: 'Monday', start: 1100, length: 15)
      expect(user.covers_timeslot?(slot)).to be false
    end
  end

  describe '#scheduled?' do
    it 'returns true when the user is scheduled during the given time' do
      user = user_shift_setup(800, 1000, 'Sunday')
      expect(user).to be_scheduled('0-800')
    end

    it 'returns false when user is not scheduled during the given time' do
      user = user_shift_setup(800, 1000, 'Sunday')
      user.shifts.first.update(end_time: 900)
      expect(user).not_to be_scheduled('0-900')
    end
  end

  describe 'email' do
    # rubocop:disable RSpec/MultipleExpectations
    it 'allows more than one email to be nil' do
      user = create(:user, email: nil)
      user2 = create(:user, email: nil)
      expect(described_class.exists?(user.id)).to eq(true)
      expect(described_class.exists?(user2.id)).to eq(true)
    end
    # rubocop:enable RSpec/MultipleExpectations
  end

  describe 'min_hours' do
    it 'is invalid when min hours is greater than max hours' do
      user = build(:user, min_hours: 10, max_hours: 5)
      expect(user).not_to be_valid
    end

    it 'is valid when min hours is not greater than max hours' do
      user = build(:user, min_hours: 5, max_hours: 10)
      expect(user).to be_valid
    end
  end

  describe 'min_shift' do
    it 'is invalid when min shift is greater than max shift' do
      user = build(:user, min_shift: 3, max_shift: 1)
      expect(user).not_to be_valid
    end

    it 'is valid when min shift is not greater than max shift' do
      user = build(:user, min_shift: 1, max_shift: 3)
      expect(user).to be_valid
    end

    it 'is valid when min shift is not present' do
      user = build(:user, max_shift: 10, min_shift: nil)
      expect(user).to be_valid
    end

    it 'is valid when max shift is not present' do
      user = build(:user, min_shift: 5, max_shift: nil)
      expect(user).to be_valid
    end
  end

  describe 'target_hours' do
    it 'is invalid when target hours is less than min hours' do
      user = build(:user, target_hours: 1, min_hours: 3)
      expect(user).not_to be_valid
    end

    it 'is invalid when target hours is greater than max hours' do
      user = build(:user, target_hours: 20, max_hours: 10)
      expect(user).not_to be_valid
    end

    it 'is valid when target hours is between min hours and max hours' do
      user = build(:user, target_hours: 5, min_hours: 1, max_hours: 10)
      expect(user).to be_valid
    end

    it 'is valid when target hours is not present' do
      user = build(:user, min_hours: 1, max_hours: 10, target_hours: nil)
      expect(user).to be_valid
    end
  end

  context 'when updated' do
    it 'clears upls when branch changes' do
      user = create(:user)
      create(:user_preferred_location, user: user)
      user.update(branch: create(:branch, title: 'ct'))
      expect(user.user_preferred_locations).to be_empty
    end

    it 'clears upls when min_hours changes' do
      user = create(:user, target_hours: 10)
      create(:user_preferred_location, user: user)
      user.update(min_hours: user.min_hours + 1)
      expect(user.user_preferred_locations).to be_empty
    end

    it 'clears upls when max_hours changes' do
      user = create(:user, target_hours: 10)
      create(:user_preferred_location, user: user)
      user.update(max_hours: user.max_hours - 1)
      expect(user.user_preferred_locations).to be_empty
    end
  end

  describe '#available_during?' do
    # rubocop:disable RSpec/ExampleLength
    it 'returns true when the user is available within chair' do
      user = user_avail_setup
      chair = build(:chair, start_time: 800,
                            end_time: 900,
                            day: 'Monday',
                            location: build(:location, branch: user.branch))
      expect(user.available_during?(chair)).to eq true
    end
    # rubocop:enable RSpec/ExampleLength

    it 'returns false when the user belong to a different branch than chair' do
      user = user_avail_setup
      chair = build(:chair, start_time: 800,
                            end_time: 900,
                            day: 'Monday')
      expect(user.available_during?(chair)).to eq false
    end
  end

  describe '#mins_covered' do
    it 'is able to get the total minutes that are assigned' do
      user = user_shift_setup(1200, 1400, 1)
      expect(user.mins_covered).to eq(120)
    end
  end

  def user_avail_setup
    user = build(:user)
    avail = build(:availability,
                  user_id: user.id,
                  start_time: 830,
                  end_time: 940,
                  day: 'Monday')
    allow(user).to receive(:availabilities) { [avail] }
    user
  end

  def user_shift_setup(start_time, end_time, day)
    user = create(:user)
    branch = create(:branch, title: 'sdmp')
    loc = create(:location, branch: branch)
    c = create(:chair, start_time: start_time,
                       end_time: end_time, location: loc)
    create(:shift, user: user, start_time: start_time,
                   end_time: end_time, day: day, chair: c)
    user
  end

  def make_slot(day:, start:, length:)
    Slot.new(day: day, start: start, length: length)
  end
end
