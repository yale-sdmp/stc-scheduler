# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserPreferredLocation, type: :model do
  subject! do
    build(:user_preferred_location, user: st_user,
                                    location: loc1, preferred_hours: 5)
  end

  let(:st) { create(:branch, title: 'st') }
  let(:st_user) { create(:user, branch: st) }
  let(:loc1) { create(:location, branch: st) }
  let(:loc2) { create(:location, branch: st) }
  let(:msg) { 'For each user, only one record may exist per location.' }

  it { is_expected.to belong_to :user }
  it { is_expected.to belong_to :location }

  it {
    expect(subject).to validate_uniqueness_of(:location)
      .scoped_to(:user_id)
      .with_message(msg)
  }

  it { is_expected.to validate_presence_of(:preferred_hours) }

  it 'does not raise error when more than one upl created' do
    create(:user_preferred_location, user: st_user, location: loc1,
                                     preferred_hours: 1)
    expect(create(:user_preferred_location, user: st_user,
                                            location: loc2,
                                            preferred_hours: 2)).to be_valid
  end
end
