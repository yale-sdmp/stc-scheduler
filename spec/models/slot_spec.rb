# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Slot, type: :model do
  describe '#adjacent?' do
    it 'correctly identifies adjacent slots' do
      s1 = described_class.new(day: 'Sunday', start: 830, length: 30)
      s2 = described_class.new(day: 'Sunday', start: 900, length: 30)
      expect(s1.adjacent?(s2)).to be true
    end

    it 'correctly identifies non-adjacent slots' do
      s1 = described_class.new(day: 'Sunday', start: 830, length: 30)
      s2 = described_class.new(day: 'Sunday', start: 845, length: 15)
      expect(s1.adjacent?(s2)).to be false
    end

    it 'correctly identifies non-adjacent slots on different days' do
      s1 = described_class.new(day: 'Sunday', start: 2330, length: 30)
      s2 = described_class.new(day: 'Monday', start: 0, length: 15)
      expect(s1.adjacent?(s2)).to be false
    end
  end

  it 'self.from_id makes correct slot' do
    slot = described_class.from_id('0-830')
    expect(slot.day == 'Sunday' && slot.int == 830 && \
           slot.length == MINUTE_STEP).to be true
  end

  describe 'self.from_chair' do
    it 'makes slot with correct start time' do
      chair = build(:chair)
      slot = described_class.from_chair(chair)
      expect(slot.int).to eq chair.start_time
    end

    it 'makes slot with correct length' do
      chair = build(:chair)
      slot = described_class.from_chair(chair)
      expect(slot.succ.int).to eq chair.end_time
    end
  end

  describe 'self.from_shift' do
    it 'makes slot with correct start time' do
      shift = build(:shift)
      slot = described_class.from_shift(shift)
      expect(slot.int).to eq shift.start_time
    end

    it 'makes slot with correct length' do
      shift = build(:shift)
      slot = described_class.from_shift(shift)
      expect(slot.succ.int).to eq shift.end_time
    end
  end

  describe '#merge' do
    it 'merges adjacent slots' do
      s1 = described_class.new(day: 'Sunday', start: 830, length: 30)
      s2 = described_class.new(day: 'Sunday', start: 900, length: 30)
      merged = s1.merge(s2)
      expect(merged).to eq \
        described_class.new(day: 'Sunday', start: 830, length: 60)
    end

    it 'raises error trying to merge non-adjacent slots' do
      s1 = described_class.new(day: 'Sunday', start: 830, length: 30)
      s2 = described_class.new(day: 'Sunday', start: 945, length: 30)
      expect { s1.merge(s2) }.to raise_error(/adjacent/)
    end
  end

  describe '#<=>' do
    it 'gives correct ordering of slots on same day' do
      s1 = described_class.new(day: 'Sunday', start: 830, length: 30)
      s2 = described_class.new(day: 'Sunday', start: 900, length: 30)
      expect(s1 < s2).to be true
    end

    it 'gives correct ordering of slots on different days' do
      s1 = described_class.new(day: 'Sunday', start: 830, length: 30)
      s2 = described_class.new(day: 'Monday', start: 830, length: 30)
      expect(s1 < s2).to be true
    end
  end
end
