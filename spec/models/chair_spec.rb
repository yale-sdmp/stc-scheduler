# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Chair, type: :model do
  context 'with relations' do
    it { is_expected.to belong_to :location }
    it { is_expected.to have_many(:shifts).dependent(:destroy) }
    it { is_expected.to have_many(:users).through(:shifts) }
  end

  context 'with validations' do
    let(:chair) { build(:chair) }

    it { is_expected.to validate_presence_of :start_time }
    it { is_expected.to validate_presence_of :end_time }
  end

  context 'with user_min' do
    let(:chair_with_min) { build(:chair, user_min: 3) }
    let(:location) { build(:location, user_min: 5) }
    let(:chair_without_min) { build(:chair, location: location) }

    it 'returns user_min of chair when present' do
      expect(chair_with_min.user_min).to eq 3
    end

    it "returns user_min of chair's location when nil" do
      expect(chair_without_min.user_min).to eq 5
    end
  end

  context 'with user_max' do
    let(:chair_with_max) { build(:chair, user_max: 15) }
    let(:location) { build(:location, user_max: 20) }
    let(:chair_without_max) { build(:chair, location: location) }

    it 'returns user_max of chair when present' do
      expect(chair_with_max.user_max).to eq 15
    end

    it "returns user_max of chair's location when nil" do
      expect(chair_without_max.user_max).to eq 20
    end
  end

  describe '#slots' do
    it 'returns the slots that make up the availability' do
      chair = build(:chair, start_time: 2330, end_time: 2400, day: 'Monday')
      times = chair.slots.map(&:start_time)

      # if this is failing it's likely because the
      # MINUTE_STEP has been changed (see config/initializers/constants.rb)
      # and you probably just need to change the times below
      expect(times).to eq [{ hr: 23, min: 30 }, { hr: 23, min: 45 }]
    end
  end

  context 'when #covers_timeslot? is called, returns' do
    it 'true when user covers slot' do
      chair = build(:chair, start_time: 830,
                            end_time: 940,
                            day: 'Monday')
      slot = make_slot(day: 'Monday', start: 845, length: 15)
      expect(chair.covers_timeslot?(slot)).to be true
    end

    it 'false when slot is on different day' do
      chair = build(:chair, start_time: 830,
                            end_time: 940,
                            day: 'Monday')
      slot = make_slot(day: 'Tuesday', start: 845, length: 15)
      expect(chair.covers_timeslot?(slot)).to be false
    end

    it 'false when user only covers part of slot' do
      chair = build(:chair, start_time: 830,
                            end_time: 940,
                            day: 'Monday')
      slot = make_slot(day: 'Monday', start: 845, length: 150)
      expect(chair.covers_timeslot?(slot)).to be false
    end

    it 'false when slot is at a different time' do
      chair = build(:chair, start_time: 830,
                            end_time: 940,
                            day: 'Monday')
      slot = make_slot(day: 'Monday', start: 1100, length: 15)
      expect(chair.covers_timeslot?(slot)).to be false
    end
  end

  describe '#filled' do
    let(:chair) { create(:chair) }

    it 'returns EMPTY when no shifts are scheduled' do
      expect(chair.filled_status).to eq :empty
    end

    context 'when chair is continuous' do
      before { chair.location.update(continuous: true) }

      let(:shift) { create(:shift, chair: chair) }

      it 'returns FILLED when staffed at all times' do
        end_time = chair.slots[-1].start_time
        shift.update(end_time: end_time)
        shift2 = create(:shift, chair: chair)
        shift2.update(start_time: end_time)
        expect(chair.filled_status).to eq :satisfied
      end

      it 'returns PARTIAL when not staffed at all times' do
        shift.update(end_time: shift.end_time - 1)
        expect(chair.filled_status).to eq :partial
      end

      it 'returns PARTIAL when there are not enough workers' do
        chair.update(user_min: 2)
        shift2 = create(:shift, chair: chair)
        shift2.update(end_time: shift2.end_time - 1)
        expect(chair.filled_status).to eq :partial
      end

      it 'returns PARTIAL when lead is needed but not fully scheduled' do
        chair.location.update(lead_needed: true)
        shift2 = create(:shift, chair: chair, user: create(:user, lead: true))
        shift2.update(end_time: shift2.end_time - 1)
        expect(chair.filled_status).to eq :partial
      end
    end

    context 'when chair is not continuous and lead is needed' do
      before { chair.location.update(lead_needed: true) }

      it 'returns PARTIAL when lead is not present during non lead shift' do
        create(:shift, chair: chair)
        lead_shift = create(:shift, chair: chair,
                                    user: create(:user, lead: true))
        lead_shift.update(end_time: lead_shift.end_time - 1)
        expect(chair.filled_status).to eq :partial
      end
    end
  end

  describe '#filled_slots' do
    let(:chair) { create(:chair, user_min: 2) }
    let(:u1) { create(:user, branch: chair.location.branch) }
    let(:u2) { create(:user, branch: chair.location.branch) }

    it 'returns 0 when no shifts are scheduled' do
      expect(chair.filled_slots([u1, u2])).to eq 0
    end

    it 'returns 0 when there are only partially scheduled slots' do
      schedule_for_full_chair(u1, chair)
      expect(chair.filled_slots([u1, u2])).to eq 0
    end

    it 'returns the number of fully scheduled slots' do
      schedule_for_full_chair(u1, chair)
      schedule_for_full_chair(u2, chair)
      expect(chair.filled_slots([u1, u2])).to eq chair.slots.size
    end
  end

  describe '#unfilled_slots' do
    let(:chair) { create(:chair, user_min: 2) }
    let(:u1) { create(:user, branch: chair.location.branch) }
    let(:u2) { create(:user, branch: chair.location.branch) }

    it 'returns the chair\'s slots when no shifts are scheduled' do
      expect(chair.unfilled_slots([u1, u2])).to eq chair.slots
    end

    it 'returns slots which are only partially filled' do
      schedule_for_full_chair(u1, chair)
      expect(chair.unfilled_slots([u1, u2])).to eq chair.slots
    end

    it 'does not return fully scheduled slots' do
      schedule_for_full_chair(u1, chair)
      schedule_for_full_chair(u2, chair)
      expect(chair.unfilled_slots([u1, u2])).to eq []
    end
  end

  describe '#num_available' do
    let(:chair) { create(:chair) }
    let(:u1) { create(:user, branch: chair.location.branch) }

    it 'returns 0 when none of the users are available' do
      expect(chair.num_available([u1])).to eq 0
    end

    it 'returns 0 when the users are available but scheduled' do
      make_available_for_full_chair(u1, chair)
      schedule_for_full_chair(u1, chair)
      expect(chair.num_available([u1])).to eq 0
    end

    it 'returns the number of available but unscheduled users' do
      make_available_for_full_chair(u1, chair)
      expect(chair.num_available([u1])).to eq 1
    end
  end

  describe '#tot_hrs' do
    let(:chair) do
      create(:chair, start_time: 930,
                     end_time: 1130)
    end

    before do
      create(:shift, chair: chair,
                     start_time: 1100,
                     end_time: 1130)
      create(:shift, chair: chair,
                     start_time: 930,
                     end_time: 1115)
    end

    it 'returns the number of scheduled hours for the chair' do
      expect(chair.tot_hrs).to eq 0.5 + 1.75
    end
  end

  describe '#lead_hrs' do
    let(:chair) do
      create(:chair, start_time: 930,
                     end_time: 1130)
    end

    before do
      lead = create(:user, lead: true, branch: chair.location.branch)
      create(:shift, chair: chair,
                     start_time: 1100,
                     end_time: 1130,
                     user: lead)
      create(:shift, chair: chair,
                     start_time: 930,
                     end_time: 1115)
    end

    it 'returns the number of scheduled hours for the chair' do
      expect(chair.lead_hrs).to eq 0.5
    end
  end

  def make_available_for_full_chair(user, chair)
    create(:availability, user: user,
                          start_time: chair.start_time,
                          end_time: chair.end_time,
                          day: chair.day)
  end

  def schedule_for_full_chair(user, chair)
    create(:shift, user: user,
                   chair: chair,
                   start_time: chair.start_time,
                   end_time: chair.end_time)
  end

  def make_slot(day:, start:, length:)
    Slot.new(day: day, start: start, length: length)
  end
end
