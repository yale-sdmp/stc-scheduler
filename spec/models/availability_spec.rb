# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Availability, type: :model do
  context 'with relations' do
    it { is_expected.to belong_to :user }

    it {
      expect(described_class.new).to define_enum_for(:day)
        .with_values(
          %i(Sunday Monday Tuesday Wednesday Thursday Friday Saturday)
        )
    }

    it { is_expected.to validate_presence_of :day }
    it { is_expected.to validate_presence_of :start_time }
    it { is_expected.to validate_presence_of :end_time }
  end

  context 'when #covers_timeslot? is called, returns' do
    it 'true when user covers slot' do
      avail = build(:availability, start_time: 830,
                                   end_time: 940,
                                   day: 'Monday')
      slot = make_slot(day: 'Monday', start: 845, length: 15)
      expect(avail.covers_timeslot?(slot)).to be true
    end

    it 'false when slot is on different day' do
      avail = build(:availability, start_time: 830,
                                   end_time: 940,
                                   day: 'Monday')
      slot = make_slot(day: 'Tuesday', start: 845, length: 15)
      expect(avail.covers_timeslot?(slot)).to be false
    end

    it 'false when user only covers part of slot' do
      avail = build(:availability, start_time: 830,
                                   end_time: 940,
                                   day: 'Monday')
      slot = make_slot(day: 'Monday', start: 845, length: 150)
      expect(avail.covers_timeslot?(slot)).to be false
    end

    it 'false when slot is at a different time' do
      avail = build(:availability, start_time: 830,
                                   end_time: 940,
                                   day: 'Monday')
      slot = make_slot(day: 'Monday', start: 1100, length: 15)
      expect(avail.covers_timeslot?(slot)).to be false
    end
  end

  describe '#slots' do
    it 'returns the slots that make up the availability' do
      avail = build(:availability, start_time: 830, end_time: 940,
                                   day: 'Monday')

      times = avail.slots.map(&:start_time)

      # if this is failing it's likely because the
      # MINUTE_STEP has been changed (see config/initializers/constants.rb)
      # and you probably just need to change the times below
      expect(times).to eq [{ hr: 8, min: 30 }, { hr: 8, min: 45 },
                           { hr: 9, min: 0 }, { hr: 9, min: 15 }]
    end

    it 'returns the right slots for slots that end at midnight' do
      avail = build(:availability, start_time: 2330, end_time: 2400,
                                   day: 'Monday')

      times = avail.slots.map(&:start_time)
      expect(times).to eq [{ hr: 23, min: 30 }, { hr: 23, min: 45 }]
    end
  end

  describe '#length' do
    it 'gives the correct length' do
      avails = [build(:availability, start_time: 830, end_time: 920,
                                     day: 'Monday'),
                build(:availability, start_time: 2315, end_time: 2400,
                                     day: 'Tuesday')]
      expect(avails.map(&:length)).to eq [50, 45]
    end
  end

  def make_slot(day:, start:, length:)
    Slot.new(day: day, start: start, length: length)
  end
end
