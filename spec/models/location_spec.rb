# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Location, type: :model do
  describe 'with relations' do
    it { is_expected.to belong_to :branch }
    it { is_expected.to have_many(:chairs).dependent(:destroy) }
    it { is_expected.to have_many :user_preferred_locations }
  end

  describe 'basic validation' do
    let(:location) { build(:location) }

    it 'default public_facing to true' do
      expect(location.public_facing).to eq true
    end

    it { is_expected.to validate_uniqueness_of(:name).ignoring_case_sensitivity }
    it { is_expected.to validate_presence_of(:user_min) }

    it { expect(location.lead_needed).to eq false }
    it { expect(location.lead_only).to eq false }
    it { expect(location.continuous).to eq false }
    it { expect(location.user_min).to eq 1 }
  end
end
