# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Branch, type: :model do
  context 'with relations' do
    subject(:branch) { described_class.new(title: 'st') }

    it { is_expected.to have_many :users }
    it { is_expected.to have_many :locations }

    it {
      expect(branch).to define_enum_for(:title)
        .with_values(mt: 0, ct: 1, st: 2, sdmp: 3)
    }

    it {
      expect(branch).to validate_uniqueness_of(
        :title
      ).ignoring_case_sensitivity
    }
  end
end
