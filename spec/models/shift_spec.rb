# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Shift, type: :model do
  context 'with relations' do
    it { is_expected.to belong_to :user }
    it { is_expected.to belong_to :chair }
  end

  context 'with validations' do
    let(:shift) { build(:shift) }

    it { is_expected.to validate_presence_of :start_time }
    it { is_expected.to validate_presence_of :end_time }
  end

  describe '#length' do
    it 'returns the correct length' do
      s = create(:shift, start_time: 1145, end_time: 1330)
      expect(s.length).to eq 105
    end
  end
end
