# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ShiftCalendarCreator do
  let(:user) { create(:user) }
  let(:loc) { create(:location, branch: user.branch) }
  let(:chair) do
    create(:chair, location: loc,
                   day: 'Sunday',
                   start_time: 800,
                   end_time: 1000)
  end

  before do
    create(:availability, user: user,
                          start_time: chair.start_time,
                          end_time: chair.end_time)
  end

  context 'when no ids are supplied' do
    it 'returns false' do
      expect(create_scc([]).call).to be false
    end

    it 'does not create new shifts' do
      expect { create_scc([]).call }.not_to change { user.reload.shifts.size }
    end
  end

  # if this is failing, it is likely because the times are hardcoded and
  # MINUTE_STEP has changed (see config/initializers/constants.rb)
  context 'when given valid ids' do
    it 'merges ids to give appropriate shifts' do
      create_scc(%w(0-800 0-815 0-830 0-915)).call
      res = [{ 'start_time' => 800, 'end_time' => 845, 'chair_id' => chair.id },
             { 'start_time' => 915, 'end_time' => 930, 'chair_id' => chair.id }]
      expect(shifts_params).to eq res
    end
  end

  def create_scc(ids)
    described_class.new(user: user.id,
                        chair: chair.id,
                        location: loc.id,
                        'highlightedIds' => ids)
  end

  def shifts_params(fields = %w(start_time end_time chair_id))
    user.reload.shifts.map do |shift|
      shift.attributes.slice(*fields)
    end
  end
end
