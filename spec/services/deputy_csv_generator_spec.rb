# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DeputyCsvGenerator, type: :service do
  let(:chair) { create(:chair) }
  let(:user) { create(:user, role: 'student', branch: chair.location.branch) }
  let(:shift) do
    create(:shift, chair: chair,
                   user: user,
                   start_time: 1145,
                   end_time: 1300)
  end

  before do
    user.update(deputy_role: create(:deputy_role, branch: user.branch))
  end

  it 'gives an error when called with nil branch' do
    dep_exp = described_class.new(nil)
    dep_exp.call
    expect(dep_exp.errors).not_to be(nil)
  end

  describe '#csv_data' do
    let(:csv_data) do
      shift
      dep_exp = described_class.new(chair.location.branch)
      dep_exp.call
      dep_exp.csv_data
    end

    it 'has the correct headers' do
      headers = ['Start Time', 'End Time', 'Area', 'Location', 'Meal break',
                 'Employee']
      first_row = csv_data.split("\n").first
      expect(first_row.split(',')).to eq headers
    end

    it 'includes the correct shift information' do
      cols = ['11:45', '13:00', chair.location.name, user.full_name,
              user.deputy_role.title]
      cols.each do |col|
        expect(csv_data).to include(col)
      end
    end
  end
end
