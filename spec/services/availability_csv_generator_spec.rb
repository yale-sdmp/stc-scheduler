# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AvailabilityCsvGenerator, type: :service do
  let(:admin) { create(:user, role: 'admin', branch: create(:branch)) }

  before do
    create(:availability,
           day: 'Sunday',
           start_time: 800,
           end_time: 900,
           user_id: admin.id)
  end

  it 'gives an error when called with no users' do
    acg = described_class.new(nil)
    acg.call
    expect(acg.errors).not_to be(nil)
  end

  it 'gives correct data when called by admin' do
    acg = described_class.new([admin])
    acg.call
    expect(acg.csv_data).to include('Sun 08:30,YES')
  end
end
