# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AvailabilityCalendarCreator do
  let(:user) { create(:user) }
  let(:minute_step) { MINUTE_STEP }

  # rubocop:disable RSpec/ExampleLength
  it 'generates corrects availabilities', include_helpers: true do
    params = ['0-800', '1-1000', "0-#{800 + minute_step}"]
    described_class.new(params, user).call
    details = user.availabilities.map do |a|
      [a.day, a.start_time, a.length]
    end
    expect(details).to eq [['Sunday', 800, 2 * minute_step],
                           ['Monday', 1000, minute_step]]
  end
  # rubocop:enable RSpec/ExampleLength

  it 'gives error when overlapping with existing availability' do
    create(:availability, start_time: 800, end_time: 900, day: 0, user: user)
    acc = described_class.new(['0-830'], user)
    expect(acc.call).to eq false
  end

  it 'gives error when given overlapping availabilities' do
    acc = described_class.new(%w(0-830 0-830), user)
    expect(acc.call).to eq false
  end

  it 'allows empty calendar submission' do
    acc = described_class.new(%w(), user)
    expect(acc.call).to eq true
  end
end
