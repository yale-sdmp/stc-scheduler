# frozen_string_literal: true

require 'rails_helper'

RSpec.describe BulkAvailabilitiesDestroyer do
  let(:user) { build(:user, role: 'admin') }

  it 'destroys availabitilies for users with same branch as calling user' do
    deleter = described_class.new(user)
    users = spy
    allow(deleter).to receive(:branch_users).and_return(users)
    deleter.call
    expect(users).to have_received(:destroy_all)
  end
end
