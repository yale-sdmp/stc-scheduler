# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserPreferredLocationCreator do
  context 'when CT is logged in' do
    let(:ct) { create(:branch, title: 'ct') }
    let(:ct_user) { create(:user, branch: ct) }
    let(:loc1) { create(:location, branch: ct) }
    let(:loc2) { create(:location, branch: ct) }
    let(:params1) do
      ActionController::Parameters
        .new(location_id: loc1.id).permit!
    end
    let(:params2) do
      ActionController::Parameters
        .new(location_id: loc2.id).permit!
    end
    let(:no_pref_params) do
      ActionController::Parameters
        .new(no_preference: '1').permit!
    end

    it 'returns true on successful call' do
      creator = described_class.new(params: params1, user: ct_user)
      expect(creator.call).to eq(true)
    end

    it 'creates correct record of upl on success' do
      described_class.new(params: params1, user: ct_user).call
      expect(UserPreferredLocation
        .find_by(user: ct_user, location: loc1)).to be_present
    end

    it 'updates existing record of upl on success' do
      creator = described_class.new(params: params2, user: ct_user)
      create(:user_preferred_location, user: ct_user, location: loc1)
      expect { creator.call }.to change {
                                   find_upl(ct_user).location
                                 }.from(loc1).to(loc2)
    end

    it 'does not create a upl if no preference checked' do
      described_class.new(params: no_pref_params, user: ct_user).call
      expect(find_upl(ct_user)).to be(nil)
    end

    it 'destroys existing upls if no preference checked' do
      create(:user_preferred_location, user: ct_user, location: loc1)
      described_class.new(params: no_pref_params, user: ct_user).call
      expect(find_upl(ct_user)).to be(nil)
    end

    it 'fails to create upl when missing parameters' do
      incomplete_params = ActionController::Parameters.new({}).permit!
      creator = described_class.new(params: incomplete_params, user: ct_user)
      expect(creator.call).to eq(false)
    end

    it 'does not validate that min_hours <= requested hours' do
      user = create(:user, branch: ct, min_hours: 18)
      creator = described_class.new(params: params1, user: user)
      expect(creator.call).to eq(true)
    end
  end

  context 'when ST or MT is logged in' do
    let(:st) { create(:branch, title: 'st') }
    let(:st_user) { create(:user, branch: st) }
    let(:loc1) { create(:location, branch: st) }
    let(:loc2) { create(:location, branch: st) }
    let(:params) do
      ActionController::Parameters.new(
        loc1.id => { 'preferred_hours' => 3 },
        loc2.id => { 'preferred_hours' => 5 }
      ).permit!
    end
    let(:no_pref_params) do
      ActionController::Parameters
        .new(no_preference: '1').permit!
    end

    it 'returns truthy object on successful call' do
      creator = described_class.new(params: params, user: st_user)
      expect(creator.call).not_to eq(false)
    end

    it 'creates correct records of upl on success' do
      creator = described_class.new(params: params, user: st_user)
      expect { creator.call }.to change(UserPreferredLocation, :count).by(2)
    end

    it 'updates existing records of upl on success' do
      creator = described_class.new(params: params, user: st_user)
      create_st_upl(st_user, loc1, 8)
      expect { creator.call }.to change {
                                   find_upl(st_user, loc1).preferred_hours
                                 }.from(8).to(3)
    end

    it 'raises error if total hours exceeds max hours' do
      error = 'Total number of requested hours must not be greater than maximum requested hours.'
      user = build(:user, id: 124, branch: st, max_hours: 7)
      creator = described_class.new(params: params, user: user)
      call_and_expect(creator, error)
    end

    it 'raises error if total hours is less than min hours' do
      error = 'Total number of requested hours must not be less than minimum requested hours.'
      user = build(:user, id: 125, branch: st, min_hours: 10)
      creator = described_class.new(params: params, user: user)
      call_and_expect(creator, error)
    end

    it 'does not create a upl if no preference checked' do
      described_class.new(params: no_pref_params, user: st_user).call
      expect(find_upl(st_user)).to be(nil)
    end

    it 'destroys existing upls if no preference checked' do
      create_st_upl(st_user, loc1, 3)
      create_st_upl(st_user, loc2, 4)
      described_class.new(params: no_pref_params, user: st_user).call
      expect(find_upl(st_user)).to be(nil)
    end

    it 'fails to create upl when missing parameters' do
      incomplete_params = ActionController::Parameters.new({})
      creator = described_class.new(params: incomplete_params, user: st_user)
      expect(creator.call).to eq(false)
    end
  end

  private

  def find_upl(user, location = nil)
    if location
      UserPreferredLocation.find_by(user: user, location: location)
    else
      UserPreferredLocation.find_by(user: user)
    end
  end

  def create_st_upl(user, location, preferred_hours)
    create(:user_preferred_location, user: user, location: location,
                                     preferred_hours: preferred_hours)
  end

  # @param creator [AvailabilityCreator]
  # @param error [String] the expected error
  def call_and_expect(creator, error)
    creator.call
    expect(creator.errors[:base]).to include(error)
  end
end
