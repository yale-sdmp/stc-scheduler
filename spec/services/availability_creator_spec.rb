# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AvailabilityCreator do
  let(:user) { create(:user) }
  let(:a) do
    create(:availability, user: user, start_time: 1215, end_time: 1315)
  end
  let(:params) { { user: user, day: 'Sunday' }.merge(time_form_input(0, 100)) }

  it 'returns true when availability is successfully created' do
    creator = described_class.new(params: params)
    expect(creator.call).to eq(true)
  end

  it 'creates availabilty with correct values' do
    start_time = 0
    creator = described_class.new(params: params)
    creator.call
    expect(creator.availability.start_time).to eq(start_time)
  end

  it 'fails to create availability when missing parameters' do
    incomplete_params = { day: 'Sunday' }
    creator = described_class.new(params: incomplete_params)
    expect(creator.call).to eq(false)
  end

  context 'when adjacent to an existing availability' do
    it 'allows adjacent availabilities' do
      creator = new_availability_creator(a.start_time - 15, a.start_time)
      expect(creator.call).to eq(true)
    end

    it 'deletes old availability' do
      new_availability_creator(a.start_time - 15, a.start_time).call
      check_availability_deleted(a)
    end

    it 'does merge earlier adjacent availability' do
      creator = new_availability_creator(a.start_time - 15, a.start_time)
      creator.call
      check_availability(creator.availability, a.start_time - 15, a.end_time)
    end

    it 'does merge later adjacent availability' do
      creator = new_availability_creator(a.end_time, a.end_time + 15)
      creator.call
      check_availability(creator.availability, a.start_time, a.end_time + 15)
    end

    it 'does not change total availability count' do
      creator = new_availability_creator(a.start_time - 15, a.start_time)
      expect { creator.call }.to change(Availability, :count).by(0)
    end

    context 'with gap-closing availability' do
      let(:b) do
        create(:availability, user: user, start_time: 1330, end_time: 1345)
      end

      it 'does merge with correct times' do
        creator = new_availability_creator(a.end_time, b.start_time)
        creator.call
        check_availability(creator.availability, a.start_time, b.end_time)
      end

      it 'does merge and delete old availability' do
        new_availability_creator(a.end_time, b.start_time).call
        check_availability_deleted(a)
        check_availability_deleted(b)
      end

      it 'does decrease total availability count by 1' do
        creator = new_availability_creator(a.end_time, b.start_time)
        expect { creator.call }.to change(Availability, :count).by(-1)
      end
    end
  end

  describe 'validation' do
    it 'raises error for start_time after end_time' do
      invalid_params = { user: user, day: 'Sunday' }
                       .merge(time_form_input(1200, 0))
      creator = described_class.new(params: invalid_params)
      call_and_expect(creator, :invalid_start_time)
    end

    it 'raises error for start time overlaps with availabilty on same day' do
      invalid_params = { user: a.user, day: a.day }.merge\
        time_form_input(a.start_time + 15, a.end_time + 15)
      creator = described_class.new(params: invalid_params)
      call_and_expect(creator, :overlap)
    end

    it 'raises error for end time overlaps with availabilty on same day' do
      invalid_params = { user: user, day: a.day }.merge\
        time_form_input(a.start_time - 15, a.end_time - 15)
      creator = described_class.new(params: invalid_params)
      call_and_expect(creator, :overlap)
    end

    it 'does not allow both end and start time within existing availability' do
      invalid_params = { user: user, day: a.day }.merge\
        time_form_input(a.start_time + 15, a.end_time - 15)
      creator = described_class.new(params: invalid_params)
      expect(creator.call).to eq(false)
    end

    it 'allows end time at midnight' do
      params = { user: user, day: 'Sunday', start_time: 2300, end_time: 2400 }
      creator = described_class.new(params: params)
      expect(creator.call).to eq(true)
    end

    it 'does allow availabilities at same time on different days' do
      valid_params = { user: user, day: (Availability.days[a.day] + 1) % 7 }
                     .merge(time_form_input(a.start_time, a.end_time))
      creator = described_class.new(params: valid_params)
      expect(creator.call).to eq(true)
    end

    it 'raises error for availability at exact same time' do
      creator = new_availability_creator(a.start_time, a.end_time)
      call_and_expect(creator, :overlap)
    end
  end

  # @param creator [AvailabilityCreator]
  # @param error [Symbol] key of the expected error in the errors hash
  def call_and_expect(creator, error)
    errors = { overlap:
               'Availability overlaps with existing availability',
               invalid_start_time:
               'Start time must be before end time' }
    creator.call
    expect(creator.errors[:base]).to include(errors[error])
  end

  # @param start [Int] start time in 24h int format
  # @param end_ [Int] end time in 24h int format
  # returns a hash that looks like what the availabilities form returns
  def time_form_input(start, end_)
    { start_time: start, end_time: end_ }
  end

  def check_availability(availability, start_time, end_time)
    expect(availability.start_time).to eq(start_time)
    expect(availability.end_time).to eq(end_time)
  end

  def check_availability_deleted(avail)
    expect(Availability.exists?(avail.id)).to eq(false)
  end

  # @param start_time [Int] start time for new availability
  # @param end_time [Int] end time for new availability
  def new_availability_creator(start_time, end_time)
    new_availability_params = { user: user, day: a.day }.merge\
      time_form_input(start_time, end_time)
    described_class.new(params: new_availability_params)
  end
end
