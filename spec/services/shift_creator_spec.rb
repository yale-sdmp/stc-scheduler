# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ShiftCreator do
  let(:user) { create(:user) }
  let(:loc) { create(:location, branch: user.branch) }
  let(:chair) do
    create(:chair, location: loc,
                   day: 'Sunday',
                   start_time: 800,
                   end_time: 900)
  end

  let(:slot) { Slot.new(day: chair.day, start: 800, length: 30) }

  context 'when user is available and unscheduled' do
    before do
      add_availability(day: chair.day, range: chair.start_time..chair.end_time)
    end

    it 'returns true' do
      expect(create_shift).to be true
    end

    it 'creates correct shift' do
      create_shift
      expect(shifts_params).to eq [{ 'start_time' => slot.int,
                                     'end_time' => slot.succ.int,
                                     'chair_id' => chair.id }]
    end
  end

  context 'when user is available but scheduled' do
    before do
      add_availability(day: chair.day, range: chair.start_time..chair.end_time)
      add_shift(day: chair.day, range: chair.start_time..chair.end_time)
    end

    it 'returns false' do
      expect(create_shift).to be false
    end

    it 'does not create a shift' do
      expect { create_shift }.not_to change { user.reload.shifts.size }
    end
  end

  context 'when user is unscheduled, but also unavailable' do
    it 'returns false' do
      expect(create_shift).to be false
    end

    it 'does not create a shift' do
      expect { create_shift }.not_to change { user.reload.shifts.size }
    end
  end

  context 'when there is an adjacent shift for a different chair' do
    before do
      add_availability(day: chair.day, range: chair.start_time..chair.end_time)
      add_shift(day: chair.day, range: slot.int - 100..slot.int)
    end

    it 'returns false' do
      expect(create_shift).to be false
    end

    it 'does not create a shift' do
      expect { create_shift }.not_to change { user.reload.shifts.size }
    end
  end

  context 'when there are adjacent shifts for the same chair' do
    before do
      add_availability(day: chair.day, range: chair.start_time..chair.end_time)
      add_shift(day: chair.day, range: slot.int - 100..slot.int, same_loc: true)
      add_shift(day: chair.day,
                range: slot.succ.int..(slot.succ.int + 100),
                same_loc: true)
    end

    it 'returns true' do
      expect(create_shift).to be true
    end

    it 'merges into single shift' do
      expect { create_shift }.to change { user.reload.shifts.size }.by(-1)
    end

    it 'merges new shift with existing shift' do
      create_shift
      expect(shifts_params).to eq [{ 'start_time' => slot.int - 100,
                                     'end_time' => slot.succ.int + 100,
                                     'chair_id' => chair.id }]
    end
  end

  def add_availability(day:, range:)
    create(:availability, user: user,
                          day: day,
                          start_time: range.begin,
                          end_time: range.end)
  end

  def add_shift(day:, range:, same_loc: false)
    s = create(:shift, user: user,
                       day: day,
                       start_time: range.begin,
                       end_time: range.end)
    s.update(chair: chair) if same_loc
  end

  def create_shift
    sc = described_class.new(user: user,
                             chair: chair,
                             location: loc,
                             slot: slot)
    sc.call
  end

  def shifts_params(fields = %w(start_time end_time chair_id))
    user.reload.shifts.map do |shift|
      shift.attributes.slice(*fields)
    end
  end
end
