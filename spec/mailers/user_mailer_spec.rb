# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserMailer, type: :mailer do
  let(:user) { create(:user) }
  let(:incomplete_user_email) do
    described_class.email_incomplete_user(user: user)
  end

  it 'sends a multipart email(html and text)' do
    expect(incomplete_user_email.body.parts.collect(&:content_type))
      .to match(['text/plain; charset=UTF-8', 'text/html; charset=UTF-8'])
  end
end
