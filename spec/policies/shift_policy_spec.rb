# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ShiftPolicy do
  subject { described_class }

  context 'with admin role' do
    let(:admin) { build(:user, role: 'admin') }

    permissions :deputy_export? do
      it { is_expected.to permit(admin) }
    end

    context 'with same branch' do
      let(:shift) do
        loc = build(:location, branch: admin.branch)
        build(:shift, chair: build(:chair, location: loc))
      end

      permissions :calendar_create? do
        it { is_expected.to permit(admin, shift) }
      end
    end

    context 'with different branch' do
      let(:shift) { build(:shift, chair: build(:chair)) }

      permissions :calendar_create? do
        it { is_expected.not_to permit(admin, shift) }
      end
    end

    context 'with non-admin role' do
      let(:non_admin) { build(:user) }
      let(:shift) do
        loc = build(:location, branch: non_admin.branch)
        build(:shift, chair: build(:chair, location: loc))
      end

      permissions :deputy_export? do
        it { is_expected.not_to permit(non_admin) }
      end

      permissions :calendar_create? do
        it { is_expected.not_to permit(non_admin, shift) }
      end
    end
  end
end
