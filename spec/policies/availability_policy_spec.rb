# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AvailabilityPolicy do
  subject { described_class }

  let(:user) { build(:user, role: 'student') }
  let(:other_user) { build(:user, role: 'student') }
  let(:submitted) { build(:user, role: 'student', submitted: true) }
  let(:admin) { build(:user, role: 'admin') }
  let(:avail) { build(:availability, user: user) }
  let(:submitted_avail) { build(:availability, user: submitted) }

  # rubocop:disable RSpec/RepeatedExample
  context 'with non-admin role' do
    permissions :new?, :create?, :calendar?, :calendar_create? do
      it { is_expected.to permit(user, avail) }
    end

    permissions :destroy? do
      it { is_expected.to permit(user, avail) }
      it { is_expected.not_to permit(other_user, avail) }
    end

    permissions :availabilities_csv?, :delete_all? do
      it { is_expected.not_to permit(user, avail) }
    end
  end
  # rubocop:enable RSpec/RepeatedExample

  context 'with admin role' do
    permissions :destroy?, :availabilities_csv?, :delete_all? do
      it { is_expected.to permit(admin, avail) }
    end

    permissions :new?, :create?, :calendar?, :calendar_create? do
      it { is_expected.not_to permit(admin, avail) }
    end
  end

  context 'with user submitted is true' do
    permissions :new?, :create?, :calendar?, :calendar_create? do
      it { is_expected.not_to permit(submitted, submitted_avail) }
    end
  end
end
