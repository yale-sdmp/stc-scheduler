# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserPolicy do
  subject { described_class }

  context 'with non-admin role' do
    let(:user) { build(:user, role: 'student') }
    let(:other_user) { build(:user, role: 'student') }
    let(:submitted_user) { build(:user, role: 'student', submitted: true) }

    permissions :index?, :edit?, :update? do
      it { is_expected.to permit(user, user) }
      it { is_expected.not_to permit(user, other_user) }
    end

    permissions :show? do
      it { is_expected.not_to permit(user, user) }
    end

    permissions :edit?, :update? do
      it { is_expected.not_to permit(submitted_user, submitted_user) }
    end
  end

  # rubocop:disable RSpec/RepeatedExample
  context 'with role admin' do
    let(:admin) { build_stubbed(:user, role: 'admin') }
    let(:other_user) { build(:user, role: 'student') }

    permissions :show?, :destroy? do
      it { is_expected.to permit(admin, admin) }
      it { is_expected.to permit(admin, other_user) }
    end

    permissions :edit?, :update? do
      it { is_expected.to permit(admin, admin) }
      it { is_expected.not_to permit(admin, other_user) }
    end
  end
  # rubocop:enable RSpec/RepeatedExample
end
