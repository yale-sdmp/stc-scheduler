# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserPreferredLocationPolicy do
  subject { described_class }

  let(:student) { build(:user, role: 'student') }
  let(:submitted) { build(:user, role: 'student', submitted: true) }
  let(:admin) { build(:user, role: 'admin') }
  let(:upl_student) { build(:user_preferred_location, user: student) }
  let(:upl_submitted) { build(:user_preferred_location, user: submitted) }

  permissions :new?, :create? do
    it { is_expected.to permit(student, upl_student) }
    it { is_expected.not_to permit(submitted, upl_submitted) }
    it { is_expected.not_to permit(admin, upl_student) }
  end
end
