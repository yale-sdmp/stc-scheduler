# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'User Preferred Form', type: :system do
  context 'when CT is logged in' do
    let(:ct) { create(:branch, title: 'ct') }
    let(:ct_user) { create(:user, branch: ct) }
    let(:loc) { create(:location, branch: ct, name: 'Zone 1') }

    before { ct_setup }

    context 'when the user has already chosen a preferred location' do
      it 'has that upl.location selected' do
        create(:user_preferred_location, user: ct_user, location: loc)
        visit new_user_preferred_location_path
        expect(page).to have_select('location_pref', selected: loc.name)
      end
    end

    it 'upon error, persists the submitted value' do
      create(:user_preferred_location, user: ct_user, location: loc)
      visit new_user_preferred_location_path
      first('#location_pref option').select_option
      click_on 'Next'
      expect(page).to have_select('location_pref', selected: nil)
    end

    it 'disables form fields when no preference box is checked', js: true do
      visit new_user_preferred_location_path
      find(:css, '#user_preferred_location_no_preference').set(true)
      expect(find('#location_pref')).to be_disabled
    end
  end

  context 'when ST is logged in' do
    let(:st) { create(:branch, title: 'st') }
    let!(:loc1) { create(:location, branch: st) }
    let!(:loc2) { create(:location, branch: st) }
    let(:st_user) { create(:user, branch: st, max_hours: 10) }

    before { login_as st_user }

    # rubocop:disable RSpec/MultipleExpectations
    context 'when the user already has upls' do
      it 'has number fields with preferred_hours filled in' do
        st_setup_upls
        expect(page).to have_field(loc1.name, with: 1)
        expect(page).to have_field(loc2.name, with: 2)
      end
    end

    it 'upon error, persists the submitted values' do
      visit new_user_preferred_location_path
      fill_in loc1.name, with: 11
      click_on 'Next'
      expect(page).to have_field(loc1.name, with: 11)
      expect(page).to have_field(loc2.name, with: 0)
    end
    # rubocop:enable RSpec/MultipleExpectations

    context 'when the user does not have upls' do
      it 'has one number field for each branch.location' do
        visit new_user_preferred_location_path
        expect(page).to have_selector('input[type="number"]', count: 2)
      end
    end

    it 'disables form fields when no preference box is checked', js: true do
      visit new_user_preferred_location_path
      find(:css, '#user_preferred_location_no_preference').set(true)
      expect(find("#location_pref_#{loc1.id}")).to be_disabled
    end
  end

  context 'when SDMP is logged in' do
    let(:sdmp) { create(:branch, title: 'sdmp') }

    before do
      login_as create(:user, branch: sdmp)
      visit new_user_preferred_location_path
    end

    it 'does not display location preference form' do
      expect(page).to have_text('No locations. Please click next.')
    end

    it 'can navigate to availabilities calendar' do
      click_on 'Next'
      expect(page).to have_css('h2', text: 'Add Availability')
    end

    it 'can navigate back to user info page' do
      click_on 'Back'
      expect(page).to have_css('h2', text: 'Add User Information')
    end
  end

  context 'when no branch is selected' do
    before do
      login_as create(:user, branch: nil)
      visit new_user_preferred_location_path
    end

    it 'does not display location preference form' do
      expect(page).to have_text('Please select your branch in the user profile')
    end

    it 'can navigate back to user info page' do
      click_on 'Back'
      expect(page).to have_css('h2', text: 'Add User Information')
    end
  end

  private

  def ct_setup
    login_as ct_user
    create(:location, branch: ct)
    create(:location, branch: ct)
  end

  def st_setup_upls
    create(:user_preferred_location, user: st_user, location: loc1,
                                     preferred_hours: 1)
    create(:user_preferred_location, user: st_user, location: loc2,
                                     preferred_hours: 2)
    visit new_user_preferred_location_path
  end
end
