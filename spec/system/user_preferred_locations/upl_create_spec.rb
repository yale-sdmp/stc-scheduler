# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'User Preferred Location Creation', type: :system do
  context 'when CT is logged in' do
    let(:ct) { create(:branch, title: 'ct') }
    let(:ct_user) { create(:user, branch: ct) }

    before { ct_setup }

    it 'displays success message for successful create' do
      create_ct_upl
      expect(page).to have_content('Successfully saved location preferences!')
    end

    it 'displays success message when no preference box is checked' do
      check_no_preference
      expect(page).to have_content('Successfully saved location preferences!')
    end

    it 'can navigate to availabilities calendar' do
      create_ct_upl
      expect(page).to have_css('h2', text: 'Add Availability')
    end

    it 'can navigate back to user info page' do
      click_on 'Back'
      expect(page).to have_css('h2', text: 'Add User Information')
    end
  end

  context 'when ST is logged in' do
    let(:st) { create(:branch, title: 'st') }
    let(:loc1) { create(:location, branch: st) }
    let(:loc2) { create(:location, branch: st) }
    let(:st_user) { create(:user, branch: st, min_hours: 5, max_hours: 10) }

    before { st_setup }

    it 'displays success message for successful update' do
      create_st_upl(5, 2)
      expect(page).to have_content('Successfully saved location preferences!')
    end

    it 'displays error message when preferred hours greater than max' do
      create_st_upl(5, 8)
      expect(page).to have_text('must not be greater than maximum')
    end

    it 'displays error message when preferred hours less than min' do
      create_st_upl(1, 1)
      expect(page).to have_text('must not be less than minimum')
    end

    it 'displays success message when no preference box is checked' do
      check_no_preference
      expect(page).to have_content('Successfully saved location preferences!')
    end

    it 'can navigate to availabilities calendar' do
      create_st_upl(5, 2)
      expect(page).to have_css('h2', text: 'Add Availability')
    end

    it 'can navigate back to user info page' do
      click_on 'Back'
      expect(page).to have_css('h2', text: 'Add User Information')
    end
  end

  private

  def ct_setup
    login_as ct_user
    create(:location, branch: ct)
    create(:location, branch: ct)
    visit new_user_preferred_location_path
  end

  def create_ct_upl
    select_option('#location_pref', ct.locations.first.name)
    click_on 'Next'
  end

  def st_setup
    login_as st_user
    create(:user_preferred_location, user: st_user, location: loc1,
                                     preferred_hours: 1)
    create(:user_preferred_location, user: st_user, location: loc2,
                                     preferred_hours: 2)
    visit new_user_preferred_location_path
  end

  def create_st_upl(time1, time2)
    fill_in "location_pref_#{loc1.id}", with: time1
    fill_in "location_pref_#{loc2.id}", with: time2
    click_on 'Next'
  end

  def sdmp_setup
    login_as sdmp_user
    visit new_user_preferred_location_path
  end

  def check_no_preference
    find(:css, '#user_preferred_location_no_preference').set(true)
    click_on 'Next'
  end

  def select_option(css_selector, value)
    find(:css, css_selector).find(:option, value).select_option
  end
end
