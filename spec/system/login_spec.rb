# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Login', type: :system do
  context 'when student logs in' do
    let(:user) { build(:user, submitted: false, role: 'student') }

    before { stub_cas(user.username) }

    it 'redirects to user info page if not submitted' do
      login
      expect(page).to have_css('h2', text: 'Add User Information')
    end

    it 'redirects to review page if submitted' do
      user.update(submitted: true)
      login
      expect(page).to have_css('h2', text: 'Review and Submit')
    end
  end

  context 'when user first signs in' do
    let(:uname) { FFaker::Internet.user_name }

    before { stub_cas(uname) }

    it 'creates new user with correct netid' do
      create(:user, email: nil)
      visit root_path
      click_on 'Log In'
      expect(User.where(username: uname).size).to eq 1
    end
  end

  def login
    visit root_path
    click_on 'Log In'
  end
end
