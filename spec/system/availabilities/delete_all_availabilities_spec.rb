# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Bulk delete availabilities button', type: :system do
  let(:admin) do
    create(:user, role: 'admin')
  end
  let(:non_admin) do
    create(:user, role: 'student')
  end

  let(:button_str) { 'Delete all availabilities' }

  it 'appears for admins' do
    login_as admin
    visit root_path
    expect(page).to have_selector(:link_or_button, button_str)
  end

  it 'does not appear for non-admins' do
    login_as non_admin
    visit root_path
    expect(page).not_to have_selector(:link_or_button, button_str)
  end

  it 'deletes all availabilities for that users branch' do
    create(:availability, user: admin)
    login_as admin
    visit root_path
    expect { click_on button_str }.to change { get_num_avails(admin) }.by(-1)
  end

  def get_num_avails(user)
    Availability.where(user: user.branch.users).size
  end
end
