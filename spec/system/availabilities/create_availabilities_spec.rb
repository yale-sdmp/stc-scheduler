# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Availability creation', type: :system do
  before { login_as create(:user) }

  it 'succeeds' do
    expect { create_avail }.to change(Availability, :count).by(1)
  end

  it 'displays success message for successful create' do
    create_avail
    expect(page).to have_content('Successfully added availability!')
  end

  it 'displays error message for start time after end time' do
    create_avail('1:15 PM', '1:30 AM')
    expect(page).to have_content('Start time must be before end time')
  end

  it 'displays error message for overlap' do
    create_avail('8:00 AM', '12:00 AM')
    create_avail
    expect(page).to have_content(
      'Availability overlaps with existing availability'
    )
  end

  it 'succeeds after failed create' do
    create_avail('1:15 PM', '8:30 AM') # unsuccessful
    select_option('#availability_end_time', '9:00 AM')
    click_on 'Add Availability'
    expect(page).to have_content('Successfully added availability!')
  end

  def create_avail(start_time = '8:00 AM', end_time = '9:00 AM')
    visit new_availability_path
    select_option('#availability_day', 'Sunday')
    select_option('#availability_start_time', start_time)
    select_option('#availability_end_time', end_time)
    click_on 'Add Availability'
  end

  def select_option(css_selector, value)
    find(:css, css_selector).find(:option, value).select_option
  end
end
