# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Availability Table', type: :system do
  let(:user) { create(:user) }

  before { setup }

  context 'when user is signed in' do
    it 'displays a list of all availabilities' do
      within('tbody') do
        expect(page).to have_xpath('.//tr', count: 2)
      end
    end

    it 'can be deleted' do
      within('table.availabilities-table') do
        click_on 'X', match: :first
      end
      expect(page).to have_content('Successfully deleted availability!')
    end
  end

  def setup
    login_as user
    create(:availability, user: user, start_time: 0, end_time: 15)
    create(:availability, user: user, start_time: 1200, end_time: 1300)
    visit availabilities_path
  end
end
