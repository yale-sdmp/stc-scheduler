# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Availability deletion', type: :system do
  let(:user) do
    create(:user, role: 'student')
  end

  before do
    login_as user
    create(:availability, user: user)
  end

  it 'flashes message on success' do
    visit availabilities_path
    click_on 'X'
    expect(page).to have_content('Successfully deleted availability!')
  end

  it 'redirects to dashboard when called from dashboard' do
    visit dashboards_path
    click_on 'X'
    expect(page).to have_css('h2', text: 'Review and Submit')
  end

  it 'redirects to /availabilities when called from /availabilities' do
    visit availabilities_path
    click_on 'X'
    expect(page).to have_css('h2', text: 'Availabilities')
  end

  context 'when called from calendar' do
    it 'deletes the correct availability' do
      avail = create(:availability, user: user,
                                    day: 6, start_time: 800, end_time: 1000)
      visit calendar_availabilities_path
      find('td#6-800').click_on 'X'
      expect(Availability.exists?(avail.id)).to eq(false)
    end

    it 'redirects to calendar on success' do
      visit calendar_availabilities_path
      click_on 'X'
      expect(page).to have_css('.calendar-table')
    end
  end

  it 'cannot delete when user.submitted is true' do
    user.update!(submitted: true)
    visit dashboards_path
    expect(page).to have_button('X', disabled: true)
  end
end
