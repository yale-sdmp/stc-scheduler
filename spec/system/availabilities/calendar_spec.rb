# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Calendar', type: :system do
  let(:user) { create(:user, first_name: 'Student', last_name: 'Student') }
  let(:slots_per_hour) { 60 / MINUTE_STEP }

  before { setup }

  describe '\'s live readout' do
    it 'gives live update of number of hours selected', js: true do
      find(:id, '0-830').click
      expect(page.find('#number_of_hours')).to \
        have_text((MINUTE_STEP / 60.0).to_s)
    end

    it 'gives "hour" when 1 hour is selected', js: true do
      tds = page.find('#calendar-table').find_all('td[id]')
      tds.take(slots_per_hour).each(&:click)
      expect(page.find('#number_of_hours')).not_to have_text('hours selected')
    end

    it 'includes previously submitted hours in total', js: true do
      a = create(:availability, user: user.reload)
      refresh
      page.find(id: "#{Availability.days[a.day] + 1}-#{a.start_time}").click
      expect(page.find('#number_of_hours')).to \
        have_text(((a.length + MINUTE_STEP) / 60.0).to_s)
    end
  end

  it 'makes slot available when clicked on', js: true do
    slot = page.find(:id, '0-830')
    slot.click
    expect(page.find(:css, '.highlighted')).to eq(page.find(:id, '0-830'))
  end

  it 'makes slots available when when dragged over', js: true do
    slot1 = Slot.new(day: 'Sunday', start: 830)
    slot2 = slot1.succ
    page.find(:id, slot1.to_id).drag_to(page.find(:id, slot2.to_id))
    avail_ids = page.find_all(:css, '.highlighted').pluck(:id)
    (slot1..slot2).each { |slot| expect(avail_ids).to include(slot.to_id) }
  end

  it 'makes available slot unavailable on click', js: true do
    slot = page.find(:id, '0-830')
    slot.click
    expect { slot.click }.to change { slot[:class] }
  end

  it 'does not make time label cells available on click', js: true do
    slot = page.find(:id, '1000')
    expect { slot.click }.not_to change { slot[:class] }
  end

  it 'does not make day label cells available on click', js: true do
    slot = page.find(:id, 'Sunday')
    expect { slot.click }.not_to change { slot[:class] }
  end

  it 'submits selected availabilities', js: true do
    page.find(id: '0-830').click
    page.find(id: '1-1030').click
    expect { submit }.to \
      change { user.reload.availabilities.size }.by(2)
  end

  it 'shows error if the user submits an existing availability', js: true do
    a = create(:availability, user: user)
    page.find(id: "#{Availability.days[a.day]}-#{a.start_time}").click
    submit
    expect(page).to have_text('overlaps')
  end

  it 'prevents users from painting over submitted availabilities', js: true do
    a = create(:availability, user: user)
    refresh
    page.find(id: a.slots[1].to_id).click
    expect(page).to have_css('.available', count: 0)
  end

  context 'when navigated to from another page' do
    it 'loads javascript', js: true do
      visit dashboards_path
      click_on '3. Add Availability'
      page.find(:id, '0-830').click
      expect(page.find(:css, '.highlighted')).to eq(page.find(:id, '0-830'))
    end
  end

  def setup
    login_as user
    visit calendar_availabilities_path
  end

  def submit
    click_on 'Next'
  end
end
