# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'User Edit', type: :system do
  describe 'as admin' do
    let(:user) { create(:user, role: 'admin', target_hours: nil) }

    before { visit_edit_path(user) }

    it 'succeeds even if the student-required fields are nil' do
      first_name = FFaker::Name.first_name
      fill_in 'First name', with: first_name
      click_on 'Next'
      expect(page).to have_text(first_name)
    end

    def visit_edit_path(user_)
      login_as user_
      visit root_path
      click_on user_.username
      click_on 'Profile'
      click_on 'Edit'
    end
  end

  describe 'as student' do
    let(:user) { create(:user, lead: false, meal_plan: false, role: 'student') }

    before do
      login_as user
      visit edit_user_path(user)
    end

    it 'can navigate to the edit page' do
      visit root_path
      click_on '1. Add User Information'
      expect(page).to have_content('Add User Information')
    end

    it 'on success, it redirects to upl form' do
      click_on 'Next'
      expect(page).to have_css('h2', text: 'Add Location Preferences')
    end

    # rubocop:disable RSpec/ExampleLength
    it 'can edit name' do
      first_name = FFaker::Name.first_name
      last_name = FFaker::Name.last_name

      fill_in 'user_first_name', with: first_name
      fill_in 'user_last_name', with: last_name
      click_on 'Next'
      visit root_path
      expect(page).to have_content("#{first_name} #{last_name}")
    end
    # rubocop:enable RSpec/ExampleLength

    it 'can edit email' do
      email = FFaker::Internet.email

      fill_in 'user_email', with: email
      click_on 'Next'
      visit root_path

      expect(page).to have_content(email)
    end

    it 'can edit minimum hours' do
      hours = 1

      fill_in 'user_min_hours', with: hours
      click_on 'Next'
      visit root_path

      expect(page).to have_content("Minimum hours: #{hours}")
    end

    it 'can edit maximum hours' do
      hours = user.target_hours

      fill_in 'user_max_hours', with: hours
      click_on 'Next'
      visit root_path

      expect(page).to have_content("Maximum hours: #{hours}")
    end

    it 'can edit target hours' do
      hours = user.max_hours
      fill_in 'user_target_hours', with: hours
      click_on 'Next'
      visit root_path

      expect(page).to have_content("Target hours: #{hours}")
    end

    it 'can edit minimum shift length' do
      hours = 1
      fill_in 'user_min_shift', with: hours
      click_on 'Next'
      visit root_path

      expect(page).to have_content("Minimum shift length: #{hours}")
    end

    it 'can edit maximum shift length' do
      hours = 3
      fill_in 'user_max_shift', with: hours
      click_on 'Next'
      visit root_path

      expect(page).to have_content("Maximum shift length: #{hours}")
    end

    it 'can edit meal plan status' do
      choose 'user_meal_plan_true'
      click_on 'Next'
      visit root_path

      expect(page).to have_content('On Yale meal plan?: Yes')
    end

    it 'can edit lead status' do
      choose 'user_lead_true'
      click_on 'Next'
      visit root_path

      expect(page).to have_content('Currently a lead?: Yes')
    end

    it 'can edit branch' do
      branch = Branch.last.full_title
      select branch, from: 'user_branch_id'
      click_on 'Next'
      visit root_path

      expect(page).to have_content(branch)
    end

    it 'cannot submit blank branch' do
      first('#user_branch_id option').select_option
      click_on 'Next'
      expect(page).to have_content("Branch can't be blank")
    end

    it 'can edit special requests' do
      request = FFaker::Lorem.sentence

      fill_in 'user_special_requests', with: request
      click_on 'Next'
      visit root_path

      expect(page).to have_content(request)
    end
  end
end
