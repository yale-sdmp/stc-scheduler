# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'User Submit', type: :system do
  let(:user) { create(:user, role: 'student', submitted: false) }

  it 'changes user.submitted to true' do
    login_as user
    visit dashboards_path
    expect { click_on 'Submit' }.to change { user.reload.submitted }.to(true)
  end
end
