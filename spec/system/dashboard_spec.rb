# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Dashboard', type: :system do
  let(:user) { create(:user, first_name: 'Student', last_name: 'Student') }
  let(:admin) do
    create(:user, role: 'admin',
                  first_name: 'Admin',
                  last_name: 'OAdmin',
                  branch: user.branch)
  end
  let(:non_admin) { create(:user, role: 'student') }

  context 'when not signed in' do
    it 'redirects to unauthenticated page' do
      visit root_path
      expect(page).to have_content('Log In')
    end
  end

  context 'when signed in as user' do
    before { setup }

    it 'displays submission status' do
      expect(page).to have_content('No')
    end

    it 'displays availabilities' do
      within('tbody') do
        expect(page).to have_xpath('.//tr', count: 1)
      end
    end
  end

  describe 'Deputy export button' do
    let(:export_dep_button) { 'Export shifts to Deputy' }

    context 'when signed in as student' do
      before { setup }

      it 'is not present' do
        expect(page).not_to have_button(export_dep_button)
      end
    end

    context 'when signed is as admin' do
      before do
        login_as admin
        visit root_path
      end

      it 'is present' do
        expect(page).to have_button(export_dep_button)
      end

      it 'gives downloadable file' do
        click_on export_dep_button
        cont_disp = page.response_headers['Content-Disposition']
        expect(cont_disp).to match('attachment')
      end

      it 'gives expected shift' do
        shift = create(:shift, user: create(:user, branch: admin.branch))
        click_on export_dep_button
        expect(page.html).to include(shift.user.full_name)
      end
    end
  end

  describe 'CSV exporting' do
    let(:exp_avail_button) { 'Export availabilities' }

    context 'when signed in as admin' do
      before do
        create_availabilities
        login_as admin
        visit root_path
      end

      it 'has export availabilities button' do
        expect(page).to have_button(exp_avail_button)
      end

      it 'has export availabilities button that gives downloadable file' do
        click_on exp_avail_button
        cont_disp = page.response_headers['Content-Disposition']
        expect(cont_disp).to match('attachment')
      end

      it '\'s export button gives a CSV with the right content' do
        click_on exp_avail_button
        fname = Rails.root.join('spec/fixtures/availability_csv_generator/availabilities.csv')
        expect(page.html).to eq(File.read(fname))
      end
    end

    context 'when signed in as non_admin' do
      it 'does not have export availabilities button' do
        login_as non_admin
        visit root_path
        expect(page).not_to have_button('Export availabilities')
      end
    end

    def create_availabilities
      create(:availability, user: user,
                            day: 'Sunday',
                            start_time: 1159,
                            end_time: 1222)
      create(:availability, user: user,
                            day: 'Sunday',
                            start_time: 1201,
                            end_time: 1252)
    end
  end

  describe 'Unsubmitted Report' do
    context 'when user role is admin' do
      it 'displays button to get unsubmitted report' do
        login_as admin
        visit root_path
        expect(page).to have_selector(:link_or_button, 'Unsubmitted Report')
      end

      it 'displays button to email incomplete users' do
        login_as admin
        visit root_path
        expect(page).to have_selector(:link_or_button, 'Send Reminder Email')
      end
    end

    context 'when user role is non-admin' do
      it 'does not display button to get unsubmitted report' do
        login_as non_admin
        visit root_path
        expect(page).not_to have_selector(:link_or_button, 'Unsubmitted Report')
      end

      it 'does not display button to email incomplete users' do
        login_as non_admin
        visit root_path
        expect(page).not_to have_selector(:link_or_button,
                                          'Email All Incomplete')
      end
    end

    it 'displays students that have not submitted availabilities' do
      create_data_and_visit_dashboard
      click_on 'Unsubmitted Report'
      within('tbody') do
        expect(page).to have_xpath('.//tr', count: 3)
      end
    end

    # This will result in one submitted student, two unsubmitted students,
    #   and an unsubmitted admin
    def create_data_and_visit_dashboard
      create(:user, branch: admin.branch)
      create(:user, branch: admin.branch, submitted: true)
      create(:user)
      login_as admin
      visit dashboards_path
    end
  end

  def setup
    login_as user
    create(:availability,
           user: user,
           day: 'Sunday',
           start_time: 0,
           end_time: 15)
    visit root_path
  end
end
