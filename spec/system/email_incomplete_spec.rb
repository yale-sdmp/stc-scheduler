# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Dashboard', type: :system do
  let(:ct) { create(:branch, title: 'ct') }
  let(:st) { create(:branch, title: 'st') }
  let(:admin) { create(:user, role: 'admin', branch: ct) }

  context 'when emails sent successfully' do
    before { setup }

    it 'emails students that have not submitted availabilities' do
      create(:user, role: 'student', branch: ct, submitted: false)
      expect { click_on 'Send Reminder Email' }.to change {
        ActionMailer::Base.deliveries.count
      }.by(1)
    end

    it 'does not email admins that have not submitted availabilities' do
      create(:user, role: 'admin', branch: ct, submitted: false)
      expect { click_on 'Send Reminder Email' }.to change {
        ActionMailer::Base.deliveries.count
      }.by(0)
    end

    it 'does not email students that have submitted availabilities' do
      create(:user, role: 'student', branch: ct, submitted: true)
      expect { click_on 'Send Reminder Email' }.to change {
        ActionMailer::Base.deliveries.count
      }.by(0)
    end

    it 'does not email students in other branches' do
      create(:user, role: 'student', branch: st, submitted: false)
      expect { click_on 'Send Reminder Email' }.to change {
        ActionMailer::Base.deliveries.count
      }.by(0)
    end
  end

  context 'when there is a delivery error' do
    let(:fail_user) { create(:user, branch: ct, first_name: 'test') }

    before do
      setup
      copy = double
      allow(UserMailer).to receive(:email_incomplete_user).and_return(copy)
      allow(copy).to receive(:deliver).and_raise StandardError
    end

    it 'displays failed email when there is a delivery error' do
      fail_user
      click_on 'Send Reminder Email'
      expect(page).to have_text(fail_user.email)
    end
  end

  # rubocop:disable Metrics/AbcSize
  def setup
    login_as admin
    visit root_path
    allow(ENV).to receive(:[]).and_call_original
    allow(ENV).to receive(:[]).with('DB_HOST_DEV')
                              .and_return('stc_scheduler_development')
    allow(ENV).to receive(:[]).with('MANDRILL_FROM_NAME')
                              .and_return('STC-Scheduler')
    allow(ENV).to receive(:[]).with('MANDRILL_FROM_EMAIL')
                              .and_return('stc-scheduler@example.com')
  end
  # rubocop:enable Metrics/AbcSize
end
