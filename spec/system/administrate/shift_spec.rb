# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Shifts administrate dashboard', type: :system do
  describe 'user ordering' do
    before do
      admin = create(:user, role: 'admin')
      login_as admin
      chair = create(:chair, location: create(:location, branch: admin.branch))
      create_list(:shift, 5, chair: chair)
      visit admin_shifts_path
      click_on 'User'
    end

    let(:usr) do
      find_all(:css, '.js-table-row').map { |tr| tr.find_all('td').first }
    end

    it 'orders by last name, first name' do
      last_names = usr.map { |u| u.text.split[1] }
      (0..(last_names.size - 2)).each do |i|
        expect(last_names[i]).to be <= last_names[i + 1]
      end
    end

    it 'sorts properly in reverse order too' do
      click_on('User')
      last_names = usr.map { |u| u.text.split[1] }
      (0..(last_names.size - 2)).each do |i|
        expect(last_names[i]).to be >= last_names[i + 1]
      end
    end
  end
end
