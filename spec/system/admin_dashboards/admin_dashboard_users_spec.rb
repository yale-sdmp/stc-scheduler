# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Admin Dashboard User Checks', type: :system do
  let(:branch1) { create(:branch, title: 'sdmp') }
  let(:branch2) { create(:branch, title: 'ct') }
  let(:user) { create(:user, role: 'admin', branch: branch1) }

  before { setup }

  it 'display student when they belong to same branch' do
    create(:user, role: 'student', branch: branch1,
                  first_name: 'John', last_name: 'Smith')
    visit users_admin_dashboard_index_path
    expect(page).to have_text('John Smith')
  end

  it 'doesnt display students that are of different branch' do
    create(:user, role: 'student', branch: branch2,
                  first_name: 'John', last_name: 'Smith')
    visit users_admin_dashboard_index_path
    expect(page).not_to have_text('John Smith')
  end

  it 'displays student availabilities' do
    user2 = create(:user, role: 'student', branch: branch1,
                          first_name: 'John', last_name: 'Smith')
    create(:availability, user: user2, start_time: 1400, end_time: 1600)
    visit users_admin_dashboard_index_path(user_id: user2.id)
    expect(page).to have_text('2:00 PM to 4:00 PM')
  end

  # rubocop:disable RSpec/ExampleLength
  it 'displays correct percentage of hours reached' do
    user2 = create(:user, role: 'student', branch: branch1,
                          first_name: 'John', last_name: 'Smith',
                          target_hours: 10)
    setup_shift(user2, 1400, 1600, 1)
    visit users_admin_dashboard_index_path
    expect(page).to have_text('20%')
  end
  # rubocop:enable RSpec/ExampleLength

  it 'displays 0% when no hours reached' do
    create(:user, role: 'student', branch: branch1,
                  first_name: 'John', last_name: 'Smith', target_hours: 10)
    visit users_admin_dashboard_index_path
    expect(page).to have_text('0%')
  end

  it 'displays error message when no target hours specified' do
    create(:user, role: 'student', branch: branch1,
                  first_name: 'John', last_name: 'Smith', target_hours: nil)
    visit users_admin_dashboard_index_path
    expect(page).to have_text('Form incomplete')
  end

  it 'clicking student name displays his availabilities' do
    user2 = create(:user, role: 'student', branch: branch1,
                          first_name: 'John', last_name: 'Smith')
    create(:availability, user: user2, start_time: 1400, end_time: 1600)
    click_user_link(user2)
    expect(page).to have_text('2:00 PM to 4:00 PM')
  end

  def click_user_link(user)
    visit users_admin_dashboard_index_path
    click_link("user_id_#{user.id}")
  end

  def setup_shift(user, start_time, end_time, day)
    loc = create(:location, branch: branch1)
    c = create(:chair, start_time: 1400, end_time: 1600, location: loc)
    create(:shift, user: user, start_time: start_time,
                   end_time: end_time, day: day, chair: c)
  end

  def setup
    login_as(user)
  end
end
