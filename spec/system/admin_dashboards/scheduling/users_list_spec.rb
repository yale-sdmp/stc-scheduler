# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Users list', type: :system do
  let(:user) { create(:user, role: 'student') }
  let(:admin) { create(:user, role: 'admin', branch: user.branch) }
  let(:location) { create(:location, branch: user.branch) }
  let(:chair) { create(:chair, location: location) }

  before { setup }

  describe 'users list' do
    it 'shows the users in the branch corresponding to the location' do
      visit_location(location, chair: chair)
      user_list = page.find(:id, 'users-list')
      expect(user_list).to have_text(user.full_name)
    end

    it 'does not show users not in the branch' do
      user_other_branch = create(:user)
      visit_location(location)
      users_list = page.find(:id, 'users-list')
      expect(users_list).not_to have_text(user_other_branch.first_name)
    end

    it 'greys out users that are unavailable' do
      chair = create(:chair, location: location, start_time: 1200,
                             end_time: 1600, day: 'Friday')
      visit_location(location, chair: chair)
      expect(page).to have_css('.greyed-out')
    end

    context 'when user clicks on unselected name' do
      before do
        create(:availability, user: user)
        visit_location(location, chair: chair)
        within '#users-list' do
          click_on user.full_name.to_s
        end
      end

      it 'goes to link with correct params' do
        expect(page.current_url).to have_text("user=#{user.id}")
      end

      it 'shows mini calendar with the users availabilities and shifts' do
        expect(page).to have_css('#mini-calendar .mini-available')
      end
    end

    context 'when user clicks on selected name' do
      it 'deselects that name' do
        visit_location(location, user: user.id, chair: chair)
        within '#users-list' do
          click_on user.full_name.to_s
        end
        expect(page.current_url).not_to have_text("user=#{user.id}")
      end
    end

    context 'when clicking special request icon' do
      before { user.update(special_requests: 'My Special Request') }

      # rubocop:disable RSpec/ExampleLength
      it 'displays the special request when it is hidden', js: true do
        visit_location(location, user: user.id, chair: chair)
        within find('.card-header', text: user.full_name.to_s) do
          find('.special-request-icon').click
          expect(find('.special-request-text',
                      text: user.special_requests)[:class])
            .not_to include('collapse')
        end
      end

      it 'hides the special request when it is shown', js: true do
        visit_location(location, user: user.id, chair: chair)
        within find('.card-header', text: user.full_name.to_s) do
          2.times { find('.special-request-icon').click }
          expect(find('.special-request-text',
                      text: user.special_requests)[:class])
            .to include('collapse')
        end
      end
    end
    # rubocop:enable RSpec/ExampleLength
  end

  describe 'when sorted' do
    it 'retains sorting when clicking on user', js: true do
      visit_location(location)
      find("option[value='special_requests']").click
      within('#users-list') { click_on user.full_name.to_s }
      expect(page.find('#filter-dropdown')[:value]).to eq 'special_requests'
    end

    context 'when chair is selected' do
      before do
        chair
        visit_location(location)
        find("option[value='special_requests']").click
        find(:id, chair.slots.first.to_id).click
      end

      it 'retains sorting when clicking on chair', js: true do
        expect(page.find('#filter-dropdown')[:value]).to eq 'special_requests'
      end

      it 'retains sorting when exiting chair selection', js: true do
        click_on 'Done'
        expect(page.find('#filter-dropdown')[:value]).to eq 'special_requests'
      end
    end

    context 'with special request' do
      let(:s_user) do
        create(:user, role: 'student',
                      branch: user.branch,
                      special_requests: 'Special Request')
      end

      before do
        s_user
        visit_location(location)
        find("option[value='special_requests']").click
      end

      it 'greys out users without special request', js: true do
        expect(find('.card-header', text: user.full_name.to_s)
        .find(:xpath, '../..')[:class]).to include('greyed-out')
      end

      it 'does not grey out users with special request', js: true do
        expect(find('.card-header', text: s_user.full_name.to_s)
        .find(:xpath, '../..')[:class]).not_to include('greyed-out')
      end

      it 'has special request users above other users', js: true do
        user_id = find('.card-header', text: user.full_name.to_s)
                  .find(:xpath, '../..')[:id]
        special_user_id = find('.card-header', text: s_user.full_name.to_s)
                          .find(:xpath, '../..')[:id]
        expect(page).to have_css("##{special_user_id} ~ ##{user_id}")
      end
    end

    context 'with position' do
      let(:lead_user) do
        create(:user, role: 'student',
                      branch: user.branch,
                      lead: true)
      end

      before do
        lead_user
        visit_location(location)
        find("option[value='lead']").click
      end

      it 'greys non-lead users', js: true do
        expect(find('.card-header', text: user.full_name.to_s)
        .find(:xpath, '../..')[:class]).to include('greyed-out')
      end

      it 'does not grey out lead users', js: true do
        expect(find('.card-header', text: lead_user.full_name.to_s)
        .find(:xpath, '../..')[:class]).not_to include('greyed-out')
      end

      it 'has lead users above non-lead users', js: true do
        user_id = find('.card-header', text: user.full_name.to_s)
                  .find(:xpath, '../..')[:id]
        lead_user_id = find('.card-header', text: lead_user.full_name.to_s)
                       .find(:xpath, '../..')[:id]
        expect(page).to have_css("##{lead_user_id} ~ ##{user_id}")
      end
    end

    context 'with hour differential' do
      let(:other_user) do
        create(:user, role: 'student',
                      branch: user.branch,
                      target_hours: user.target_hours)
      end

      before do
        other_user
        create(:shift, user: user)
        visit_location(location)
        find("option[value='hour_differential']").click
      end

      it 'has higher differential user first', js: true do
        shift_user_id = find('.card-header', text: user.full_name.to_s)
                        .find(:xpath, '../..')[:id]
        other_user_id = find('.card-header', text: other_user.full_name.to_s)
                        .find(:xpath, '../..')[:id]
        expect(page).to have_css("##{other_user_id} ~ ##{shift_user_id}")
      end
    end
  end

  def setup
    login_as admin
  end

  def visit_location(loc, params = {})
    opts = { controller: 'admin_dashboards',
             action: 'scheduling',
             location: loc,
             only_path: true }

    visit url_for(opts.merge(params))
  end
end
