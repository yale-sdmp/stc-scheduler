# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Scheduling interface calendar', type: :system do
  let(:user) { create(:user, role: 'student') }
  let(:admin) { create(:user, role: 'admin', branch: user.branch) }
  let(:location) { create(:location, branch: user.branch) }
  let(:calendar_id) { 'shifts-calendar' }
  let(:hl_class) { HIGHLIGHT_STR }
  let(:chair) do
    create(:chair, location: location,
                   start_time: 800,
                   end_time: 900,
                   day: 'Sunday')
  end

  before do |example|
    setup
    page.current_window.resize_to(1920, 1000) if example.metadata[:js]
  end

  context 'when only user is selected' do
    it 'does not show calendar' do
      visit_location(location, user: user)
      expect(page).not_to have_selector('#sched-panel')
    end
  end

  context 'when only chair is selected' do
    before { visit_location(location, chair: chair) }

    it 'does not highlight calendar cells on click', js: true do
      page.current_window.resize_to(1920, 1000)
      page.find(:id, calendar_id).find_all('td[id]').each do |cell|
        cell.click
        expect(cell[:class]).not_to include hl_class
      end
    end

    it 'does not show save button' do
      panel = page.find(:id, 'sched-panel')
      expect(panel.find_all('input').size).to eq 0
    end
  end

  context 'when selected chair is not in requested location' do
    it 'does not show shift scheduling panel' do
      bad_chair = create(:chair)
      visit_location(location, chair: bad_chair)
      expect(page).not_to have_selector('#sched-panel')
    end
  end

  describe '\'s Done button' do
    before do
      visit_location(location, chair: chair, user: user)
      click_on 'Done'
    end

    it 'exits chair view' do
      expect(page.current_url).to have_text("user=#{user.id}")
    end
  end

  describe 'unavailable slots' do
    it 'are given correct class' do
      visit_location(location, user: user, chair: chair)
      cal_cells = page.find_all(:css, '#shifts-calendar td[id]')
      cal_cells.each do |cell|
        expect(cell[:class]).to include UNAVAIL_STR
      end
    end

    it 'are not highlighted', js: true do
      visit_location(location, user: user, chair: chair)
      cell = page.find(:id, calendar_id).find_all('td[id]').first
      cell.click
      expect(cell[:class]).not_to include hl_class
    end
  end

  describe 'scheduled slots' do
    before do
      add_availability(chair, user)
      add_shift(chair, user)
    end

    it 'gives correct class to scheduled slots' do
      visit_location(location, user: user, chair: chair)
      cal_cells = page.find_all(:css, "##{calendar_id} td[id]")
      cal_cells.each do |cell|
        expect(cell[:class]).to include SCHEDULED_CLASS
      end
    end

    it 'does not display tooltips for same location slots' do
      visit_location(location, user: user, chair: chair)
      cal_cells = page.find_all(:css, "##{calendar_id} td[id]")
      cal_cells.each do |cell|
        expect(cell['data_toggle']).to be_nil
      end
    end

    it 'does not highlight scheduled slots', js: true do
      visit_location(location, user: user, chair: chair)
      cell = page.find(:id, calendar_id).find_all('td[id]').first
      cell.click
      expect(cell[:class]).not_to include hl_class
    end
  end

  describe 'slots scheduled at a different location' do
    before do
      add_availability(chair, user)
      add_shift(chair, user)
      diff_loc = create(:location, branch: user.branch)
      diff_chair = create(:chair, location: diff_loc,
                                  start_time: chair.start_time,
                                  end_time: chair.end_time,
                                  day: chair.day)
      visit_location(diff_loc, user: user, chair: diff_chair)
    end

    it 'displays tooltips with the correct location' do
      cal_cells = page.find_all(:css, "##{calendar_id} td[id]")
      cal_cells.each do |cell|
        expect(cell['title']).to eq(location.name)
      end
    end
  end

  describe 'available and unscheduled slots' do
    before do
      add_availability(chair, user)
      visit_location(location, user: user, chair: chair)
    end

    it 'are highlighted when clicked on', js: true do
      cell = page.find(:id, calendar_id).find_all('td[id]').first
      cell.click
      expect(cell[:class]).to include hl_class
    end

    it 'are highlighted when dragged over', js: true do
      slots = user.availabilities.first.slots
      cal = page.find(:id, calendar_id)
      cal.find(:id, slots[0].to_id).drag_to(cal.find(:id, slots[1].to_id))
      avail_ids = page.find_all(:css, '.highlighted').pluck(:id)
      slots[0..1].each { |slot| expect(avail_ids).to include(slot.to_id) }
    end

    it 'generate shift when submitted', js: true do
      page.find(:id, calendar_id).find(:id, Slot.from_chair(chair).to_id).click
      click_on 'Save'
      expect(user.reload.shifts.size).to eq 1
    end
  end

  describe 'currently scheduled users' do
    let(:sched_user) { create(:user, branch: location.branch) }

    before do
      add_shift(chair, sched_user)
    end

    it 'appear as in the row for the correct slot' do
      visit_location(location, chair: chair)
      chair.slots.each do |slot|
        tr = page.find(:css, "tr#t#{slot.int}")
        expect(tr.text).to include sched_user.full_name
      end
    end
  end

  describe 'availability column' do
    context 'when expanded' do
      it 'shows names of available users' do
        avail = add_availability(chair)
        visit_location(location, chair: chair)
        click_on 'expand'
        tr = page.find(:css, "tr#t#{chair.start_time}")
        expect(tr.text).to include avail.user.full_name
      end
    end

    context 'when collapsed' do
      it 'shows number of available users' do
        add_availability(chair)
        visit_location(location, chair: chair)
        tr = page.find(:css, "tr#t#{chair.start_time}")
        expect(tr.text).to include '1 available'
      end
    end
  end

  def setup
    chair
    login_as admin
  end

  def add_availability(chair, avail_user = nil)
    chair_params = chair.attributes.slice('day', 'start_time', 'end_time')
    chair_params[:user] = avail_user if avail_user
    avail = create(:availability, chair_params)
    avail.user.update(branch: location.branch) unless avail_user
    avail
  end

  def add_shift(chair, sched_user)
    add_availability(chair, sched_user)
    chair_params = chair.attributes.slice('day', 'start_time', 'end_time')
    create(:shift, chair_params.merge(user: sched_user, chair: chair))
  end

  def visit_location(loc, params = {})
    opts = { controller: 'admin_dashboards',
             action: 'scheduling',
             location: loc,
             only_path: true }

    visit url_for(opts.merge(params))
  end
end
