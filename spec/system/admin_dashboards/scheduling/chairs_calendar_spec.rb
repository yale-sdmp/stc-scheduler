# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Chairs calendar', type: :system do
  let(:user) { create(:user, role: 'student') }
  let(:admin) { create(:user, role: 'admin', branch: user.branch) }
  let(:location) { create(:location, branch: user.branch) }
  let(:location_no_chairs) { create(:location, branch: user.branch) }
  let(:chair) do
    create(:chair, location: location,
                   start_time: 800,
                   end_time: 900,
                   day: 'Sunday')
  end

  before { setup }

  describe 'chairs calendar' do
    it 'gives chair class to slots covered by chair' do
      visit_location(location)
      expect(chairs_calendar_find('0-800')[:class]).to have_text 'chair'
    end

    it 'does not give chair class to slots not covered by chair' do
      visit_location(location)
      expect(chairs_calendar_find('0-1000')[:class]).not_to have_text 'chair'
    end

    it 'does not give chair class when covered by another location\'s chairs' do
      visit_location(location_no_chairs)
      expect(chairs_calendar_find('0-800')[:class]).not_to have_text 'chair'
    end

    it 'goes to correct chair page upon clicking on cell in chair', js: true do
      visit_location(location)
      chairs_calendar_find('0-800').click
      expect(page.current_url).to have_text("chair=#{chair.id}")
    end

    it 'displays progress text in top cell of chair', js: true do
      location.update(continuous: true)
      visit_location(location)
      txt = chairs_calendar_find('0-800').find('div').text
      expect(txt).to include 'filled'
    end

    it 'displays progress bar styling in top cell of chair', js: true do
      location.update(continuous: true)
      visit_location(location)
      style = chairs_calendar_find('0-800').style('background')
      expect(style['background']).to include 'linear-gradient'
    end

    describe 'no-availability alert' do
      it 'appears for slots with no users available', js: true do
        visit_location(location)
        slot_cell = chairs_calendar_find(Slot.from_chair(chair).to_id)
        expect(slot_cell[:class]).to include 'no-avail-alert'
      end

      it 'does not appear for slots not within a chair' do
        visit_location(location)
        slot = Slot.from_chair(chair).succ
        slot_cell = chairs_calendar_find(slot.to_id)
        expect(slot_cell[:class]).not_to have_text 'no-avail-alert'
      end

      it 'does not appear for slots with available users' do
        chair_attribs = chair.attributes.slice('start_time', 'end_time', 'day')
        create(:availability, { user: user }.merge(chair_attribs))
        visit_location(location)
        slot_cell = chairs_calendar_find(Slot.from_chair(chair).to_id)
        expect(slot_cell[:class]).not_to have_text 'no-avail-alert'
      end
    end

    def chairs_calendar_find(id)
      within '#chairs-calendar' do
        page.find(:id, id)
      end
    end
  end

  def setup
    chair
    login_as admin
  end

  def visit_location(loc, params = {})
    opts = { controller: 'admin_dashboards',
             action: 'scheduling',
             location: loc,
             only_path: true }
    visit url_for(opts.merge(params))
  end
end
