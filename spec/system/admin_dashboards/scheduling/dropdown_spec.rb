# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Location dropdown', type: :system do
  let(:admin) { create(:user, role: 'admin') }
  let(:location) { create(:location, branch: admin.branch) }
  let(:other_location) { create(:location, branch: admin.branch) }

  before { setup }

  describe 'dropdown' do
    it 'has options for all branch locations' do
      other_location.reload
      visit_location(location)
      loc_names = admin.branch.reload.locations.map(&:name)
      options = find(:id, 'locations').find_all(:option).map(&:text)
      expect(options).to eq loc_names
    end

    it 'has requested location selected' do
      visit_location(location)
      selected = find(:id, 'locations').find('[selected="selected"]')
      expect(selected.text).to eq location.name
    end

    it 'goes to correct page upon selecting different location', js: true do
      other_location.reload
      visit_location(location)
      select other_location.name, from: 'locations'
      expect(page.current_url).to have_text("location=#{other_location.id}")
    end

    context 'when location not in current users branch is requested' do
      it 'does not show that location' do
        loc = create(:location)
        visit_location(loc)
        expect(page).not_to have_text(loc.name)
      end
    end
  end

  def setup
    location
    login_as admin
  end

  def visit_location(loc, params = {})
    opts = { controller: 'admin_dashboards',
             action: 'scheduling',
             location: loc,
             only_path: true }

    visit url_for(opts.merge(params))
  end
end
