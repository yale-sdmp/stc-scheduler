# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Admin Dashboard Heatmap Checks', type: :system do
  let(:branch1) { create(:branch, title: 'sdmp') }
  let(:branch2) { create(:branch, title: 'ct') }
  let(:user) { create(:user, role: 'admin', branch: branch1) }

  before { setup }

  it 'display slot content' do
    visit heatmap_admin_dashboard_index_path(slot_id: '1-1230')
    expect(page).to have_text('Mon 12:30')
  end

  it 'display no students when no students with same branch or availability' do
    user2 = create(:user, role: 'student', branch: branch2,
                          first_name: 'JohnSmith')
    create(:availability, user: user2, start_time: 1400, end_time: 1600)
    visit heatmap_admin_dashboard_index_path(slot_id: '1-1230')
    expect(page).not_to have_text(user2.first_name)
  end

  it 'display no students when students with same branch but no availability' do
    user2 = create(:user, role: 'student', branch: branch1,
                          first_name: 'JohnSmith')
    create(:availability, user: user2, start_time: 1400, end_time: 1600)
    visit heatmap_admin_dashboard_index_path(slot_id: '1-1230')
    expect(page).not_to have_text(user2.first_name)
  end

  it 'display none when different branch but same availability' do
    user2 = create(:user, role: 'student', branch: branch2,
                          first_name: 'JohnSmith')
    create(:availability, user: user2, start_time: 1100, end_time: 1600)
    visit heatmap_admin_dashboard_index_path(slot_id: '1-1230')
    expect(page).not_to have_text(user2.first_name)
  end

  it 'display students with matching branch and availability' do
    user2 = create(:user, role: 'student', branch: branch1,
                          first_name: 'JohnSmith')
    create(:availability, user: user2, start_time: 1100, end_time: 1600, day: 1)
    visit heatmap_admin_dashboard_index_path(slot_id: '1-1230')
    expect(page).to have_text(user2.first_name)
  end

  it 'links to slot view from heatmap view' do
    visit '/admin_dashboard/heatmap'
    click_link('3-1715_link')
    expect(page).to have_current_path(
      heatmap_admin_dashboard_index_path(slot_id: '3-1715')
    )
  end

  def setup
    login_as(user)
  end
end
