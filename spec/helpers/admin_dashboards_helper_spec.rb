# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AdminDashboardsHelper, type: :helper do
  let(:loc) { create(:location) }
  let(:chair) { create(:chair, location: loc) }

  describe '#scheduling_calendar_styling' do
    context 'when given location' do
      it 'gives chair class to the locations chairs' do
        styling = helper.scheduling_calendar_styling(loc, [])
        chair.slots.each do |slot|
          expect(styling.call(slot.to_id)[:class]).to include 'chair'
        end
      end

      it 'does not give chair class to other cells' do
        styling = helper.scheduling_calendar_styling(loc, [])
        slot = chair.slots.last.succ
        expect(styling.call(slot.to_id)[:class]).to be nil
      end

      it 'gives top-of-chair class to first cell in chair' do
        style = helper.scheduling_calendar_styling(loc, [])
        slot = chair.slots.first
        expect(style.call(slot.to_id)[:class]).to include 'top-of-chair'
      end

      it 'gives bottom-of-chair class to last cell in chair' do
        style = helper.scheduling_calendar_styling(loc, [])
        slot = chair.slots.last
        ch_bot_str = 'bottom-of-chair'
        expect(style.call(slot.to_id)[:class]).to include ch_bot_str
      end
    end
  end

  describe '#scheduling_calendar_classes' do
    it 'gives top-of-chair class for first slot' do
      id = '0-800'
      ids = %w(0-800 0-815)
      classes = helper.scheduling_calendar_classes(id, ids, [])
      expect(classes).to include('top-of-chair')
    end

    it 'gives bottom-of-chair class for last slot' do
      id = '0-815'
      ids = %w(0-800 0-815)
      classes = helper.scheduling_calendar_classes(id, ids, [])
      expect(classes).to include('bottom-of-chair')
    end

    it 'includes no-avail-alert if no users are available' do
      id = '0-815'
      ids = %w(0-800 0-815)
      classes = helper.scheduling_calendar_classes(id, ids, [])
      expect(classes).to include('no-avail-alert')
    end

    it 'omits alert if users are available' do
      u = make_available_user(chair)
      id = chair.slots.first.to_id
      ids = chair.slots.map(&:to_id)
      classes = helper.scheduling_calendar_classes(id, ids, [u])
      expect(classes).not_to include('no-avail-alert')
    end
  end

  describe '#shift_scheduling_styling' do
    context 'when unavailable' do
      it 'gives class unavailable' do
        u = create(:user, branch: chair.location.branch)
        style = helper.shift_scheduling_styling(chair.location)
        id = chair.slots.first.to_id
        expect(style.call(id, u, false)[:class]).to include UNAVAIL_STR
      end
    end

    context 'when available but scheduled' do
      let(:u) { make_available_user(chair) }
      let(:style) { helper.shift_scheduling_styling(chair.location) }

      before do
        style
        schedule_during(u, chair)
      end

      it 'gives class scheduled here when scheduled at same location' do
        id = chair.slots.first.to_id
        classes = style.call(id, u, false)[:class]
        expect(classes).to include SCHEDULED_HERE_CLASS
      end

      it 'does not display tooltip when scheduled at same location' do
        id = chair.slots.first.to_id
        expect(style.call(id, u, false)).not_to include('data_toggle', 'title')
      end

      it 'gives class scheduled when scheduled at different location' do
        loc = create(:location, branch: u.branch)
        chair.update(location: loc)
        id = chair.slots.first.to_id
        classes = style.call(id, u, false)[:class]
        expect(classes).to include SCHEDULED_CLASS
      end

      it 'displays correct tooltip when scheduled at different location' do
        loc = create(:location, branch: u.branch)
        chair.update(location: loc)
        id = chair.slots.first.to_id
        expect(style.call(id, u, false)).to include('data-toggle' =>
           'tooltip', 'title' => loc.name)
      end
    end

    context 'when selected' do
      let(:style) { helper.shift_scheduling_styling(chair.location) }
      let(:id) { chair.slots.first.to_id }

      it 'gives class selected' do
        u = create(:user, branch: chair.location.branch)
        expect(style.call(id, u, true)[:class]).to \
          include(AdminDashboardsHelper::SELECTED_USER_CLASS)
      end

      it 'allows painting when available and unscheduled' do
        u = make_available_user(chair)
        expect(style.call(id, u, true)[:class]).to \
          include(PAINTABLE_STR)
      end
    end

    it 'does not allow painting when not selected' do
      style = helper.shift_scheduling_styling(chair.location)
      u = make_available_user(chair)
      id = chair.slots.first.to_id
      expect(style.call(id, u, false)[:class]).not_to \
        include(PAINTABLE_STR)
    end
  end

  describe '#heatmap_calendar_inner_html' do
    it 'contains link to the slots heatmap page' do
      id = '0-900'
      inner = helper.heatmap_calendar_inner_html(id)
      expect(inner).to include "slot_id=#{id}"
    end
  end

  describe '#heatmap_calendar_styling' do
    it 'gives correct color to slot with no availabilities' do
      style = helper.heatmap_calendar_styling([]).call('0-900')
      expect(style).to eq({})
    end

    it 'gives correct color to slot with availabilities' do
      u = create(:user)
      a = create(:availability, user: u)
      style = helper.heatmap_calendar_styling([u]).call(a.slots.first.to_id)
      expect(style[:class]).to eq 'heatmap-color-1'
    end
  end

  describe '#location_options' do
    it 'includes names of all locations within the branch' do
      loc2 = create(:location, branch: loc.branch)
      loc_opts = helper.location_options(loc.branch)
      expect(loc_opts.map(&:first)).to eq [loc.name, loc2.name]
    end
  end

  describe '#available_users' do
    it 'does not return unavailable users' do
      expect(helper.available_users([create(:user)], chair)).to eq []
    end

    it 'returns available users' do
      u = make_available_user(chair)
      expect(helper.available_users([u], chair)).to eq [u]
    end
  end

  describe '#mini_calendar_scheduling' do
    let(:user) { create(:user) }
    let(:mini_cal) { helper.mini_calendar_styling(user) }
    let(:avail) do
      create(:availability, day: 'Monday', start_time: 1300,
                            end_time: 1400, user: user)
    end
    let(:shift) do
      create(:shift, day: 'Monday', start_time: 1300, end_time: 1330,
                     user: user)
    end

    it 'gives class mini-scheduled when the user is scheduled' do
      sched = avail.slots.filter { |x| shift.covers_timeslot?(x) }
      id = sched[0].to_id
      expect(mini_cal[id][:class]).to include 'mini-scheduled'
    end

    it 'gives class mini-available when user is available but not scheduled' do
      avail_not_sched = avail.slots.filter { |x| !shift.covers_timeslot?(x) }
      id = avail_not_sched[0].to_id
      expect(mini_cal[id][:class]).to include 'mini-available'
    end

    it 'gives class mini-top to first slot in availability' do
      expect(mini_cal[avail.slots.first.to_id][:class]).to include 'mini-top'
    end

    it 'gives class mini-bottom to last slot in availability' do
      expect(mini_cal[avail.slots.last.to_id][:class]).to include 'mini-bottom'
    end
  end

  describe '#formatted_user_table' do
    it 'gives form incomplete when target hours is missing' do
      users = [create(:user, target_hours: nil)]
      table = helper.formatted_user_table(users)
      expect(table).to eq [{ user: users[0], msg: 'Form incomplete' }]
    end

    it 'gives percenter of target hours when present' do
      users = [create(:user, target_hours: 6)]
      table = helper.formatted_user_table(users)
      expect(table[0][:msg]).to include('0')
    end
  end

  describe '#icon_button' do
    it 'returns invisible element if show is false' do
      elem = helper.icon_button(false, 'value')
      expect(elem).to have_css('.invisible')
    end

    it 'returns visible element if show is true' do
      elem = helper.icon_button(true, 'value')
      expect(elem).not_to have_css('.invisible')
    end
  end

  describe '#progress_text' do
    let(:chair) { create(:chair, start_time: 900, end_time: 1100) }
    let(:sched) { create(:user, branch: chair.location.branch, lead: true) }
    let(:shift) do
      create(:shift, chair: chair,
                     start_time: 930,
                     end_time: 1030,
                     user: sched)
    end
    let(:unsched) { create(:user, branch: chair.location.branch) }

    context 'when showing continuously staffed chairs' do
      before do
        shift
        chair.location.continuous = true
      end

      it 'gives number of filled slots' do
        expect(progress_text(chair, [sched, unsched])).to include '4/8'
      end

      it 'gives number of users available to fill unscheduled slots' do
        create(:availability, user: unsched,
                              start_time: 900,
                              end_time: 1100,
                              day: chair.day)
        expect(progress_text(chair, [sched, unsched])).to include '1 available'
      end
    end

    context 'when showing chairs that are not continuously staffed' do
      before do
        shift
        chair.location.continuous = false
      end

      it 'gives the number of lead hours scheduled for the chair' do
        expect(progress_text(chair, [sched, unsched])).to include\
          '1.0 lead hours'
      end

      it 'gives the number total number of user hours scheduled' do
        expect(progress_text(chair, [sched, unsched])).to include\
          '1.0 scheduled hours'
      end
    end
  end

  describe '#available_users_slot' do
    let(:users) { users_with_availabilities(loc.branch) }

    it 'of the given users, returns those available during the slot' do
      slot = users[0].availabilities.first.slots.first.to_id
      expect(available_users_slot(users, slot)).to eq [users[0]]
    end

    it 'does not return available users who are not among the given users' do
      slot = users[0].availabilities.first.slots.first.to_id
      expect(available_users_slot(users[1..], slot)).to eq []
    end

    it 'does not return available users who are already scheduled' do
      slot = users[0].availabilities.first.slots.first
      chair.update(start_time: slot.int, end_time: slot.succ.int, location: loc)
      schedule_during(users[0], chair)
      expect(available_users_slot(users, slot.to_id)).to eq []
    end
  end

  def make_available_user(chair)
    u = create(:user, branch: chair.location.branch)
    create(:availability, start_time: chair.start_time,
                          end_time: chair.end_time,
                          user: u,
                          day: chair.day)
    u
  end

  def schedule_during(user, chair)
    create(:shift, user: user,
                   start_time: chair.start_time,
                   end_time: chair.end_time,
                   day: chair.day,
                   chair: chair)
  end

  describe '#users_sorted_by_availability' do
    it 'puts available users before unavailable ones' do
      users = users_with_availabilities(loc.branch)
      chair = make_chair(loc.branch)
      sorted_users = helper.users_sorted_by_availability(loc, chair)
      expect(sorted_users[0].username).to eq users[2].username
    end
  end

  describe '#user_list_greyed_out' do
    let(:chair) { create(:chair) }
    let(:user) { make_available_user(chair) }

    it 'returns true when the user is not available' do
      u = create(:user)
      expect(helper.user_list_greyed_out(u, chair)).to be true
    end

    it 'returns false when the user is available' do
      expect(helper.user_list_greyed_out(user, chair)).to be false
    end

    it 'returns false when chair is nil' do
      expect(helper.user_list_greyed_out(user, nil)).to be false
    end
  end

  def make_chair(branch)
    loc = create(:location, branch: branch, name: branch.title)
    create(:chair, location: loc, start_time: 1200,
                   end_time: 1600, day: 'Monday')
  end

  def users_with_availabilities(branch)
    users = [create(:user, username: 'user1', target_hours: 6, branch: branch),
             create(:user, username: 'user2', target_hours: 6, branch: branch),
             create(:user, username: 'user3', target_hours: 6, branch: branch)]
    create(:availability, day: 'Monday', start_time: 1300,
                          end_time: 1400, user: users[2])
    create(:availability, day: 'Tuesday', start_time: 1300,
                          end_time: 1400, user: users[1])
    create(:availability, day: 'Monday', start_time: 1800,
                          end_time: 1830, user: users[0])
    users
  end
end
