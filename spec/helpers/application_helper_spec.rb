# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe '#hours_string' do
    it 'drops .0 from result' do
      expect(helper.hours_string(10.0)).to eq('10 hours')
    end

    it 'returns singular when hours is 1' do
      expect(helper.hours_string(1.0)).to eq('1 hour')
    end
  end
end
