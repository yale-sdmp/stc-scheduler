# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AvailabilitiesHelper, type: :helper do
  describe '#display_time' do
    it 'returns a string formatted as hour:minute AM' do
      expect(helper.display_time(145)).to eq('1:45 AM')
    end

    it 'returns a string formatted as hour:minute PM' do
      expect(helper.display_time(2300)).to eq('11:00 PM')
    end

    it 'returns a string formatted as hour:minute PM for 12' do
      expect(helper.display_time(1200)).to eq('12:00 PM')
    end

    it 'returns a string formatted as 12:00 AM for 0' do
      expect(helper.display_time(0)).to eq('12:00 AM')
    end

    it 'returns a string formatted as 12:00 AM for 2400' do
      expect(helper.display_time(2400)).to eq('12:00 AM')
    end
  end

  # rubocop:disable RSpec/MultipleExpectations
  describe '#times' do
    let(:minute_step) { MINUTE_STEP }

    it 'contains correct times [800 to 2400)' do
      expect(helper.times).to include(800)
      # assuming minute_step < 60
      expect(helper.times).to include(800 + minute_step)
      expect(helper.times).to include(2360 - minute_step)
      expect(helper.times.length).to eq(16 * (60 / minute_step))
    end

    it 'does not contain times with minutes inconsistent with slot length' do
      # assuming minute_step < 60
      helper.times.each do |time|
        expect(time % 100).to be <= 60 - minute_step
      end
    end

    it 'does not contain times with hour > 23' do
      expect(helper.times).not_to include(2400)
      expect(helper.times).not_to include(0)
    end

    context 'when given a range' do
      it 'returns the correct range' do
        range = (1215...1630)
        times = helper.times(range)
        expect(times).to include(range.begin)
        expect(times).to include(next_time(range.begin))
        expect(times).to include(range.end - minute_step)
      end

      it 'does not return times outside the range' do
        range = (1200...1630)
        times = helper.times(range)
        expect(times).not_to include(range.begin - minute_step)
        expect(times).not_to include(range.end)
      end

      it 'return an empty array when given an invalid range' do
        range = (900...800)
        expect(helper.times(range)).to eq([])
      end
    end
  end
  # rubocop:enable RSpec/MultipleExpectations

  describe '#sum_hours' do
    it 'returns 0 if given no availabilities' do
      expect(helper.sum_hours([])).to eq(0)
    end

    it 'returns the correct sum' do
      availabilities = [
        build(:availability, start_time: 800, end_time: 900),
        build(:availability, start_time: 1115, end_time: 2345)
      ]
      expect(helper.sum_hours(availabilities)).to eq(13.5)
    end
  end

  describe '#generate_avails_hash' do
    let(:availabilities) do
      (0..5).map { |i| build(:availability, day: i, id: i) }
    end

    it 'adds all of the given availabilities to the hash' do
      avails = generate_avails_hash(availabilities)
      count = avails.sum(&:size)
      expect(count).to eq(availabilities.length)
    end

    it 'returns hash[day][time]=> avail.id that starts at that day and time' do
      avails = generate_avails_hash(availabilities)
      avail = availabilities[0]
      expect(avails[day(avail)][avail.start_time]).to eq(avail.id)
    end
  end

  describe '#delete_availability_button' do
    let(:avail) { build(:availability, id: 4) }
    let(:current_user) { build(:user) }

    it 'returns nil if the given day and time do not start an availability' do
      avails = generate_avails_hash([avail])
      expect(delete_availability_button(0, 800, avails)).to be_nil
    end

    it 'does not return nil if the given day and time start an availability' do
      avails = generate_avails_hash([avail])
      button = delete_availability_button(day(avail), avail.start_time, avails)
      expect(button).not_to be_nil
    end
  end

  # returns day as integer
  def day(availability)
    Availability.days[availability.day]
  end
end
