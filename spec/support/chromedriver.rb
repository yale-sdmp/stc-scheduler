# frozen_string_literal: true

require 'selenium-webdriver'

Capybara.register_driver :selenium_chrome_headless_docker_friendly do |app|
  Capybara::Selenium::Driver.load_selenium
  capabilities = Selenium::WebDriver::Chrome::Options.new(
    args: %w(headless disable-gpu no-sandbox disable-dev-shm-usage)
  )
  Capybara::Selenium::Driver
    .new(app, browser: :chrome, capabilities: capabilities)
end

Capybara.ignore_hidden_elements = false
Capybara.server = :puma, { Silent: true }
Capybara.javascript_driver = :selenium_chrome_headless_docker_friendly
