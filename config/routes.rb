Rails.application.routes.draw do
  namespace :admin do
    resources :users
    resources :user_preferred_locations
    resources :locations
    resources :chairs
    resources :availabilities
    resources :branches
    resources :shifts
    resources :deputy_roles

    root to: "users#index"
  end

  devise_for :users

  resources :user_preferred_locations, only: %i(new create)
  
  resources :availabilities, only: %i(index new create destroy) do
    collection do
      get 'export', to: 'availabilities#availabilities_csv'
      delete 'delete', to: 'availabilities#delete_all'
      post 'calendar_create', to: 'availabilities#calendar_create'
      get 'calendar', to: 'availabilities#calendar'
    end
  end

  resources :shifts do
    collection do
      post 'calendar_create', to: 'shifts#calendar_create'
      get 'deputy_export', to: 'shifts#deputy_export'
    end
  end

  resources :admin_dashboard, only: [:heatmap, :users] do
    collection do
      get 'heatmap', to: 'admin_dashboards#heatmap'
      get 'users', to: 'admin_dashboards#users'
      get 'scheduling', to: 'admin_dashboards#scheduling'
    end
  end

  get '/heartbeat' => 'heartbeat#show'

  resources :user, only: [:show, :edit, :update, :destroy] do
    member do
      post 'submit', to: 'user#submit'
    end
  end

  resources :dashboards, only: [:index] do
    collection do
      get 'unsubmitted_report'
    end
  end


  unauthenticated :user do
    root to: 'dashboards#unauthenticated', as: 'landing_page'
  end
  root to: 'dashboards#index'
  
  get 'dashboards/email_incomplete', to: 'dashboards#email_incomplete', as: 'email_incomplete'
  
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
