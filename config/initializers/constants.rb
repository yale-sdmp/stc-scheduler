# This file stores constants which are used in multiple places in the
# applications. 

# all time blocks are a multiple of MINUTE_STEP
MINUTE_STEP = 15


# CSS class names
# classes that are applied to individual cells in the calendar
HIGHLIGHT_STR = 'highlighted'
UNAVAIL_STR = 'unavailable'
SUBMIT_STR = 'submitted'
PAINTABLE_STR = 'paintable'

# classes for shift scheduling calendar
SCHEDULED_CLASS = 'scheduled'
SCHEDULED_HERE_CLASS = 'scheduled-here'
